#The default settings for local EMS.
#Date: 14/Feb/2017

#This function is to store the following default settings for local EMS.
#1)The default values of database
#2)The default values of time_information
#3)The default values of forecasting performance
#4)The default values of input parameter,e.g prices.
#5)The default parameters of diesel generators, type I, II, III
#6)The default parameters of utility grid
#7)The default parameters of wind power
#8)The default parameters of photovoltaic generators
#9)The default parameters of energy storage systems
#10)The default parameters of bidirectional converters
#11)The default parameters of ac load type I and type II
#12)The default parameters of dc load type I and type II
#13)The default parameters of resource manager
#14)The default parameters of penal factors

#####################################1)The default values of database########################
class default_local_database:
    db_str = 'mysql+pymysql://' + 'root' + ':' + 'Ntu@1003' + '@' + 'localhost' + '/' + 'emsdb'

class default_network_database:
    db_str_microgrid = 'mysql+pymysql://' + 'microgrid' + ':' + 'microgrid' + '@' + '155.69.227.32:3306/emsdb'
#155.69.218.214
class default_BBB_database:
    db_str = 'mysql+pymysql://' + 'ems' + ':' + 'Ntu@1003' + '@' + '192.168.7.2' + '/' + 'emsdb'

class default_local_EMS_version:
    version = 1 # 1 indicates the EMS is local one, else EMS is the UEMS

class default_forecasting_model:
    #The forecasting functions
    pv_forecasting_model = '/pv_forecasting.pkl'
    load_forecasting_model = '/load_forecasting.pkl'
    wp_forecasting_model = '/wp_forecasting.pkl'

class default_data_format:
    #The forecasting functions
    opf_accuracy = 4
####################################2)The default values of time_information#################
class default_time:
    Base_time = 1490889600
    Time_step_uc = 3600 # The time step of unit committment
    Time_step_ed = 300  # The time step of economic dispatch
    Time_step_opf = 300 #The time step of optimal power flow
    Look_ahead_time_uc = 24 * 3600 # The look ahead time of unit committment
    Look_ahead_time_ed = 3600  # The look ahead time of economic dispatch
    Look_ahead_time_uc_intra_day = 12 * 3600  # The look ahead time of inta-day unit committment, 12 hours
    Look_ahead_time_uc_time_step = round(Look_ahead_time_uc/Time_step_uc)
    Look_ahead_time_uc_intra_day_time_step = round(Look_ahead_time_uc_intra_day / Time_step_uc)
    Look_ahead_time_ed_time_step = round(Look_ahead_time_ed/Time_step_ed)

###################################3)The default values of forecasting performance###########
class default_uncertainty_settings:
    #The uncertainty level of different resources in the day-ahead stage
    Uncertainty_pv_day_ahead = 0.1
    Uncertainty_wp_day_ahead = 0.1
    Uncertainty_critical_load_ac_day_ahead = 0.1
    Uncertainty_uncritical_load_ac_day_ahead = 0.1
    Uncertainty_critical_load_dc_day_ahead = 0.1
    Uncertainty_uncritical_load_dc_day_ahead = 0.1

    #The uncertainty level of different resources in the real-time dispatch stage
    Uncertainty_pv_real_time = 0.1
    Uncertainty_wp_real_time = 0.1
    Uncertainty_critical_load_ac_real_time = 0.1
    Uncertainty_uncritical_load_ac_real_time = 0.1
    Uncertainty_critical_load_dc_real_time = 0.1
    Uncertainty_uncritical_load_dc_real_time= 0.1

#################################4)The default values of input parameter,e.g prices.#########
class default_resources_settings:
    #Default resources capacities
    BASE_VA = 1000 #The base VA in the operation
    ESS_CHARGING_COST = 0.002 # The charging cost of ESS, S$/kWh
    ESS_DISCHARGING_COST = 0.01  # The charging cost of ESS, S$/kWh
    FUEL_COST = 1.3 #The fuel cost of diesel generators, 1.3 S$/L
    UG_PRICE = 0.08 #The energy price from the utility grid, 0.08 S$/kWh
    PV_CURTAILMENT_COST = 2 #The curtailment cost of photovoltaic generators
    WP_CURTAILMENT_COST = 2 #The curtailment cost of photovoltaic generators
    CRITICAL_LOAD_SHEDDING_COST = 100 #The shedding cost of critical load, 100 S$/kWh
    UNCRITICAL_LOAD_SHEDDING_COST = 10  # The shedding cost of uncritical load, 10 S$/kWh

################################5)The default parameters of diesel generators, type I, II, III######
class default_diesel_generator_parameters_type_I:
    #Classical parameters for type I distributed generators
    DG_PMAX = 3#The maximal output of the diesel generator
    DG_PMIN = 1

    DG_FUEL_CAP = 9.3
    DG_FUEL_LIMITATION = 0
    DG_FUEL0 = 9.3

    DG_FUEL_WARNING = 4.0

    DG_FUEL_PARAMETER_A = 0.26
    DG_FUEL_PARAMETER_B = 0.6

class default_diesel_generator_parameters_type_II:
    # Classical parameters for type II distributed generators
    #In this type the start up time and shut down time is considered
    DG_PMAX = 9.6
    DG_PMIN = 0

    DG_FUEL_CAP = 9.3
    DG_FUEL0 = 9.3
    DG_FUEL_LIMITATION = 0

    DG_FUEL_WARNING = 4.0

    DG_FUEL_PARAMETER_A = 0.26
    DG_FUEL_PARAMETER_B = 0.6

    DG_START_UP_TIME = 0
    DG_SHUT_DOWN_TIME = 0
    DG_ON_DURATION_MIN = 0
    DG_OFF_DURATION_MIN = 0

class default_diesel_generator_parameters_type_III:
    # Classical parameters for type II distributed generators
    #In this type the start up time and shut down time is considered
    #The fuel cost model is extended to qudratic format
    DG_PMAX = 9.6
    DG_PMIN = 0

    DG_FUEL_CAP = 9.3
    DG_FUEL0 = 9.3
    DG_FUEL_LIMITATION = 0

    DG_FUEL_WARNING = 4.0

    DG_FUEL_PARAMETER_A = 0.0
    DG_FUEL_PARAMETER_B = 0.26
    DG_FUEL_PARAMETER_C = 0.6

    DG_START_UP_TIME = 0
    DG_SHUT_DOWN_TIME = 0
    DG_ON_DURATION_MIN = 0
    DG_OFF_DURATION_MIN = 0

################################6)The parameters values of utility grid#####################
class default_utility_grid_parameters_type_I:
    UG_PMAX = 4
    UG_PMIN = 0
    UG_POWER_FACOTR_MAX = 1
    UG_POWER_FACOTR_MIN = -1

################################7)The default parameters of wind power#####################
class default_wind_power_parameters:
    WP_CAP = 0.0
    WP_NWP = 0.0
    WP_FORECAST = 0.0

################################8)The default parameters of photovoltaic generators#####################
class default_photovoltaic_generators_parameters:

    PV_CAP = 6.0
    PV_NWP = 6.0
    PV_FORECAST = 0.0

################################9)The default parameters of energy storage system#####################
class default_energy_strorage_system_parameters:
    BAT_MAX_CPOW = 5
    BAT_MAX_DPOW = 5
    BAT_CAP = 13
    BAT_SOC_MIN = 0.2
    BAT_SOC_MAX = 1.0
    BAT_EFFICIENCY_DISCHARGING = 0.95
    BAT_EFFICIENCY_CHARGING = 0.95
    BAT_SOC0= 0.5
    BAT_COST_CHARGING = 0.0 # The charging cost is set to 0.0 S$/kWh
    BAT_COST_DISCHARGING = 0.02  # The discharging cost is set to 0.2 S$/kWh

################################10)The default parameters of bidirectional_converter#####################
class default_bidirectional_converter_parameters:
    BIC_CAP = 5
    BAT_EFFICIENCY_AC2DC = 0.95
    BIC_EFFICIENCY_DC2AC = 0.95

################################11)The default parameters of ac load#####################
class default_load_ac_type_I_parameters:
    #The classical parameters for type I critical AC load
    #In this type of AC load, the load can not be adjusted continuously.
    LOAD_AC_CAP = 5
    LOAD_AC_POWER_FACTOR = 1
    LOAD_AC_SHEDDING_DURATION_MAX = 10
    LOAD_AC_FORECAST = 3

class default_load_ac_type_II_parameters:
    # The classical parameters for type I critical AC load
    # In this type of AC load, the load can not be adjusted continuously.
    LOAD_AC_CAP = 5
    LOAD_AC_POWER_FACTOR = 1
    LOAD_AC_SHEDDING_ENERGY_MAX = 10 #This is the additionan parameters
    LOAD_AC_FORECAST = 3
    LOAD_AC_SHEDDING_DURATION_MAX = 10

################################12)The default parameters of dc load#####################
class default_load_dc_type_I_parameters:
    # The classical parameters for type I critical AC load
    # In this type of AC load, the load can not be adjusted continuously.
    LOAD_DC_CAP = 5
    LOAD_DC_SHEDDING_DURATION_MAX = 10
    LOAD_DC_FORECAST = 3

class default_load_dc_type_II_parameters:
    # The classical parameters for type I critical AC load
    # In this type of AC load, the load can not be adjusted continuously.
    LOAD_DC_CAP = 5
    LOAD_DC_SHEDDING_DURATION_MAX = 10
    LOAD_DC_FORECAST = 3
    LOAD_DC_SHEDDING_ENERGY_MAX = 10

################################13)The default parameters of resource manager#####################
class default_resourcec_manager_parameters:
    NWP = 0
    NPV = 3
    WP_CAP = 0
    PV_CAP = 4.0
    Dg_status = 1
    Ug_status = 0
    ESS_status = 1
    BIC_status = 1
    CAC3PL_ACTPOW_CAP = 1
    UCAC3PL_ACTPOW_CAP = 2
    CDCL_POW_CAP = 2
    UCDCL_POW_CAP = 1

################################14)The default parameters of penalty factors #####################
class default_penalty_factors:
    Penalty_SOC = 1 # The penalty factor for this in the dynamic economic dispatch
    Equaility_relation = 0.01  # The penalty factor for this in the dynamic economic dispatch
    Penalty_BIC = 0.01  # The penalty factor for this in the dynamic economic dispatch
    Penalty_Output = 0.1 # The penalty factor for the output profile between the set points of economic dispatch and optimal power flow
    Penalty_ESS = 0.01