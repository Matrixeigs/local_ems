#The normilized load profile for commercial users
#The data is obtained from the following websites:
#http://en.openei.org/datasets/files/961/pub/

#Author: Zhao TY
#Email: zhaoty@ntu.edu.sg

#Import from the database, this function might be useful for the further use, e.g. reliability analysis or planning.
from data_management.database_format import Test_data_database_format
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

db_str = 'mysql+pymysql://' + 'root' + ':' + 'Ntu@1003' + '@' + 'localhost' + '/' + 'emsdb'#The predefined database in local EMS
engine = create_engine(db_str, echo=False)
Session = sessionmaker(bind=engine)
session =Session()

def typical_commercial_load_generation(Start_time=1, End_time= 12):
    profile=[ ]
    for i in range(Start_time,End_time):
        if session.query(Test_data_database_format).filter_by(TIME_STAMP=i).count()>0:#There is at least one history data stored in the database
            row=session.query(Test_data_database_format).filter_by(TIME_STAMP=i).first()
            profile.append(row.LOAD_COMMERCIAL)
        else:#If their is no result, return 0
            profile.append(0)

    session.close()

    return profile