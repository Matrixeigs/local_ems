#This function is to gengerate the PV ouotput forecasting result
from data_management.database_format import PV_history_data_real_time,PV_history_data_hourly
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from default_setting import default_time
db_str = 'mysql+pymysql://' + 'root' + ':' + 'Ntu@1003' + '@' + 'localhost' + '/' + 'emsdb'

engine = create_engine(db_str, echo=False)
Session = sessionmaker(bind=engine)
session =Session()

def pv_output_generation_hourly(Start_time=1, End_time=12):
    profile=[]
    Start_time = Start_time % 24
    End_time = End_time % 24
    if End_time<=Start_time:
        End_time=Start_time+24

    for i in range(Start_time,End_time):
        if session.query(PV_history_data_hourly).filter_by(TIME_STAMP=i).count()>0:#There is at least one history data stored in the database
            row=session.query(PV_history_data_hourly).filter_by(TIME_STAMP=i).first()
            profile.append(row.SOLAR_RAD)
        else:#If their is no result, return 0
            profile.append(0)
    session.close()

    return profile

def pv_output_generation_real_time(Start_time = 1, End_time = 12):
    profile=[]

    Start_time = Start_time % 288
    End_time = End_time % 288
    if End_time <= Start_time:
        End_time = Start_time + 12

    for i in range(Start_time,End_time):
        if session.query(PV_history_data_real_time).filter_by(TIME_STAMP = i).count()>0:#There is at least one history data stored in the database
            row=session.query(PV_history_data_real_time).filter_by(TIME_STAMP = i).first()
            profile.append(row.SOLAR_RAD)
        else:#If their is no result, return 0
            profile.append(0)
    session.close()

    return profile