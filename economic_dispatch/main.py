from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from default_setting import default_time
from default_setting import default_local_database
from data_management.database_format import Economic_dispatch_database_format
import modelling.modelling_local_ems as modelling_ems

#According to the different kind of informations (time horizen and existing of UC)
import economic_dispatch.economic_dispatch_dynamic as ded

def economic_dispatch(Start_time=default_time.Base_time,
                      DG = modelling_ems.Diesel_generator_type_I_model(),
                      UG = modelling_ems.Utility_grid_model(),
                      PV = modelling_ems.Photovoltaic_generation_model(),
                      WP = modelling_ems.Wind_turbine_model(),
                      ESS = modelling_ems.Energy_storage_system_model(),
                      BIC = modelling_ems.Bi_directional_conveter_model(),
                      LOAD_AC = modelling_ems.AC_load_type_I_model(),
                      LOAD_AC_NC = modelling_ems.AC_load_type_I_model(),
                      LOAD_DC = modelling_ems.DC_load_type_I_model(),
                      LOAD_DC_NC = modelling_ems.DC_load_type_I_model(),
                      RESOURCE_MANAGER = modelling_ems.Resource_manager_model(),
                      UNCERTAINTY = modelling_ems.Uncertainty_model(),
                      UC = modelling_ems.unit_committment2economic_dispatch(),
                      Load_profile_ac=[2.77124, 2.80655, 2.83889, 2.88904, 3.02493, 3.22008],
                      Load_profile_ac_nc=[2.77124, 2.80655, 2.83889, 2.88904, 3.02493, 3.22008],
                      Load_profile_dc=[0.253247, 0.253413, 0.263015, 0.254706, 2.49743, 2.92667],
                      Load_profile_dc_nc=[0.253247, 0.253413, 0.263015, 0.254706, 2.49743, 2.92667],
                      PV_profile=[0, 0, 0, 0, 0, 0],
                      WP_profile=[0, 0, 0, 0, 0, 0],
                      Price_profile=[0.08, 0.08, 0.08, 0.08, 0.08, 0.08]):
    #Implement the economic dispatch
    result_ed = ded.dynamic_economic_dispatch(DG,UG,PV,WP,ESS,BIC,LOAD_AC,LOAD_AC_NC,LOAD_DC,LOAD_DC_NC,RESOURCE_MANAGER,
                                              UNCERTAINTY,UC,Load_profile_ac,Load_profile_ac_nc,Load_profile_dc,Load_profile_dc_nc,
                                              PV_profile,WP_profile,Price_profile)
    db_str = default_local_database.db_str
    engine = create_engine(db_str, echo=False)

    Session = sessionmaker(bind=engine)

    session = Session()
    # 5) save the results to database
    T = len(Load_profile_ac)
    for i in range(T):
        Dynamic_economic_dispatch_result = Economic_dispatch_database_format(
            TIME_STAMP=Start_time + i * default_time.Time_step_ed,
            CAC3PL_ACTPOW=Load_profile_ac[i],
            UCAC3PL_ACTPOW=Load_profile_ac_nc[i],
            CDCL_POW=Load_profile_dc[i],
            UCDCL_POW=Load_profile_dc_nc[i],
            PV_CAP = RESOURCE_MANAGER.PV_CAP,
            PV_FORECASTING = PV_profile[i],
            WP_CAP = RESOURCE_MANAGER.NWP,
            WP_FORECASTING = WP_profile[i],
            ELEC_PRICE = Price_profile[i],
            DG_STATUS = result_ed.Ig[i],
            DG_ACTPOW = result_ed.Pg[i],
            DG_FUEL = result_ed.DG_fuel[i],
            UG_STATUS = result_ed.Iug[i],
            UG_X_ACTPOW = result_ed.Pug[i],
            BAT_POW = result_ed.Pess[i],
            BAT_SOC = result_ed.SOC[i],
            BIC_ACTPOW =result_ed.Pbic[i],
            PV_CURTAIL = result_ed.Ipv[i],
            WP_CURTAIL = result_ed.Iwp[i],
            CAC3PL_SHED = result_ed.Iac[i],
            UCAC3PL_SHED = result_ed.Iac_nc[i],
            CDCL_SHED = result_ed.Idc[i],
            UCDCL_SHED = result_ed.Idc_nc[i])

        if session.query(Economic_dispatch_database_format).filter_by(TIME_STAMP=(Start_time + i * default_time.Time_step_ed)).count() == 0:  # if the data do not exist, the data will be inserted
            session.add(Dynamic_economic_dispatch_result)
            session.commit()
        else:  # if the data has already existed, this data will be updated.
            row = session.query(Economic_dispatch_database_format).filter_by(
                TIME_STAMP=(Start_time + i * default_time.Time_step_ed)).first()
            row.CAC3PL_ACTPOW = Load_profile_ac[i]
            row.UCAC3PL_ACTPOW = Load_profile_ac_nc[i]
            row.CDCL_POW = Load_profile_dc[i]
            row.UCDCL_POW = Load_profile_dc_nc[i]
            row.PV_CAP = RESOURCE_MANAGER.PV_CAP
            row.PV_FORECASTING = PV_profile[i]
            row.WP_CAP = RESOURCE_MANAGER.NWP
            row.WP_FORECASTING = WP_profile[i]
            row.ELEC_PRICE = Price_profile[i]
            row.DG_STATUS = result_ed.Ig[i]
            row.DG_ACTPOW = result_ed.Pg[i]
            row.DG_FUEL = result_ed.DG_fuel[i],
            row.UG_STATUS = result_ed.Iug[i]
            row.UG_X_ACTPOW = result_ed.Pug[i]
            row.BAT_POW = result_ed.Pess[i]
            row.BAT_SOC = result_ed.SOC[i]
            row.BIC_ACTPOW = result_ed.Pbic[i]
            row.PV_CURTAIL = result_ed.Ipv[i]
            row.WP_CURTAIL = result_ed.Iwp[i]
            row.CAC3PL_SHED = result_ed.Iac[i],
            row.UCAC3PL_SHED = result_ed.Iac_nc[i]
            row.CDCL_SHED = result_ed.Idc[i]
            row.UCDCL_SHED = result_ed.Idc_nc[i]
            session.commit()
    #Return the economic dispatch result
    return result_ed

if __name__=="__main__":
    Start_time = default_time.Base_time
    DG = modelling_ems.Diesel_generator_type_I_model()
    UG = modelling_ems.Utility_grid_model()
    PV = modelling_ems.Photovoltaic_generation_model()
    WP = modelling_ems.Wind_turbine_model()
    ESS = modelling_ems.Energy_storage_system_model()
    BIC = modelling_ems.Bi_directional_conveter_model()
    LOAD_AC = modelling_ems.AC_load_type_I_model()
    LOAD_AC_NC = modelling_ems.AC_load_type_I_model()
    LOAD_DC = modelling_ems.DC_load_type_I_model()
    LOAD_DC_NC = modelling_ems.DC_load_type_I_model()
    RESOURCE_MANAGER = modelling_ems.Resource_manager_model()
    UNCERTAINTY = modelling_ems.Uncertainty_model()
    UC = modelling_ems.unit_committment2economic_dispatch()
    Load_profile_ac = [2.77124, 2.80655, 2.83889, 2.88904, 3.02493, 3.22008]
    Load_profile_ac_nc = [2.77124, 2.80655, 2.83889, 2.88904, 3.02493, 3.22008]
    Load_profile_dc = [0.253247, 0.253413, 0.263015, 0.254706, 2.49743, 2.92667]
    Load_profile_dc_nc = [0.253247, 0.253413, 0.263015, 0.254706, 2.49743, 2.92667]
    PV_profile = [0, 0, 0, 0, 0, 0]
    WP_profile = [0, 0, 0, 0, 0, 0]
    Price_profile = [0.08, 0.08, 0.08, 0.08, 0.08, 0.08]

    result_ed = economic_dispatch(Start_time, DG,UG, PV, WP, ESS, BIC, LOAD_AC, LOAD_AC_NC, LOAD_DC, LOAD_DC_NC,
                                  RESOURCE_MANAGER, UNCERTAINTY,UC,Load_profile_ac, Load_profile_ac_nc, Load_profile_dc,
                                  Load_profile_dc_nc, WP_profile, PV_profile,Price_profile)

    print("The economic dispatch result is:", result_ed.Solution_status)