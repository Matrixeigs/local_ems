#This function is the data prepration for the dynamic economic dispatch
#Date: 16/Feb/2017
#Author: Zhao TY
#This should be a general query function for UC and ED

from data_management.database_format import Unit_committment_day_ahead_database_format,Real_time_forecasting
from sqlalchemy import create_engine, and_
from sqlalchemy.orm import sessionmaker
from default_setting import default_local_database
from default_setting import default_time
from modelling.modelling_local_ems import unit_committment2economic_dispatch,default_resourcec_manager_parameters

#The information from resource manager

def dynamic_economic_dispatch_set_up(Time = default_time.Base_time,
                                     Look_ahead_time_ed = default_time.Look_ahead_time_ed):
    if Time % 300 == 0:  # The operation of ED is to realize for half an hour later
        Start_time_ed = Time  # The qurey model
        End_time_ed = Time + Look_ahead_time_ed
    else:
        print('This is not the implement time of economic dispatch')
        Start_time_ed = Time- Time%default_time.Time_step_ed + default_time.Time_step_ed  # The qurey model
        End_time_ed = Start_time_ed + Look_ahead_time_ed

    #Local the base time of this time slot
    Base_time_uc = Start_time_ed
    Base_time_uc_look_ahead = End_time_ed


    db_str = default_local_database.db_str
    engine = create_engine(db_str, echo=False)
    Session = sessionmaker(bind=engine)
    session = Session()

    Ig = session.query(Unit_committment_day_ahead_database_format.DG_STATUS).filter(
        and_(Unit_committment_day_ahead_database_format.TIME_STAMP >= Base_time_uc,
             Unit_committment_day_ahead_database_format.TIME_STAMP < Base_time_uc_look_ahead)).all()

    Pg = session.query(Unit_committment_day_ahead_database_format.DG_ACTPOW).filter(
        and_(Unit_committment_day_ahead_database_format.TIME_STAMP >= Base_time_uc,
             Unit_committment_day_ahead_database_format.TIME_STAMP < Base_time_uc_look_ahead)).all()

    DG_fuel = session.query(Unit_committment_day_ahead_database_format.DG_FUEL).filter(
        and_(Unit_committment_day_ahead_database_format.TIME_STAMP >= Base_time_uc,
             Unit_committment_day_ahead_database_format.TIME_STAMP < Base_time_uc_look_ahead)).all()

    Iug = session.query(Unit_committment_day_ahead_database_format.UG_STATUS).filter(
        and_(Unit_committment_day_ahead_database_format.TIME_STAMP >= Base_time_uc,
             Unit_committment_day_ahead_database_format.TIME_STAMP < Base_time_uc_look_ahead)).all()

    Pug = session.query(Unit_committment_day_ahead_database_format.UG_X_ACTPOW).filter(
        and_(Unit_committment_day_ahead_database_format.TIME_STAMP >= Base_time_uc,
             Unit_committment_day_ahead_database_format.TIME_STAMP < Base_time_uc_look_ahead)).all()

    SOC = session.query(Unit_committment_day_ahead_database_format.BAT_SOC).filter(
        and_(Unit_committment_day_ahead_database_format.TIME_STAMP >= Base_time_uc,
             Unit_committment_day_ahead_database_format.TIME_STAMP < Base_time_uc_look_ahead)).all()

    Iac = session.query(Unit_committment_day_ahead_database_format.CAC3PL_SHED).filter(
        and_(Unit_committment_day_ahead_database_format.TIME_STAMP >= Base_time_uc,
             Unit_committment_day_ahead_database_format.TIME_STAMP < Base_time_uc_look_ahead)).all()

    Iac_nc = session.query(Unit_committment_day_ahead_database_format.UCAC3PL_SHED).filter(
        and_(Unit_committment_day_ahead_database_format.TIME_STAMP >= Base_time_uc,
             Unit_committment_day_ahead_database_format.TIME_STAMP < Base_time_uc_look_ahead)).all()

    Idc = session.query(Unit_committment_day_ahead_database_format.CDCL_SHED).filter(
        and_(Unit_committment_day_ahead_database_format.TIME_STAMP >= Base_time_uc,
             Unit_committment_day_ahead_database_format.TIME_STAMP < Base_time_uc_look_ahead)).all()

    Idc_nc = session.query(Unit_committment_day_ahead_database_format.UCDCL_SHED).filter(
        and_(Unit_committment_day_ahead_database_format.TIME_STAMP >= Base_time_uc,
             Unit_committment_day_ahead_database_format.TIME_STAMP < Base_time_uc_look_ahead)).all()

    Ipv = session.query(Unit_committment_day_ahead_database_format.PV_CURTAIL).filter(
        and_(Unit_committment_day_ahead_database_format.TIME_STAMP >= Base_time_uc,
             Unit_committment_day_ahead_database_format.TIME_STAMP < Base_time_uc_look_ahead)).all()

    Iwp = session.query(Unit_committment_day_ahead_database_format.WP_CURTAIL).filter(
        and_(Unit_committment_day_ahead_database_format.TIME_STAMP >= Base_time_uc,
             Unit_committment_day_ahead_database_format.TIME_STAMP < Base_time_uc_look_ahead)).all()

    session.close()

    result = unit_committment2economic_dispatch()

    result.Ig = [Ig[0][0]] * default_time.Look_ahead_time_ed_time_step
    result.Pg = [Pg[0][0]] * default_time.Look_ahead_time_ed_time_step
    result.DG_fuel = [DG_fuel[0][0]] * default_time.Look_ahead_time_ed_time_step

    result.Iug = [Iug[0][0]] * default_time.Look_ahead_time_ed_time_step
    result.Pug = [Pug[0][0]] * default_time.Look_ahead_time_ed_time_step

    result.SOC = [SOC[0][0]] * default_time.Look_ahead_time_ed_time_step

    result.Iac = [Iac[0][0]] * default_time.Look_ahead_time_ed_time_step
    result.Iac_nc = [Iac_nc[0][0]] * default_time.Look_ahead_time_ed_time_step
    result.Idc = [Idc[0][0]] * default_time.Look_ahead_time_ed_time_step
    result.Idc_nc = [Idc_nc[0][0]] * default_time.Look_ahead_time_ed_time_step

    result.Ipv = [Ipv[0][0]] * default_time.Look_ahead_time_ed_time_step
    result.Iwp = [Iwp[0][0]] * default_time.Look_ahead_time_ed_time_step
    result.SOC_tracing_time = default_time.Look_ahead_time_ed_time_step - 1 - round(
        Base_time_uc % default_time.Time_step_uc / default_time.Time_step_ed)

    # if len(Ig)==1:#There is only one result
    #     result.Ig = [Ig[0][0]] * default_time.Look_ahead_time_ed_time_step
    #     result.Pg = [Pg[0][0]] * default_time.Look_ahead_time_ed_time_step
    #     result.DG_fuel = [DG_fuel[0][0]] * default_time.Look_ahead_time_ed_time_step
    #
    #     result.Iug = [Iug[0][0]] * default_time.Look_ahead_time_ed_time_step
    #     result.Pug = [Pug[0][0]] * default_time.Look_ahead_time_ed_time_step
    #
    #     result.SOC = [SOC[0][0]] * default_time.Look_ahead_time_ed_time_step
    #
    #     result.Iac = [Iac[0][0]] * default_time.Look_ahead_time_ed_time_step
    #     result.Iac_nc = [Iac_nc[0][0]] * default_time.Look_ahead_time_ed_time_step
    #     result.Idc = [Idc[0][0]] * default_time.Look_ahead_time_ed_time_step
    #     result.Idc_nc = [Idc_nc[0][0]] * default_time.Look_ahead_time_ed_time_step
    #
    #     result.Ipv = [Ipv[0][0]] * default_time.Look_ahead_time_ed_time_step
    #     result.Iwp = [Iwp[0][0]] * default_time.Look_ahead_time_ed_time_step
    #     result.SOC_tracing_time = default_time.Look_ahead_time_ed_time_step - 1 - round(Base_time_uc%default_time.Time_step_uc/default_time.Time_step_ed)
    #
    # elif len(Ig)==2:#The operation is crossing time
    #     #calculate the remaining time period
    #     if Start_time_ed % 3600 != 0:
    #         Remaining_period = round((Base_time_uc_look_ahead-Start_time_ed)/default_time.Time_step_ed)
    #         Extra_time = default_time.Look_ahead_time_ed_time_step-Remaining_period
    #     else:
    #         Remaining_period = round((Base_time_uc_look_ahead - Start_time_ed) / default_time.Time_step_ed)
    #         Extra_time = default_time.Look_ahead_time_ed_time_step - Remaining_period
    #     Ig_real = []
    #     Pg_real = []
    #     DG_fuel_real = []
    #     Iug_real = []
    #     Pug_real =[]
    #     SOC_real = []
    #     Iac_real = []
    #     Iac_nc_real = []
    #     Idc_real = []
    #     Idc_nc_real = []
    #     Ipv_real = []
    #     Iwp_real = []
    #     for i in range(Remaining_period):
    #         Ig_real.append(Ig[0][0])
    #         Pg_real.append(Pg[0][0])
    #         DG_fuel_real.append(DG_fuel[0][0])
    #
    #         Iug_real.append(Iug[0][0])
    #         Pug_real.append(Pug[0][0])
    #
    #         SOC_real.append(SOC[0][0])
    #         Iac_real.append(Iac[0][0])
    #         Iac_nc_real.append(Iac_nc[0][0])
    #         Idc_real.append(Idc[0][0])
    #         Idc_nc_real.append(Idc_nc[0][0])
    #
    #         Ipv_real.append(Ipv[0][0])
    #         Iwp_real.append(Iwp[0][0])
    #
    #     for i in range(Extra_time):
    #         Ig_real.append(Ig[1][0])
    #         Pg_real.append(Pg[1][0])
    #         DG_fuel_real.append(DG_fuel[1][0])
    #
    #         Iug_real.append(Iug[1][0])
    #         Pug_real.append(Pug[1][0])
    #
    #         SOC_real.append(SOC[1][0])
    #         Iac_real.append(Iac[1][0])
    #         Iac_nc_real.append(Iac_nc[1][0])
    #         Idc_real.append(Idc[1][0])
    #         Idc_nc_real.append(Idc_nc[1][0])
    #
    #         Ipv_real.append(Ipv[1][0])
    #         Iwp_real.append(Iwp[1][0])
    #
    #     result.Ig = Ig_real
    #     result.Pg = Pg_real
    #     result.DG_fuel = DG_fuel_real
    #
    #     result.Iug = Iug_real
    #     result.Pug = Pug_real
    #
    #     result.SOC = SOC_real
    #
    #     result.Iac = Iac_real
    #     result.Iac_nc = Iac_nc_real
    #     result.Idc = Idc_real
    #     result.Idc_nc = Idc_nc_real
    #
    #     result.Ipv = Ipv_real
    #     result.Iwp = Iwp_real
    #
    #     result.SOC_tracing_time = round(max(Remaining_period - 1, 0))

    #According to the time
    return result

def forecasting_result_query(Time = default_time.Base_time,
                             Look_ahead_time_ed = default_time.Look_ahead_time_ed,
                             RESOURCE_MANAGER = default_resourcec_manager_parameters()):
    if Time % 300 == 0:  # The operation of ED is to realize for half an hour later
        Start_time_ed = Time  # The qurey model
        End_time_ed = Time + Look_ahead_time_ed
    else:
        print('This is not the implement time of economic dispatch')
        Start_time_ed = Time- Time%default_time.Time_step_ed + default_time.Time_step_ed  # The qurey model
        End_time_ed = Start_time_ed + Look_ahead_time_ed

    db_str = default_local_database.db_str
    engine = create_engine(db_str, echo=False)
    Session = sessionmaker(bind=engine)
    session = Session()

    Load_ac_profile = session.query(Real_time_forecasting.CAC3PL_ACTPOW).filter(
        and_(Real_time_forecasting.TIME_STAMP >= Start_time_ed,
             Real_time_forecasting.TIME_STAMP < End_time_ed)).all()

    Load_ac_nc_profile = session.query(Real_time_forecasting.UCAC3PL_ACTPOW).filter(
        and_(Real_time_forecasting.TIME_STAMP >= Start_time_ed,
             Real_time_forecasting.TIME_STAMP < End_time_ed)).all()

    Load_dc_profile = session.query(Real_time_forecasting.CDCL_POW).filter(
        and_(Real_time_forecasting.TIME_STAMP >= Start_time_ed,
             Real_time_forecasting.TIME_STAMP < End_time_ed)).all()

    Load_dc_nc_profile = session.query(Real_time_forecasting.UCDCL_POW).filter(
        and_(Real_time_forecasting.TIME_STAMP >= Start_time_ed,
             Real_time_forecasting.TIME_STAMP < End_time_ed)).all()

    WP_profile = session.query(Real_time_forecasting.WP_FORECASTING).filter(
        and_(Real_time_forecasting.TIME_STAMP >= Start_time_ed,
             Real_time_forecasting.TIME_STAMP < End_time_ed)).all()

    PV_profile = session.query(Real_time_forecasting.PV_FORECASTING).filter(
        and_(Real_time_forecasting.TIME_STAMP >= Start_time_ed,
             Real_time_forecasting.TIME_STAMP < End_time_ed)).all()

    Price_profile = session.query(Real_time_forecasting.ELEC_PRICE).filter(
        and_(Real_time_forecasting.TIME_STAMP >= Start_time_ed,
             Real_time_forecasting.TIME_STAMP < End_time_ed)).all()

    session.close()

    Load_ac_profile_real = [ ]
    Load_ac_nc_profile_real = [ ]
    Load_dc_profile_real = [ ]
    Load_dc_nc_profile_real = [ ]
    WP_profile_real = [ ]
    PV_profile_real = [ ]
    Price_profile_real = [ ]

    for i in range (len(Load_ac_profile)):
      Load_ac_profile_real.append(Load_ac_profile[i][0] * RESOURCE_MANAGER.CAC3PL_ACTPOW_CAP)
      Load_ac_nc_profile_real.append(Load_ac_nc_profile[i][0]* RESOURCE_MANAGER.UCAC3PL_ACTPOW_CAP)
      Load_dc_profile_real.append(Load_dc_profile[i][0]* RESOURCE_MANAGER.CDCL_POW_CAP)
      Load_dc_nc_profile_real.append(Load_dc_nc_profile[i][0]* RESOURCE_MANAGER.UCDCL_POW_CAP)
      WP_profile_real.append(WP_profile[i][0]* RESOURCE_MANAGER.WP_CAP)
      PV_profile_real.append(PV_profile[i][0]* RESOURCE_MANAGER.PV_CAP)
      Price_profile_real.append(Price_profile[i][0])

    return Load_ac_profile_real,Load_ac_nc_profile_real,Load_dc_profile_real,Load_dc_nc_profile_real,WP_profile_real,\
           PV_profile_real,Price_profile_real
