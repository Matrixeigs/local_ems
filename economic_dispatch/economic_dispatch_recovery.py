#The day-ahead unit committment function
#The solving method is based on the SCIP package
#http://scip.zib.de/#scipoptsuite
#Date: 14 Feb 2017
#The defination of day-ahead UC is to obtain the optimal combination of resources
from pyscipopt import Model
import modelling.modelling_local_ems as modelling_ems
from default_setting import default_resources_settings
from default_setting import default_time
from default_setting import default_penalty_factors
from modelling.modelling_local_ems import economic_dispatch_result

def dynamic_economic_recovery(DG = modelling_ems.Diesel_generator_type_I_model(),
                              UG = modelling_ems.Utility_grid_model(),
                              PV = modelling_ems.Photovoltaic_generation_model(),
                              WP = modelling_ems.Wind_turbine_model(),
                              ESS = modelling_ems.Energy_storage_system_model(),
                              BIC = modelling_ems.Bi_directional_conveter_model(),
                              LOAD_AC = modelling_ems.AC_load_type_I_model(),
                              LOAD_AC_NC = modelling_ems.AC_load_type_I_model(),
                              LOAD_DC = modelling_ems.DC_load_type_I_model(),
                              LOAD_DC_NC = modelling_ems.DC_load_type_I_model(),
                              RESOURCE_MANAGER = modelling_ems.Resource_manager_model(),
                              UNCERTAINTY = modelling_ems.Uncertainty_model(),
                              UC = modelling_ems.unit_committment2economic_dispatch(),
                              Load_profile_ac = [2.77124, 2.80655, 2.83889, 2.88904, 3.02493, 3.22008],
                              Load_profile_ac_nc = [2.77124, 2.80655, 2.83889, 2.88904, 3.02493, 3.22008],
                              Load_profile_dc = [0.253247, 0.253413, 0.263015, 0.254706, 2.49743, 2.92667],
                              Load_profile_dc_nc = [0.253247, 0.253413, 0.263015, 0.254706, 2.49743, 2.92667],
                              PV_profile = [0, 0, 0, 0, 0, 0],
                              WP_profile = [0, 0, 0, 0, 0, 0],
                              Price_profile = [0.08, 0.08, 0.08, 0.08, 0.08, 0.08]):
    T = len(Load_profile_ac)#The look_ahead horizen
    for i in range(T):
        Load_profile_ac[i]= Load_profile_ac[i] * (1 - UC.Iac[i])
        Load_profile_ac_nc[i] = Load_profile_ac_nc[i] * (1 - UC.Iac_nc[i])
        Load_profile_dc[i] = Load_profile_dc[i] * (1 - UC.Idc[i])
        Load_profile_dc_nc[i] = Load_profile_dc_nc[i] * (1 - UC.Idc_nc[i])
        if RESOURCE_MANAGER.NPV !=0:
            PV_profile[i] = PV_profile[i]*max(RESOURCE_MANAGER.NPV-UC.Ipv[i],0)/RESOURCE_MANAGER.NPV
        else:
            PV_profile[i] = 0

        if RESOURCE_MANAGER.NWP != 0:
            WP_profile[i] = WP_profile[i] * max(RESOURCE_MANAGER.NWP - UC.Iwp[i], 0) / RESOURCE_MANAGER.NWP
        else:
            WP_profile[i] = 0
    #define the decision variables
    model=Model("Dynamic economic dispatch recovery model")
    Pg = { }#Output of generators
    Ru_dg = { }#Ramp up reserve
    Rd_dg = { }#Ramp down reserve

    Pug = { }
    Ru_ug = { }
    Rd_ug = { }

    Pess_c = { }
    Pess_dc = { }
    Eess = { }
    Ru_ess = { }
    Rd_ess = { }

    Pa2d = { }
    Pd2a = { }

    Pac = { }
    Pdc = { }
    Pac_nc = { }
    Pdc_nc = { }
    Pwp = { }
    Ppv = { }

    Delta_t=default_time.Time_step_ed / 3600

    #Variables announcement
    for i in range(T):
        Pg[i] = model.addVar(obj=DG.b_g * default_resources_settings.FUEL_COST,
                             lb=DG.Pmin * min(UC.Ig[i], RESOURCE_MANAGER.Dg_status),
                             ub=DG.Pmax * min(UC.Ig[i], RESOURCE_MANAGER.Dg_status))
        Ru_dg[i] = model.addVar(lb=0, ub=DG.Pmax * min(UC.Ig[i], RESOURCE_MANAGER.Dg_status))
        Rd_dg[i] = model.addVar(lb=0, ub=DG.Pmax * min(UC.Ig[i], RESOURCE_MANAGER.Dg_status))

        Pug[i] = model.addVar(obj=Price_profile[i], lb=UG.Pmin * min(UC.Iug[i], RESOURCE_MANAGER.Ug_status),
                              ub=UG.Pmax * min(UC.Iug[i], RESOURCE_MANAGER.Ug_status))

        Ru_ug[i] = model.addVar(lb=0, ub=(UG.Pmax - UG.Pmin) * min(UC.Iug[i], RESOURCE_MANAGER.Ug_status))
        Rd_ug[i] = model.addVar(lb=0, ub=(UG.Pmax - UG.Pmin) * min(UC.Iug[i], RESOURCE_MANAGER.Ug_status))

        Pess_c[i] = model.addVar(obj = ESS.Cess_charging*Delta_t,lb=0,ub=ESS.Pmax_charging)
        Pess_dc[i]= model.addVar(obj = ESS.Cess_discharging*Delta_t,lb=0,ub=ESS.Pmax_discharging)
        Eess[i] = model.addVar( lb = ESS.SOC_min*ESS.Capacity, ub=ESS.SOC_max*ESS.Capacity)
        Ru_ess[i] = model.addVar( lb = 0, ub = ESS.Pmax_charging+ESS.Pmax_discharging)
        Rd_ess[i] = model.addVar( lb = 0, ub = ESS.Pmax_charging+ESS.Pmax_discharging)

        Pa2d[i] = model.addVar( lb=0, ub=BIC.Capacity)
        Pd2a[i] = model.addVar( lb=0, ub=BIC.Capacity)

        Pac[i] = model.addVar(obj = -LOAD_AC.Cost_shedding * Delta_t ,lb=0,ub=Load_profile_ac[i])
        Pdc[i] = model.addVar(obj = -LOAD_DC.Cost_shedding * Delta_t ,lb=0,ub=Load_profile_dc[i])
        Pac_nc[i] = model.addVar(obj = -LOAD_AC_NC.Cost_shedding * Delta_t, lb=0,ub=Load_profile_ac_nc[i])
        Pdc_nc[i] = model.addVar(obj = -LOAD_DC_NC.Cost_shedding * Delta_t, lb=0,ub=Load_profile_dc_nc[i])

        Pwp[i] = model.addVar(obj=-WP.Cost_curtailment * Delta_t , lb=0,ub=WP_profile[i])
        Ppv[i] = model.addVar(obj=-PV.Cost_curtailment * Delta_t , lb=0,ub=PV_profile[i])

    ESS_discharging_efficiency= 1.0 / ESS.Efficiency_discharging
    SOC_derivation_positive = model.addVar(obj = default_penalty_factors.Penalty_SOC, lb=0, ub = 1)
    SOC_derivation_negative = model.addVar(obj = default_penalty_factors.Penalty_SOC, lb=0, ub = 1)

    for i in range(T):
        model.addCons(Pg[i] + Ru_dg[i] <= DG.Pmax * min(UC.Ig[i], RESOURCE_MANAGER.Dg_status))
        model.addCons(Pg[i] - Rd_dg[i] >= DG.Pmin * min(UC.Ig[i], RESOURCE_MANAGER.Dg_status))

       #The utility grid part
        model.addCons(Pug[i] + Ru_ug[i] <= min(UC.Iug[i],RESOURCE_MANAGER.Ug_status) * UG.Pmax)
        model.addCons(Pug[i] - Rd_ug[i] >= min(UC.Iug[i],RESOURCE_MANAGER.Ug_status) * UG.Pmin)

        #The Energy storage part
        model.addCons(Pess_c[i] <= ESS.Pmax_charging)
        model.addCons(Pess_dc[i] <= ESS.Pmax_discharging)
        model.addCons(Pess_dc[i] - Pess_c[i] + Ru_ess[i] <= ESS.Pmax_discharging)
        model.addCons(Pess_c[i] - Pess_dc[i] + Rd_ess[i] <= ESS.Pmax_charging)
        if i==0:
           model.addCons(Eess[i] == ESS.ESS0 - Pess_dc[i] * Delta_t * ESS_discharging_efficiency  + Pess_c[i] * ESS.Efficiency_charging * Delta_t)
        else:
           model.addCons(Eess[i] == Eess[i-1] - Pess_dc[i] * ESS_discharging_efficiency * Delta_t + Pess_c[i] * ESS.Efficiency_charging * Delta_t)

        model.addCons(Eess[i] + Rd_ess[i] * ESS.Efficiency_charging * Delta_t <= ESS.SOC_max*ESS.Capacity)
        model.addCons(Eess[i] - Ru_ess[i] * ESS_discharging_efficiency * Delta_t >= ESS.SOC_min*ESS.Capacity)

        model.addCons(Eess[i]  <= ESS.SOC_max * ESS.Capacity)
        model.addCons(Eess[i]  >= ESS.SOC_min * ESS.Capacity)
         #The BIC part
        model.addCons(Pa2d[i] <= BIC.Capacity)
        model.addCons(Pd2a[i] <= BIC.Capacity)
        #Power balance constraints
        #AC side
        model.addCons(Pug[i] + Pg[i] + Pd2a[i] * BIC.Efficiency_DC2AC == Pac[i] + Pac_nc[i] + Pa2d[i])
        model.addCons(Pess_dc[i] + Ppv[i] + Pwp[i] + Pa2d[i] * BIC.Efficiency_AC2DC == Pdc[i] + Pdc_nc[i] + Pess_c[i] + Pd2a[i])

        #Reserve constraints
        model.addCons(Ru_ess[i] + Ru_dg[i] + Ru_ug[i] >= Pac[i] * UNCERTAINTY.Disturbance_range_ac_ed + Pac_nc[i] * UNCERTAINTY.Disturbance_range_ac_nc_ed + Pdc[i] *UNCERTAINTY.Disturbance_range_dc_ed
                       + Pdc_nc[i] * UNCERTAINTY.Disturbance_range_dc_nc_ed + Ppv[i] * UNCERTAINTY.Disturbance_range_pv_ed + Pwp[i] * UNCERTAINTY.Disturbance_range_wp_ed)
        model.addCons(Rd_ess[i] + Rd_dg[i] + Rd_ug[i] >= Pac[i] * UNCERTAINTY.Disturbance_range_ac_ed + Pac_nc[i] * UNCERTAINTY.Disturbance_range_ac_nc_ed + Pdc[i] *UNCERTAINTY.Disturbance_range_dc_ed
                       + Pdc_nc[i] * UNCERTAINTY.Disturbance_range_dc_nc_ed + Ppv[i] * UNCERTAINTY.Disturbance_range_pv_ed + Pwp[i] * UNCERTAINTY.Disturbance_range_wp_ed)

    Capacity_reciprocal = 1 / ESS.Capacity

    model.addCons(SOC_derivation_positive >= Eess[UC.SOC_tracing_time] * Capacity_reciprocal - UC.SOC[UC.SOC_tracing_time])
    model.addCons(SOC_derivation_negative >= UC.SOC[UC.SOC_tracing_time] - Eess[UC.SOC_tracing_time] * Capacity_reciprocal)

    model.hideOutput(True)#The output commamnd

    model.optimize()

    sol=model.getStatus()

    Ig_real = [ ]  # The start-up and shut down of diesel generators
    Pg_real = [ ]  # Output of generators
    Ru_dg_real = [ ]  # Ramp up reserve
    Rd_dg_real = [ ]  # Ramp down reserve
    DG_fuel_real = [ ]  # Fuel state of the diesel generators

    Iug_real = [ ]
    Pug_real = [ ]
    Ru_ug_real = [ ]
    Rd_ug_real = [ ]

    Pess_real = [ ]
    SOC_real = [ ]
    Ru_ess_real = [ ]
    Rd_ess_real = [ ]

    Pbic_real = [ ]

    Iac_real = [ ]
    Idc_real = [ ]
    Iac_nc_real = [ ]
    Idc_nc_real = [ ]

    Iwp_real = [ ]
    Ipv_real = [ ]

    result = economic_dispatch_result()

    for i in range(T):
        Ig_real.append(min(UC.Ig[i], RESOURCE_MANAGER.Dg_status))
        Pg_real.append(model.getVal(Pg[i]))
        Ru_dg_real.append(model.getVal(Ru_dg[i]))
        Rd_dg_real.append(model.getVal(Rd_dg[i]))
        DG_fuel_real.append( 0 )

        Iug_real.append(min(UC.Iug[i], RESOURCE_MANAGER.Ug_status))
        Pug_real.append(model.getVal(Pug[i]))
        Ru_ug_real.append(model.getVal(Ru_ug[i]))
        Rd_ug_real.append(model.getVal(Rd_ug[i]))

        Pess_real.append(model.getVal(Pess_dc[i]) - model.getVal(Pess_c[i]))
        SOC_real.append(model.getVal(Eess[i]) / ESS.Capacity)
        Ru_ess_real.append(model.getVal(Ru_ess[i]))
        Rd_ess_real.append(model.getVal(Rd_ess[i]))

        if Load_profile_ac[i] == 0:
            Iac_real.append(max(UC.Iac[i],0))
        elif model.getVal(Pac[i]) < Load_profile_ac[i]:
            Iac_real.append(1)
        else:
            Iac_real.append(0)

        if Load_profile_ac_nc[i] == 0:
            Iac_nc_real.append(max(UC.Iac_nc[i],0))
        elif model.getVal(Pac_nc[i]) < Load_profile_ac_nc[i]:
            Iac_nc_real.append(1)
        else:
            Iac_nc_real.append(0)

        if Load_profile_dc[i] == 0:
            Idc_real.append(max(UC.Idc[i],0))
        elif model.getVal(Pdc[i]) < Load_profile_dc[i]:
            Idc_real.append(1)
        else:
            Idc_real.append(0)

        if Load_profile_dc_nc[i] == 0:
            Idc_nc_real.append(max(UC.Idc_nc[i],0))
        elif model.getVal(Pdc_nc[i]) < Load_profile_dc_nc[i]:
            Idc_nc_real.append(1)
        else:
            Idc_nc_real.append(0)

        Pbic_real.append(model.getVal(Pd2a[i]) - model.getVal(Pa2d[i]))

        if WP_profile[i]!=0:
            Iwp_real.append(RESOURCE_MANAGER.NWP-int(RESOURCE_MANAGER.NWP* model.getVal(Pwp[i])/WP_profile[i]) -1)
        else:
            Iwp_real.append(0)

        if PV_profile[i]!=0:
            Ipv_real.append(RESOURCE_MANAGER.NPV-int(RESOURCE_MANAGER.NPV*model.getVal(Ppv[i])/PV_profile[i])-1)
        else:
            Ipv_real.append(0)

    result.Ig = Ig_real
    result.Pg = Pg_real
    result.Iug = Iug_real
    result.Pug = Pug_real
    result.Pess = Pess_real
    result.SOC = SOC_real

    result.Pbic = Pbic_real

    result.Iac = Iac_real
    result.Iac_nc = Iac_nc_real
    result.Idc = Idc_real
    result.Idc_nc = Idc_nc_real
    result.Ipv = Ipv_real
    result.Iwp = Iwp_real
    result.Solution_status = sol

    return result

#The end of recovery unit committment
if __name__=="__main__":
    DG = modelling_ems.Diesel_generator_type_I_model()
    UG = modelling_ems.Utility_grid_model()
    PV = modelling_ems.Photovoltaic_generation_model()
    WP = modelling_ems.Wind_turbine_model()
    ESS = modelling_ems.Energy_storage_system_model()
    BIC = modelling_ems.Bi_directional_conveter_model()
    LOAD_AC = modelling_ems.AC_load_type_I_model()
    LOAD_AC_NC = modelling_ems.AC_load_type_I_model()
    LOAD_DC = modelling_ems.DC_load_type_I_model()
    LOAD_DC_NC = modelling_ems.DC_load_type_I_model()
    RESOURCE_MANAGER = modelling_ems.Resource_manager_model()
    UNCERTAINTY = modelling_ems.Uncertainty_model()
    UC = modelling_ems.unit_committment2economic_dispatch()
    Load_profile_ac = [2.77124, 2.80655, 2.83889, 2.88904, 3.02493, 3.22008]
    Load_profile_ac_nc = [2.77124, 2.80655, 2.83889, 2.88904, 3.02493, 3.22008]
    Load_profile_dc = [0.253247, 0.253413, 0.263015, 0.254706, 2.49743, 2.92667]
    Load_profile_dc_nc = [0.253247, 0.253413, 0.263015, 0.254706, 2.49743, 2.92667]
    PV_profile = [0, 0, 0, 0, 0, 0]
    WP_profile = [0, 0, 0, 0, 0, 0]
    Price_profile = [0.08, 0.08, 0.08, 0.08, 0.08, 0.08]

    result = dynamic_economic_recovery(DG, UG, PV, WP, ESS, BIC, LOAD_AC, LOAD_AC_NC, LOAD_DC, LOAD_DC_NC,RESOURCE_MANAGER,
                                       UNCERTAINTY, UC, Load_profile_ac, Load_profile_ac_nc, Load_profile_dc,Load_profile_dc_nc,
                                       PV_profile, WP_profile, Price_profile)

    print("The recovery economic dispatch result is:",result.Solution_status)
