#Online economic dispatch
#Date:10/Jan/2017
#Author:Zhao Tianyang
#This algorithm is based on the onine algorithm in version 1
#This function is based on the
#Together with the data format, which is the same in the documents
import time

from sqlalchemy import Column, FLOAT, INTEGER, BINARY
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from test_functions.economic_dispatch_model_formulaiton import economic_dispatch_model_formulaiton_soluation

Base = declarative_base()
#Define the class in each database
class Economic_dispatch_db_data(Base):
    __tablename__ = 'economic_dispatch_data'
    TIME_LABEL=Column(FLOAT, primary_key=True)
    CAC3PL_ACTPOW=Column(FLOAT)
    UCAC3PL_ACTPOW=Column(FLOAT)
    CDCL_MAX_POW=Column(FLOAT)
    UCDCL_MAX_POW=Column(FLOAT)

    PV_CAP=Column(FLOAT)
    PV_FORECASTING= Column(FLOAT)

    WP_CAP = Column(FLOAT)
    WP_FORECASTING = Column(FLOAT)

    ELEC_PRICE = Column(FLOAT)

    DG_STATUS = Column(BINARY)
    DG_ACTPOW = Column(FLOAT)
    DG_FUEL = Column(FLOAT)

    UG_STATUS = Column(BINARY)
    UG_X_ACTPOW = Column(FLOAT)
    BAT_POW = Column(FLOAT)
    BAT_SOC = Column(FLOAT)

    BIC_ACTPOW = Column(INTEGER)
    PV_CURTAIL = Column(INTEGER)
    WP_CURTAIL = Column(INTEGER)

    CAC3PL_SHED = Column(BINARY)
    UCAC3PL_SHED = Column(BINARY)
    CDCL_SHED = Column(BINARY)
    UCDCL_SHED = Column(BINARY)

#Obtain the parameters from the database
db_str = 'mysql+pymysql://' + 'root' + ':' + 'Ntu@1003' + '@' + 'localhost' + '/' + 'emsdb'
engine = create_engine(db_str, echo=False)

Session = sessionmaker(bind=engine)

Base.metadata.create_all(engine, checkfirst=True)

t=time.time()

#for i in range(2015):
#    row=session.query(Economic_dispatch_db_data).filter_by(TIME_LABEL = i+1).first()
#    if row!=None:#if the serach
#        row.LOCAL_TIME=time.ctime(t+300*(i-1))
#        session.commit()

#The session is still online, untill a command has been sended

#1)Obtain the information
#These informations will be stroed in the database in the following versions
###1) Generation informations
DG={'Capacity':9.6,
    'Pmax':9.6,
    'Pmin':0,
    'Fuel_capacity':9.3,
    'Fuel_limitation':0,
    'Fuel_warning':4,
    'Fuel0':9.3,
     }

UG={'Capacity':10,
    'Power_factor_limitation':0.9,
    'Pmax':10,
    'Pmin':0,
    }

WP={'Capacity':0,
    'NWP':0,
    'Forecast':0,
    }

PV={'Capacity':10,
    'NPV':6,
    'Forecast':0,
    }

###2)Energy storage systems
ESS={'Capacity':10,
     'Pmax_discharging':5,
     'Pmax_charging':5,
     'Efficiency_discharging':0.95,
     'Efficiency_charging':0.95,
     'ESS_max':10,
     'ESS_min':2,
     'SOC_max':1,
     'SOC_min':0.2,
     'ESS0':5,
     }

###3)BIC information
#The positive is from AC 2 DC on BIC
BIC={'Capacity':5,
     'Efficiency_A2D':0.95,
     'Efficiency_D2A':0.95,
     }

###4)Load informations
LOAD_AC={'Capacity':5,
     'Power_factor':0.9,
     'Maximal_curtailment_time':10,
     'Forecast':3,
     }

LOAD_AC_NC={'Capacity':5,
     'Power_factor':0.9,
     'Maximal_curtailment_time':10,
     'Forecast':3,
     }

LOAD_DC={'Capacity':5,
     'Maximal_curtailment_time':10,
     'Forecast':3,
     }

LOAD_DC_NC={'Capacity':5,
     'Maximal_curtailment_time':10,
     'Forecast':3,
     }

###5)Cost informations
COST={'Cfuel':1.3,
      'Cess':0.02,
      'a_g':0.26,
      'b_g':0.6,
      'PV_curtailment':2,
      'WP_curtailment':2,
      'Cac_nc':10,
      'Cdc_nc':10,
      'Cac':100,
      'Cdc':100,
      'Ug_price':0.08,
      }

###6)Resources manager informaition,by default, all devicecs are online
RESOURCE_MANAGER={
    'NPV':6,
    'NWP':0,
    'DG_STATUS':1,
    'UG_STATUS':1,
    'ESS_STATUS':1,
    'BIC_STATUS':1,
      }
###7) Uncertainty information
UNCERTAINTY={
    'Disturbance_range_ac':0.1,
    'Disturbance_range_ac_nc':0.1,
    'Disturbance_range_dc':0.1,
    'Disturbance_range_dc_nc':0.1,
    'Disturbance_range_pv':0.1,
    'Disturbance_range_wp':0.1,
}
Delta_t_ed=1/12
#8)Obtain the unit committment plan
UC={'DG':0,
    'UG':1,
    'NPV':RESOURCE_MANAGER['NPV'],
    'MWP':RESOURCE_MANAGER['NWP'],
    'Iac':0,
    'Idc':0,
    'Iac_nc':0,
    'Idc_nc':0,
    }

for i in range(2,2016):

    session = Session()
    row=session.query(Economic_dispatch_db_data).filter_by(TIME_LABEL = i).first()#Obtain solutions from the database
#Update the values with the command from unit committment
    #
    if RESOURCE_MANAGER['NPV']!=0:
        PV['Forecast']=row.PV_FORECASTING*(RESOURCE_MANAGER['NPV']-UC['NPV'])/RESOURCE_MANAGER['NPV']
    else:
        PV['Forecast'] = 0

    if RESOURCE_MANAGER['NWP']!=0:
        WP['Forecast']=row.WP_FORECASTING*(RESOURCE_MANAGER['NWP']-UC['NWP'])/RESOURCE_MANAGER['NWP']
    else:
        WP['Forecast'] = 0

    LOAD_AC['Forecast']=row.CAC3PL_ACTPOW*(1-UC['Iac'])
    LOAD_AC_NC['Forecast']=row.UCAC3PL_ACTPOW*(1-UC['Iac_nc'])
    LOAD_DC['Forecast']=row.CDCL_MAX_POW*(1-UC['Idc'])
    LOAD_DC['Forecast']=row.UCAC3PL_ACTPOW*(1-UC['Idc_nc'])

#UC['DG'] = float(row.DG_STATUS)
#UC['UG'] = float(row.UG_STATUS)


    try:
       UC['DG']=float(row.DG_STATUS)
    except:
       UC['DG']=float(row.DG_STATUS)

    try:
       UC['UG']=float(row.UG_STATUS)
    except:
       UC['UG'] =float(row.UG_STATUS)
#Update ESS0 and DG_fuel0
    if row.DG_FUEL>=DG['Fuel_limitation']:
        DG['Fuel0']=row.DG_FUEL
    else:
        DG['Fuel0']=DG['Fuel_capacity']

    if row.BAT_SOC>=ESS['SOC_max']/ESS['Capacity']:
        ESS['ESS0'] = ESS['SOC_max'] * ESS['Capacity']
    elif row.BAT_SOC<=ESS['SOC_min']/ESS['Capacity']:
        ESS['ESS0'] = ESS['SOC_min'] * ESS['Capacity']
    else:
        ESS['ESS0'] = row.BAT_SOC*ESS['Capacity']
#2)economic dispatch implement
    economic_dispatch_solution=economic_dispatch_model_formulaiton_soluation(Delta_t_ed,DG,UG,PV,WP,ESS,BIC,LOAD_AC,LOAD_AC_NC,LOAD_DC,LOAD_DC_NC,COST,RESOURCE_MANAGER,UNCERTAINTY,UC)
#3)save data to database
    if economic_dispatch_solution['Ig']==0:
       row.DG_STATUS = 0
    else:
       row.DG_STATUS=(economic_dispatch_solution['Ig'])

#session.commit()
    row.DG_ACTPOW=economic_dispatch_solution['Pg']
#session.commit()
    row.DG_FUEL=economic_dispatch_solution['DG_fuel']
#session.commit()
    if economic_dispatch_solution['Iug']==0:
       row.UG_STATUS=0
    else:
       row.UG_STATUS = (economic_dispatch_solution['Iug'])
#session.commit()
    row.UG_X_ACTPOW=economic_dispatch_solution['Pug']
#session.commit()
    row.BAT_POW=economic_dispatch_solution['Pess']
#session.commit()
    row.BAT_SOC=economic_dispatch_solution['Eess']/ESS['Capacity']
#session.commit()

    row.PV_CURTAIL = economic_dispatch_solution['Ipv']
#session.commit()

    row.WP_CURTAIL = round(economic_dispatch_solution['Iwp'])
#session.commit()
    if economic_dispatch_solution['Iac']==0:
       row.CAC3PL_SHED=0
    else:
       row.CAC3PL_SHED=round(economic_dispatch_solution['Iac'])
#session.commit()
    if economic_dispatch_solution['Iac_nc']==0:
       row.UCAC3PL_SHED = 0
    else:
       row.UCAC3PL_SHED = round(economic_dispatch_solution['Iac_nc'])
#session.commit()
    if economic_dispatch_solution['Idc']==0:
       row.CDCL_SHED = 0
    else:
       row.CDCL_SHED = round(economic_dispatch_solution['Idc'])

#session.commit()
    if economic_dispatch_solution['Idc_nc']==0:
      row.UCDCL_SHED = 0
    else:
      row.UCDCL_SHED = round(economic_dispatch_solution['Idc_nc'])

    session.commit()
    session.close()
    print(i)