#The defination part of the different modules
#The following types are introduced
#1) AC sources: three types of diesel generators
#2) DC sources
#3) Energy storage systems
#4) Bi-directional converters
#5) AC load (Two-types)
#6) DC load (Two-types)
#7) Uncertainty model
#8) Resource managemer model

#Date: 23 Jan 2017
#Date: 16 Feb 2017
#Date: 2 Mar 2017
#What is new
# 1) the default value has been updated, as the default load values has been updated
# 2) the data format of unit committment, economic dispatch and optimal power flow has been defined
# 3) the area informaiton is provided

from default_setting import default_diesel_generator_parameters_type_I,default_diesel_generator_parameters_type_II,default_diesel_generator_parameters_type_III
from default_setting import default_resources_settings
from default_setting import default_utility_grid_parameters_type_I
from default_setting import default_wind_power_parameters
from default_setting import default_photovoltaic_generators_parameters
from default_setting import default_energy_strorage_system_parameters
from default_setting import default_bidirectional_converter_parameters

from default_setting import default_load_ac_type_I_parameters
from default_setting import default_load_ac_type_II_parameters
from default_setting import default_load_dc_type_I_parameters
from default_setting import default_load_dc_type_II_parameters

from default_setting import default_uncertainty_settings
from default_setting import default_resourcec_manager_parameters
from default_setting import default_time
########################################1) AC sources#################################
class Diesel_generator_type_I_model:
    "DG classes"
    def __init__(self):
        "Default values"
        self.Capacity = default_diesel_generator_parameters_type_I.DG_PMAX
        self.Pmin = default_diesel_generator_parameters_type_I.DG_PMIN
        self.Pmax = default_diesel_generator_parameters_type_I.DG_PMAX
        self.Fuel_capacity = default_diesel_generator_parameters_type_I.DG_FUEL_CAP
        self.Fuel_limitation = default_diesel_generator_parameters_type_I.DG_FUEL_LIMITATION
        self.Fuel_warning = default_diesel_generator_parameters_type_I.DG_FUEL_WARNING
        self.Fuel0 = default_diesel_generator_parameters_type_I.DG_FUEL0
        self.a_g = default_diesel_generator_parameters_type_I.DG_FUEL_PARAMETER_A
        self.b_g = default_diesel_generator_parameters_type_I.DG_FUEL_PARAMETER_B
        self.Cost_fuel = default_resources_settings.FUEL_COST #$/L
        self.DG_version = 1
        self.area = 1

    def add_attrib(self,value):
        "Additional attributes and their values"
        self.attrib=value

class Diesel_generator_type_II_model:
    "DG classes"
    def __init__(self):
        "Default values"
        self.Capacity = default_diesel_generator_parameters_type_II.DG_PMAX
        self.Pmin = default_diesel_generator_parameters_type_II.DG_PMIN
        self.Pmax = default_diesel_generator_parameters_type_II.DG_PMAX
        self.Fuel_capacity = default_diesel_generator_parameters_type_II.DG_FUEL_CAP
        self.Fuel_limitation = default_diesel_generator_parameters_type_II.DG_FUEL_LIMITATION
        self.Fuel_warning = default_diesel_generator_parameters_type_II.DG_FUEL_WARNING
        self.Fuel0 = default_diesel_generator_parameters_type_II.DG_FUEL0
        self.Ton_min = default_diesel_generator_parameters_type_II.DG_ON_DURATION_MIN
        self.Toff_min = default_diesel_generator_parameters_type_II.DG_OFF_DURATION_MIN

        self.Start_up = default_diesel_generator_parameters_type_II.DG_START_UP_TIME
        self.Shut_down = default_diesel_generator_parameters_type_II.DG_SHUT_DOWN_TIME

        self.a_g = default_diesel_generator_parameters_type_II.DG_FUEL_PARAMETER_A
        self.b_g = default_diesel_generator_parameters_type_II.DG_FUEL_PARAMETER_B

        self.Cost_fuel = default_resources_settings.FUEL_COST #$/L
        self.DG_version = 2
        self.area = 1
    def add_attrib(self,value):
        "Additional attributes and their values"
        self.attrib=value

class Diesel_generator_type_III_model:
    "DG classes"
    def __init__(self):
        "Default values"
        self.Capacity = default_diesel_generator_parameters_type_III.DG_PMAX
        self.Pmin = default_diesel_generator_parameters_type_III.DG_PMIN
        self.Pmax = default_diesel_generator_parameters_type_III.DG_PMAX
        self.Fuel_capacity = default_diesel_generator_parameters_type_III.DG_FUEL_CAP
        self.Fuel_limitation = default_diesel_generator_parameters_type_III.DG_FUEL_LIMITATION
        self.Fuel_warning = default_diesel_generator_parameters_type_III.DG_FUEL_WARNING
        self.Fuel0 = default_diesel_generator_parameters_type_III.DG_FUEL0
        self.Ton_min = default_diesel_generator_parameters_type_III.DG_ON_DURATION_MIN
        self.Toff_min = default_diesel_generator_parameters_type_III.DG_OFF_DURATION_MIN

        self.Start_up = default_diesel_generator_parameters_type_III.DG_START_UP_TIME
        self.Shut_down = default_diesel_generator_parameters_type_III.DG_SHUT_DOWN_TIME

        self.a_g = default_diesel_generator_parameters_type_III.DG_FUEL_PARAMETER_A
        self.b_g = default_diesel_generator_parameters_type_III.DG_FUEL_PARAMETER_B
        self.c_g = default_diesel_generator_parameters_type_III.DG_FUEL_PARAMETER_C

        self.Cost_fuel = default_resources_settings.FUEL_COST #$/L

        self.DG_version = 3
        self.area = 1
    def add_attrib(self,value):
        "Additional attributes and their values"
        self.attrib=value

class Utility_grid_model:
    "Utility classes"
    def __init__(self):
        self.Capacity = default_utility_grid_parameters_type_I.UG_PMAX
        self.Power_factor_limitation = default_utility_grid_parameters_type_I.UG_POWER_FACOTR_MAX
        self.Pmax = default_utility_grid_parameters_type_I.UG_PMAX
        self.Pmin = default_utility_grid_parameters_type_I.UG_PMIN
        self.Cug = default_resources_settings.UG_PRICE #$/kWh
        self.area = 1

    def add_attrib(self,value):
        "Additional attributes and their values"
        self.attrib=value

########################################2) DC sources#################################
class Wind_turbine_model:
    "Wind turbine classes"
    def __init__(self):
        self.Capacity = default_wind_power_parameters.WP_CAP
        self.NWP = default_wind_power_parameters.WP_NWP
        self.Forecast = default_wind_power_parameters.WP_FORECAST
        self.Cost_curtailment = default_resources_settings.WP_CURTAILMENT_COST #$/kWh
        self.area = 1
        if self.NWP == 0:
            self.Capacity = 0
            self.Forecast = 0

    def add_attrib(self,value):
        "Additional attributes and their values"
        self.attrib=value


class Photovoltaic_generation_model:
    "Wind turbine classes"
    def __init__(self):
        self.Capacity = default_photovoltaic_generators_parameters.PV_CAP
        self.NPV = default_photovoltaic_generators_parameters.PV_NWP
        self.Forecast = default_photovoltaic_generators_parameters.PV_FORECAST
        self.Cost_curtailment = default_resources_settings.PV_CURTAILMENT_COST  # $/kWh
        self.area = 1
        if self.NPV ==0:
            self.Capacity = 0
            self.Forecast = 0

    def add_attrib(self,value):
        "Additional attributes and their values"
        self.attrib=value

########################################3)Energy sotrage systems #################################
class Energy_storage_system_model:
    "ESS classes"
    def __init__(self):
        "Default values"
        self.Capacity = default_energy_strorage_system_parameters.BAT_CAP
        self.Pmax_discharging = default_energy_strorage_system_parameters.BAT_MAX_DPOW
        self.Pmax_charging = default_energy_strorage_system_parameters.BAT_MAX_CPOW
        self.Efficiency_discharging = default_energy_strorage_system_parameters.BAT_EFFICIENCY_DISCHARGING
        self.Efficiency_charging = default_energy_strorage_system_parameters.BAT_EFFICIENCY_CHARGING
        self.ESS_max = default_energy_strorage_system_parameters.BAT_CAP * default_energy_strorage_system_parameters.BAT_SOC_MAX
        self.SOC_max = default_energy_strorage_system_parameters.BAT_SOC_MAX
        self.SOC_min = default_energy_strorage_system_parameters.BAT_SOC_MIN
        self.ESS0 = default_energy_strorage_system_parameters.BAT_CAP*default_energy_strorage_system_parameters.BAT_SOC0
        self.Cess_charging = default_resources_settings.ESS_CHARGING_COST#Degradition cost of BESS
        self.Cess_discharging = default_resources_settings.ESS_CHARGING_COST  # Degradition cost of BESS
        self.area = 1

    def add_attrib(self,value):
        "Additional attributes and their values"
        self.attrib=value

########################################4)Bidirectional conveters #################################
class Bi_directional_conveter_model:
    "Wind turbine classes"
    def __init__(self):
        self.Capacity = default_bidirectional_converter_parameters.BIC_CAP
        self.Efficiency_AC2DC = default_bidirectional_converter_parameters.BAT_EFFICIENCY_AC2DC
        self.Efficiency_DC2AC = default_bidirectional_converter_parameters.BIC_EFFICIENCY_DC2AC
        self.area = 1

    def add_attrib(self,value):
        "Additional attributes and their values"
        self.attrib=value

########################################5) AC load #################################
class AC_load_type_I_model:
    def __init__(self):
        self.Capacity = default_load_ac_type_I_parameters.LOAD_AC_CAP
        self.Power_factor = default_load_ac_type_I_parameters.LOAD_AC_POWER_FACTOR
        self.Maximal_curtailment_time = default_load_ac_type_I_parameters.LOAD_AC_SHEDDING_DURATION_MAX
        self.Forecast = default_load_ac_type_I_parameters.LOAD_AC_FORECAST
        self.Load_version = 1 #Can be only adjusted through by given steps
        self.Cost_shedding = default_resources_settings.UNCRITICAL_LOAD_SHEDDING_COST # $/kWh
        self.area = 1

    def add_attrib(self, value):
       "Additional attributes and their values"
       self.attrib = value

class AC_load_type_II_model:
    def __init__(self):
        self.Capacity = default_load_ac_type_II_parameters.LOAD_AC_CAP
        self.Power_factor = default_load_ac_type_II_parameters.LOAD_AC_POWER_FACTOR
        self.Maximal_curtailment_energy = default_load_ac_type_II_parameters.LOAD_AC_SHEDDING_ENERGY_MAX  # kWh
        self.Forecast = default_load_ac_type_II_parameters.LOAD_AC_FORECAST
        self.Load_version = 2  # Can be  adjusted continuously
        self.Cost_shedding = default_resources_settings.UNCRITICAL_LOAD_SHEDDING_COST  # $/kWh
        self.area = 1
    def add_attrib(self,value):
        "Additional attributes and their values"
        self.attrib=value

########################################6) DC load #################################
class DC_load_type_I_model:
    def __init__(self):
        self.Capacity = default_load_dc_type_I_parameters.LOAD_DC_CAP
        self.Maximal_curtailment_time =default_load_dc_type_I_parameters.LOAD_DC_SHEDDING_DURATION_MAX
        self.Forecast = default_load_dc_type_I_parameters.LOAD_DC_FORECAST
        self.Load_version = 1  # Can be only adjusted through by given steps
        self.Cost_shedding = default_resources_settings.UNCRITICAL_LOAD_SHEDDING_COST  # $/kWh
        self.area = 1

    def add_attrib(self,value):
        "Additional attributes and their values"
        self.attrib=value

class DC_load_type_II_model:
    def __init__(self):
        self.Capacity = default_load_dc_type_II_parameters.LOAD_DC_CAP
        self.Maximal_curtailment_energy = default_load_dc_type_II_parameters.LOAD_DC_SHEDDING_ENERGY_MAX  # kWh
        self.Forecast = default_load_dc_type_II_parameters.LOAD_DC_FORECAST
        self.Load_version = 2  # Can be  adjusted continuously
        self.Cost_shedding = default_resources_settings.UNCRITICAL_LOAD_SHEDDING_COST  # $/kWh
        self.area = 1

    def add_attrib(self,value):
        "Additional attributes and their values"
        self.attrib=value

#####################################7) Uncertainty model##########################
class Uncertainty_model:
    def __init__(self):
        #The disturbance range in the unit committment stage (Hourly ahead)
        self.Disturbance_range_ac_uc = default_uncertainty_settings.Uncertainty_critical_load_ac_day_ahead
        self.Disturbance_range_ac_nc_uc = default_uncertainty_settings.Uncertainty_uncritical_load_ac_day_ahead
        self.Disturbance_range_dc_uc = default_uncertainty_settings.Uncertainty_critical_load_dc_day_ahead
        self.Disturbance_range_dc_nc_uc = default_uncertainty_settings.Uncertainty_uncritical_load_dc_day_ahead
        self.Disturbance_range_pv_uc = default_uncertainty_settings.Uncertainty_pv_day_ahead
        self.Disturbance_range_wp_uc = default_uncertainty_settings.Uncertainty_wp_day_ahead

        # The disturbance range in the unit committment stage (Hourly ahead)
        self.Disturbance_range_ac_ed = default_uncertainty_settings.Uncertainty_critical_load_ac_real_time
        self.Disturbance_range_ac_nc_ed = default_uncertainty_settings.Uncertainty_uncritical_load_ac_real_time
        self.Disturbance_range_dc_ed = default_uncertainty_settings.Uncertainty_critical_load_dc_real_time
        self.Disturbance_range_dc_nc_ed = default_uncertainty_settings.Uncertainty_uncritical_load_dc_real_time
        self.Disturbance_range_pv_ed = default_uncertainty_settings.Uncertainty_pv_real_time
        self.Disturbance_range_wp_ed = default_uncertainty_settings.Uncertainty_wp_real_time
        self.area = 1

    def add_attrib(self,value):
        "Additional attributes and their values"
        self.attrib=value

#####################################8) Resource manager model##########################
class Resource_manager_model:
    def __init__(self):
        self.NPV = default_resourcec_manager_parameters.NPV #Init variables
        self.NWP = default_resourcec_manager_parameters.NWP #Init variables
        self.Dg_status = default_resourcec_manager_parameters.Dg_status #Binary variables
        self.Ug_status = default_resourcec_manager_parameters.Ug_status #Binary variables
        self.ESS_status = default_resourcec_manager_parameters.ESS_status #Binary variables
        self.PV_CAP = default_resourcec_manager_parameters.PV_CAP #Binary variables
        self.WP_CAP = default_resourcec_manager_parameters.WP_CAP  # Binary variables
        self.CAC3PL_ACTPOW_CAP = default_resourcec_manager_parameters.CAC3PL_ACTPOW_CAP  # Binary variables
        self.UCAC3PL_ACTPOW_CAP = default_resourcec_manager_parameters.UCAC3PL_ACTPOW_CAP  # Binary variables
        self.CDCL_POW_CAP = default_resourcec_manager_parameters.CDCL_POW_CAP  # Binary variables
        self.UCDCL_POW_CAP = default_resourcec_manager_parameters.UCDCL_POW_CAP  # Binary variables
        self.area = 1

    def add_attrib(self,value):
        "Additional attributes and their values"
        self.attrib=value

################################## 9)  Unit committment result##########################
class unit_committment_result:
    def __init__(self):

        self.Ig = default_time.Look_ahead_time_uc_time_step * [0] #Init variables
        self.Pg = default_time.Look_ahead_time_uc_time_step * [0]
        self.DG_fuel = [default_diesel_generator_parameters_type_I.DG_FUEL0]*default_time.Look_ahead_time_uc_time_step

        self.Iug = default_time.Look_ahead_time_uc_time_step * [1] #Init variables
        self.Pug = default_time.Look_ahead_time_uc_time_step * [default_utility_grid_parameters_type_I.UG_PMAX]  # Init variables

        self.Pess = default_time.Look_ahead_time_uc_time_step * [0] #The positive direction is charging
        self.SOC = default_time.Look_ahead_time_uc_time_step * [0]  # The positive direction is charging

        self.Pbic = default_time.Look_ahead_time_uc_time_step * [0]

        self.Iac =  default_time.Look_ahead_time_uc_time_step * [0] # Binary variables
        self.Iac_nc = default_time.Look_ahead_time_uc_time_step * [0]  # Binary variables
        self.Idc = default_time.Look_ahead_time_uc_time_step * [0]  # Binary variables
        self.Idc_nc = default_time.Look_ahead_time_uc_time_step * [0]  # Binary variables

        self.Ipv = default_time.Look_ahead_time_uc_time_step * [0]  # Binary variables
        self.Iwp = default_time.Look_ahead_time_uc_time_step * [0]  # Binary variables

        self.Solution_status = 'optimal'

    def add_attrib(self,value):
        "Additional attributes and their values"
        self.attrib=value
################################## 10)  Economic dispatch result##########################
class economic_dispatch_result:
    def __init__(self):

        self.Ig = default_time.Look_ahead_time_ed_time_step * [0] #Init variables
        self.Pg = default_time.Look_ahead_time_ed_time_step * [0]
        self.DG_fuel = [default_diesel_generator_parameters_type_I.DG_FUEL0]*default_time.Look_ahead_time_ed_time_step

        self.Iug = default_time.Look_ahead_time_ed_time_step * [1] #Init variables
        self.Pug = default_time.Look_ahead_time_ed_time_step * [default_utility_grid_parameters_type_I.UG_PMAX]  # Init variables

        self.Pess = default_time.Look_ahead_time_ed_time_step * [0] #The positive direction is charging
        self.SOC = default_time.Look_ahead_time_ed_time_step * [0]  # The positive direction is charging

        self.Pbic = default_time.Look_ahead_time_ed_time_step * [0]

        self.Iac =  default_time.Look_ahead_time_ed_time_step * [0] # Binary variables
        self.Iac_nc = default_time.Look_ahead_time_ed_time_step * [0]  # Binary variables
        self.Idc = default_time.Look_ahead_time_ed_time_step * [0]  # Binary variables
        self.Idc_nc = default_time.Look_ahead_time_ed_time_step * [0]  # Binary variables

        self.Ipv = default_time.Look_ahead_time_ed_time_step * [0]  # Binary variables
        self.Iwp = default_time.Look_ahead_time_ed_time_step * [0]  # Binary variables

        self.Solution_status = 'optimal'

    def add_attrib(self,value):
        "Additional attributes and their values"
        self.attrib=value
################################## 11)  Optimal power flow result##########################
class optimal_power_flow_result:
    def __init__(self):
        self.Ig = 0 #Init variables
        self.Pg = 0
        self.Qg = 0

        self.Iug = 1 #Init variables
        self.Pug = default_utility_grid_parameters_type_I.UG_PMAX  # Init variables
        self.Qug = default_utility_grid_parameters_type_I.UG_PMAX  # Init variables

        self.Pess = 0 #The positive direction is charging
        self.SOC = 0  # The positive direction is charging

        self.Pbic = 0
        self.Qbic = 0

        self.Iac =  0 # Binary variables
        self.Iac_nc = 0  # Binary variables
        self.Idc = 0  # Binary variables
        self.Idc_nc = 0  # Binary variables

        self.Ipv = 0  # Binary variables
        self.Iwp = 0  # Binary variables

        self.Solution_status = 'optimal'

    def add_attrib(self,value):
        "Additional attributes and their values"
        self.attrib=value

class unit_committment_day_ahead2unit_committment_intra_day:
        def __init__(self):
            self.Ig = default_time.Look_ahead_time_uc_intra_day_time_step * [0]  # Init variables
            self.Pg = default_time.Look_ahead_time_uc_intra_day_time_step * [0]
            self.DG_fuel = [default_diesel_generator_parameters_type_I.DG_FUEL0] * default_time.Look_ahead_time_uc_intra_day_time_step

            self.Iug = default_time.Look_ahead_time_uc_intra_day_time_step * [1]  # Init variables
            self.Pug = default_time.Look_ahead_time_uc_intra_day_time_step * [
                default_utility_grid_parameters_type_I.UG_PMAX]  # Init variables

            self.Pess = default_time.Look_ahead_time_uc_intra_day_time_step * [0]  # The positive direction is charging
            self.SOC = default_time.Look_ahead_time_uc_intra_day_time_step * [0]  # The positive direction is charging

            self.Pbic = default_time.Look_ahead_time_uc_intra_day_time_step * [0]

            self.Iac = default_time.Look_ahead_time_uc_intra_day_time_step * [0]  # Binary variables
            self.Iac_nc = default_time.Look_ahead_time_uc_intra_day_time_step * [0]  # Binary variables
            self.Idc = default_time.Look_ahead_time_uc_intra_day_time_step * [0]  # Binary variables
            self.Idc_nc = default_time.Look_ahead_time_uc_intra_day_time_step * [0]  # Binary variables

            self.Ipv = default_time.Look_ahead_time_uc_intra_day_time_step * [0]  # Binary variables
            self.Iwp = default_time.Look_ahead_time_uc_intra_day_time_step * [0]  # Binary variables

            self.SOC_tracing_time = 0

        def add_attrib(self, value):
            "Additional attributes and their values"
            self.attrib = value

############################## 12) Unit committment plan for economic dispatch ###########
class unit_committment2economic_dispatch:
    def __init__(self):
        self.Ig = default_time.Look_ahead_time_ed_time_step * [0] #Init variables
        self.Pg = default_time.Look_ahead_time_ed_time_step * [0]
        self.DG_fuel = [default_diesel_generator_parameters_type_I.DG_FUEL0]*default_time.Look_ahead_time_ed_time_step

        self.Iug = default_time.Look_ahead_time_ed_time_step * [1] #Init variables
        self.Pug = default_time.Look_ahead_time_ed_time_step * [default_utility_grid_parameters_type_I.UG_PMAX]  # Init variables

        self.Pess = default_time.Look_ahead_time_ed_time_step * [0] #The positive direction is charging
        self.SOC = default_time.Look_ahead_time_ed_time_step * [0]  # The positive direction is charging

        self.Pbic = default_time.Look_ahead_time_ed_time_step * [0]

        self.Iac =  default_time.Look_ahead_time_ed_time_step * [0] # Binary variables
        self.Iac_nc = default_time.Look_ahead_time_ed_time_step * [0]  # Binary variables
        self.Idc = default_time.Look_ahead_time_ed_time_step * [0]  # Binary variables
        self.Idc_nc = default_time.Look_ahead_time_ed_time_step * [0]  # Binary variables

        self.Ipv = default_time.Look_ahead_time_ed_time_step * [0]  # Binary variables
        self.Iwp = default_time.Look_ahead_time_ed_time_step * [0]  # Binary variables

        self.SOC_tracing_time = 0

    def add_attrib(self,value):
        "Additional attributes and their values"
        self.attrib=value

class economic_dispatch2optimal_power_flow_information:
    def __init__(self):
        self.Ig = 0 #Init variables
        self.Pg = 0
        self.DG_fuel = default_diesel_generator_parameters_type_I.DG_FUEL0

        self.Iug =  1 #Init variables
        self.Pug = default_utility_grid_parameters_type_I.UG_PMAX  # Init variables

        self.Pess = 0 #The positive direction is charging
        self.SOC =  0  # The positive direction is charging

        self.Pbic =  0

        self.Iac =  0 # Binary variables
        self.Iac_nc =  0  # Binary variables
        self.Idc =  0  # Binary variables
        self.Idc_nc =  0  # Binary variables

        self.Ipv = 0  # Binary variables
        self.Iwp = 0  # Binary variables

    def add_attrib(self,value):
        "Additional attributes and their values"
        self.attrib=value