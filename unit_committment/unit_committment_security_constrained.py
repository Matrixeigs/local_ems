#The day-ahead unit committment function
#The solving method is based on the SCIP package
#http://scip.zib.de/#scipoptsuite
#Date: 14 Feb 2017
#The defination of day-ahead UC is to obtain the optimal combination of resources
#Note:1) In the day-ahead forecasting, it is assumed that every thing is on-line.

from pyscipopt import Model
import modelling.modelling_local_ems as modelling_ems
from default_setting import default_resources_settings
from default_setting import default_time
import unit_committment.uint_committment_recovery as recovery_uc
from modelling.modelling_local_ems import unit_committment_result


# Update the information from default value
def unit_committment_day_ahead(DG = modelling_ems.Diesel_generator_type_I_model(),
                               UG = modelling_ems.Utility_grid_model(),
                               PV = modelling_ems.Photovoltaic_generation_model(),
                               WP = modelling_ems.Wind_turbine_model(),
                               ESS = modelling_ems.Energy_storage_system_model(),
                               BIC=modelling_ems.Bi_directional_conveter_model(),
                               LOAD_AC = modelling_ems.AC_load_type_I_model(),
                               LOAD_AC_NC = modelling_ems.AC_load_type_I_model(),
                               LOAD_DC = modelling_ems.DC_load_type_I_model(),
                               LOAD_DC_NC = modelling_ems.DC_load_type_I_model(),
                               RESOURCE_MANAGER=modelling_ems.Resource_manager_model(),
                               UNCERTAINTY=modelling_ems.Uncertainty_model(),
                               Load_profile_ac=[2.77124,2.80655,2.83889,2.88904,3.02493,3.22008],
                               Load_profile_ac_nc=[2.77124,2.80655,2.83889,2.88904,3.02493,3.22008],
                               Load_profile_dc=[0.253247,0.253413,0.263015,0.254706,2.49743,2.92667],
                               Load_profile_dc_nc=[0.253247,0.253413,0.263015,0.254706,2.49743,2.92667],
                               PV_profile=[0, 0, 0, 0, 0, 0],
                               WP_profile=[0,0,0,0,0,0],
                               Price_profile=[0.08,0.08,0.08,0.08,0.08,0.08]):

    T = len(Load_profile_ac)#The look_ahead horizen
    LOAD_AC.Cost_shedding=default_resources_settings.CRITICAL_LOAD_SHEDDING_COST
    LOAD_AC_NC.Cost_shedding=default_resources_settings.UNCRITICAL_LOAD_SHEDDING_COST
    LOAD_DC.Cost_shedding=default_resources_settings.CRITICAL_LOAD_SHEDDING_COST
    LOAD_DC_NC.Cost_shedding=default_resources_settings.UNCRITICAL_LOAD_SHEDDING_COST
    #define the decision variables
    model=Model("IUC")

    Ig={}#The start-up and shut down of diesel generators
    Pg={}#Output of generators
    Ru_dg={}#Ramp up reserve
    Rd_dg={}#Ramp down reserve
    DG_fuel={}#Fuel state of the diesel generators

    Iug={}
    Pug={}
    Ru_ug={}
    Rd_ug={}

    Pess_c={}
    Pess_dc={}
    Eess={}
    Ru_ess={}
    Rd_ess={}

    Pa2d={}
    Pd2a={}
    Ia2d={}
    Iess={}

    Reserve_demand={}

    Delta_t=default_time.Time_step_uc/3600

    #Variables announcement
    for i in range(T):
        Ig[i]= model.addVar(vtype="BINARY",obj=DG.a_g*default_resources_settings.FUEL_COST)
        Pg[i] = model.addVar(obj=DG.b_g*default_resources_settings.FUEL_COST*Delta_t,lb=DG.Pmin,ub=DG.Pmax)
        Ru_dg[i] = model.addVar(lb=0,ub=DG.Pmax)
        Rd_dg[i] = model.addVar(lb=0,ub=DG.Pmax)
        DG_fuel[i]  = model.addVar(lb=DG.Fuel_limitation,ub=DG.Fuel0)

        Iug[i] = model.addVar(vtype="BINARY")#The fuel cost
        Pug[i] = model.addVar(obj=Price_profile[i]*Delta_t,lb=UG.Pmin,ub=UG.Pmax)
        Ru_ug[i] = model.addVar(lb=0,ub=UG.Pmax-UG.Pmin)
        Rd_ug[i] = model.addVar(lb=0,ub=UG.Pmax-UG.Pmin)

        Iess[i] = model.addVar(vtype="BINARY")
        Pess_c[i] = model.addVar(obj=ESS.Cess_charging*Delta_t,lb=0,ub=ESS.Pmax_charging)
        Pess_dc[i]= model.addVar(obj=ESS.Cess_discharging*Delta_t,lb=0,ub=ESS.Pmax_discharging)
        Eess[i] = model.addVar( lb=ESS.SOC_min*ESS.Capacity, ub=ESS.SOC_max*ESS.Capacity)
        Ru_ess[i] = model.addVar( lb=0, ub= ESS.Pmax_charging + ESS.Pmax_discharging)
        Rd_ess[i] = model.addVar( lb=0, ub= ESS.Pmax_charging + ESS.Pmax_discharging)

        Pa2d[i] = model.addVar( lb=0, ub= BIC.Capacity)
        Pd2a[i] = model.addVar( lb=0, ub= BIC.Capacity)
        Ia2d[i] = model.addVar( vtype="BINARY")
        Reserve_demand[i]=model.addVar( lb=0)

    ESS_discharging_efficiency=1 / ESS.Efficiency_discharging


    for i in range(T):
       model.addCons(Iug[i] + Ig[i]<=1)#C
       model.addCons(Pg[i] + Ru_dg[i] <= Ig[i] * DG.Pmax)
       model.addCons(Pg[i] - Rd_dg[i] >= Ig[i] * DG.Pmin)
       if i==0:
          model.addCons(DG_fuel[i] == DG.Fuel0 - Ig[i] * DG.a_g * Delta_t - Pg[i] * DG.b_g * Delta_t)
       else:
          model.addCons(DG_fuel[i] == DG_fuel[i-1] - Ig[i] * DG.a_g * Delta_t - Pg[i] * DG.b_g * Delta_t)

       model.addCons(DG_fuel[i]- Ru_dg[i] * DG.b_g * Delta_t>=DG.Fuel_limitation)#Reserve maintance constraint

       #The utility grid part
       model.addCons(Pug[i] + Ru_ug[i] <= Iug[i] * UG.Pmax)
       model.addCons(Pug[i] - Rd_ug[i] >= Iug[i] * UG.Pmin)

       #The Energy storage part
       model.addCons(Pess_c[i] <= Iess[i] * ESS.Pmax_charging)
       model.addCons(Pess_dc[i] <= (1-Iess[i]) * ESS.Pmax_discharging)
       model.addCons(Pess_dc[i]- Pess_c[i] + Ru_ess[i] <=  ESS.Pmax_discharging)
       model.addCons(Pess_c[i] - Pess_dc[i] + Rd_ess[i] <= ESS.Pmax_charging)
       if i==0:
           model.addCons(Eess[i] == ESS.ESS0 - Pess_dc[i] * Delta_t * ESS_discharging_efficiency  + Pess_c[i] * ESS.Efficiency_charging * Delta_t)
       else:
           model.addCons(Eess[i] == Eess[i-1] - Pess_dc[i] * ESS_discharging_efficiency * Delta_t + Pess_c[i] * ESS.Efficiency_charging * Delta_t)

       #model.addCons(Eess[i] + Rd_ess[i] * ESS.Efficiency_charging * Delta_t <= ESS.SOC_max*ESS.Capacity)
       #model.addCons(Eess[i] - Ru_ess[i] * ESS_discharging_efficiency * Delta_t >= ESS.SOC_min*ESS.Capacity)
       model.addCons(Eess[i] <= ESS.SOC_max * ESS.Capacity)
       model.addCons(Eess[i] >= ESS.SOC_min * ESS.Capacity)
       #The BIC part
       model.addCons(Pa2d[i] <= Ia2d[i] * BIC.Capacity)
       model.addCons(Pd2a[i] <= (1-Ia2d[i]) * BIC.Capacity)

       #Power balance constraints
       #AC side
       model.addCons(Pug[i] + Pg[i] + Pd2a[i] * BIC.Efficiency_DC2AC == Load_profile_ac[i] + Load_profile_ac_nc[i] + Pa2d[i])
       model.addCons(Pess_dc[i] + PV_profile[i] + WP_profile[i] + Pa2d[i] * BIC.Efficiency_AC2DC == Load_profile_dc[i] + Load_profile_dc_nc[i] + Pess_c[i] + Pd2a[i])

       #Reserve constraints
       model.addCons(Reserve_demand[i] == Load_profile_ac[i] * UNCERTAINTY.Disturbance_range_ac_uc + Load_profile_ac_nc[i] * UNCERTAINTY.Disturbance_range_ac_nc_uc + Load_profile_dc[i] *UNCERTAINTY.Disturbance_range_dc_uc \
                         + Load_profile_dc_nc[i] * UNCERTAINTY.Disturbance_range_dc_nc_uc + PV_profile[i] * UNCERTAINTY.Disturbance_range_pv_uc + WP_profile[i]*UNCERTAINTY.Disturbance_range_wp_uc)
       model.addCons(Ru_ess[i] + Ru_dg[i] + Ru_ug[i] >= Reserve_demand[i])
       model.addCons(Rd_ess[i] + Rd_dg[i] + Rd_ug[i] >= Reserve_demand[i])

    model.addCons(Eess[0] + Rd_ess[0] * ESS.Efficiency_charging * Delta_t <= ESS.SOC_max*ESS.Capacity)
    model.addCons(Eess[0] - Ru_ess[0] * ESS_discharging_efficiency * Delta_t >= ESS.SOC_min*ESS.Capacity)

    model.addCons( Eess[T-1] == ESS.ESS0 )

    model.hideOutput(True)#The output commamnd

    model.optimize()

    sol=model.getStatus()

    if sol=='optimal':
       Ig_real = [ ]#The start-up and shut down of diesel generators
       Pg_real = [ ]#Output of generators
       Ru_dg_real = [ ]#Ramp up reserve
       Rd_dg_real = [ ]#Ramp down reserve
       DG_fuel_real = [ ]#Fuel state of the diesel generators

       Iug_real = [ ]
       Pug_real = [ ]
       Ru_ug_real = [ ]
       Rd_ug_real = [ ]

       Pess_real = [ ]
       SOC_real = [ ]
       Ru_ess_real = [ ]
       Rd_ess_real = [ ]

       Pbic_real = [ ]
       Iess_real = [ ]

       Reserve_demand_real = [ ]

       Iac_real = [ ]
       Idc_real = [ ]
       Iac_nc_real = [ ]
       Idc_nc_real = [ ]

       Iwp_real = [ ]
       Ipv_real = [ ]

       result = unit_committment_result()

       for i in range(T):
          Ig_real.append(round(model.getVal(Ig[i])))
          Pg_real.append(model.getVal(Pg[i]))
          Ru_dg_real.append(model.getVal(Ru_dg[i]))
          Rd_dg_real.append( model.getVal(Rd_dg[i]))
          DG_fuel_real.append( model.getVal(DG_fuel[i]))

          Iug_real.append(round(model.getVal(Iug[i])))
          Pug_real.append(model.getVal(Pug[i]))
          Ru_ug_real.append(model.getVal(Ru_ug[i]))
          Rd_ug_real.append( model.getVal(Rd_ug[i]))

          Pess_real.append( model.getVal(Pess_dc[i])-model.getVal(Pess_c[i]))
          SOC_real.append( model.getVal(Eess[i])/ESS.Capacity)
          Ru_ess_real.append( model.getVal(Ru_ess[i]))
          Rd_ess_real.append( model.getVal(Rd_ess[i]))

          Iac_real.append(0)
          Idc_real.append(0)
          Iac_nc_real.append(0)
          Idc_nc_real.append(0)

          Reserve_demand_real.append(Reserve_demand[i])

          Pbic_real.append(model.getVal(Pd2a[i])-model.getVal(Pa2d[i]))
          Iess_real.append(model.getVal(Iess[i]))

          Iwp_real.append(0)
          Ipv_real.append(0)

       result.Ig = Ig_real
       result.Pg = Pg_real
       result.Iug = Iug_real
       result.Pug = Pug_real
       result.Pess = Pess_real
       result.SOC = SOC_real

       result.Pbic = Pbic_real

       result.Iac = Iac_real
       result.Iac_nc = Iac_nc_real
       result.Idc = Idc_real
       result.Idc_nc = Idc_nc_real
       result.Ipv = Ipv_real
       result.Iwp = Iwp_real
       result.Solution_status = sol
    else:
     #Run the recovery model
        result=recovery_uc.unit_committment_recovery(DG,BIC,ESS,UG,WP,PV,LOAD_AC,LOAD_AC_NC,
                                                     LOAD_DC,LOAD_DC_NC,RESOURCE_MANAGER,UNCERTAINTY,
                                                     Load_profile_ac, Load_profile_ac_nc,Load_profile_dc,
                                                     Load_profile_dc_nc,PV_profile,WP_profile,Price_profile)


    return result
