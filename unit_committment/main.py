#This is based on the interval unit committment
#The UC is modelling and solved with SCIP solvers
#This is a test function to show that UC can be applied here
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from default_setting import default_local_database

from default_setting import default_time

from data_management.database_format import Unit_committment_day_ahead_database_format,Unit_committment_intra_day_database_format

import modelling.modelling_local_ems as ems_models

import unit_committment.unit_committment_non_tracing as uc_day_ahead

import unit_committment.unit_committment_tracing as uc_intra_day
#1)Obtain the forecasting data from database

def unit_committment_day_ahead(Start_time = default_time.Base_time,
                               DG = ems_models.Diesel_generator_type_I_model(),
                               UG = ems_models.Utility_grid_model(),
                               PV = ems_models.Photovoltaic_generation_model(),
                               WP = ems_models.Wind_turbine_model(),
                               ESS = ems_models.Energy_storage_system_model(),
                               BIC = ems_models.Bi_directional_conveter_model(),
                               LOAD_AC = ems_models.AC_load_type_I_model(),
                               LOAD_AC_NC = ems_models.AC_load_type_I_model(),
                               LOAD_DC = ems_models.DC_load_type_I_model(),
                               LOAD_DC_NC = ems_models.DC_load_type_I_model(),
                               RESOURCE_MANAGER = ems_models.Resource_manager_model(),
                               UNCERTAINTY = ems_models.Uncertainty_model(),
                               Load_profile_ac=[2.77124, 2.80655, 2.83889, 2.88904, 3.02493, 3.22008],
                               Load_profile_ac_nc=[2.77124, 2.80655, 2.83889, 2.88904, 3.02493, 3.22008],
                               Load_profile_dc=[0.253247, 0.253413, 0.263015, 0.254706, 2.49743, 2.92667],
                               Load_profile_dc_nc=[0.253247, 0.253413, 0.263015, 0.254706, 2.49743, 2.92667],
                               PV_profile=[0, 0, 0, 0, 0, 0],
                               WP_profile=[0, 0, 0, 0, 0, 0],
                               Price_profile=[0.08, 0.08, 0.08, 0.08, 0.08, 0.08]):
   #The default databse settings
   T = len(Load_profile_ac)
   db_str = default_local_database.db_str
   engine = create_engine(db_str, echo=False)
   Session = sessionmaker(bind=engine)
   session = Session()
   #4)Obtain the solution

   result_uc = uc_day_ahead.unit_committment(DG,UG,PV,WP,ESS,BIC,LOAD_AC,LOAD_AC_NC,LOAD_DC,LOAD_DC_NC,RESOURCE_MANAGER,
                                            UNCERTAINTY,Load_profile_ac,Load_profile_ac_nc,Load_profile_dc,Load_profile_dc_nc,
                                            PV_profile,WP_profile,Price_profile)

   for i in range(T):
       Unit_committment_day_ahead_result = Unit_committment_day_ahead_database_format(TIME_STAMP = Start_time + i * default_time.Time_step_uc,
                                                                                      CAC3PL_ACTPOW = Load_profile_ac[i],
                                                                                      UCAC3PL_ACTPOW = Load_profile_ac_nc[i],
                                                                                      CDCL_POW = Load_profile_dc[i],
                                                                                      UCDCL_POW = Load_profile_dc_nc[i],
                                                                                      PV_CAP = RESOURCE_MANAGER.PV_CAP,
                                                                                      PV_FORECASTING = PV_profile[i],
                                                                                      WP_CAP = RESOURCE_MANAGER.WP_CAP,
                                                                                      WP_FORECASTING = WP_profile[i],
                                                                                      ELEC_PRICE = Price_profile[i],
                                                                                      DG_STATUS = result_uc.Ig[i],
                                                                                      DG_ACTPOW = result_uc.Pg[i],
                                                                                      DG_FUEL = result_uc.DG_fuel[i],
                                                                                      UG_STATUS = result_uc.Iug[i],
                                                                                      UG_X_ACTPOW = result_uc.Pug[i],
                                                                                      BAT_POW = result_uc.Pess[i],
                                                                                      BAT_SOC = result_uc.SOC[i],
                                                                                      BIC_ACTPOW = result_uc.Pbic[i],
                                                                                      PV_CURTAIL = result_uc.Ipv[i],
                                                                                      WP_CURTAIL = result_uc.Iwp[i],
                                                                                      CAC3PL_SHED = result_uc.Iac[i],
                                                                                      UCAC3PL_SHED = result_uc.Iac_nc[i],
                                                                                      CDCL_SHED = result_uc.Idc[i],
                                                                                      UCDCL_SHED = result_uc.Idc_nc[i])

       if session.query(Unit_committment_day_ahead_database_format).filter_by(TIME_STAMP = (Start_time + i *default_time.Time_step_uc)).count() == 0:#if the data do not exist, the data will be inserted
          session.add(Unit_committment_day_ahead_result)
          session.commit()
       else:#if the data has already existed, this data will be updated.
          row = session.query(Unit_committment_day_ahead_database_format).filter_by(TIME_STAMP = (Start_time+i *default_time.Time_step_uc)).first()
          row.CAC3PL_ACTPOW = Load_profile_ac[i]
          row.UCAC3PL_ACTPOW = Load_profile_ac_nc[i]
          row.CDCL_POW = Load_profile_dc[i]
          row.UCDCL_POW = Load_profile_dc_nc[i]
          row.PV_CAP = RESOURCE_MANAGER.PV_CAP
          row.PV_FORECASTING = PV_profile[i]
          row.WP_CAP = RESOURCE_MANAGER.PV_CAP
          row.WP_FORECASTING = WP_profile[i]
          row.ELEC_PRICE = Price_profile[i]
          row.DG_STATUS = result_uc.Ig[i]
          row.DG_ACTPOW = result_uc.Pg[i]
          row.DG_FUEL = result_uc.DG_fuel[i],
          row.UG_STATUS = result_uc.Iug[i]
          row.UG_X_ACTPOW = result_uc.Pug[i]
          row.BAT_POW = result_uc.Pess[i]
          row.BAT_SOC = result_uc.SOC[i]
          row.BIC_ACTPOW = result_uc.Pbic[i]
          row.PV_CURTAIL = result_uc.Ipv[i]
          row.WP_CURTAIL = result_uc.Iwp[i]
          row.CAC3PL_SHED = result_uc.Iac[i],
          row.UCAC3PL_SHED = result_uc.Iac_nc[i]
          row.CDCL_SHED = result_uc.Idc[i]
          row.UCDCL_SHED = result_uc.Idc_nc[i]
          session.commit()

   session.close()

   return  result_uc.Solution_status

#For the intraday unit committment

def unit_committment_intra_day(Start_time = default_time.Base_time,
                     DG = ems_models.Diesel_generator_type_I_model(),
                     UG = ems_models.Utility_grid_model(),
                     PV = ems_models.Photovoltaic_generation_model(),
                     WP = ems_models.Wind_turbine_model(),
                     ESS = ems_models.Energy_storage_system_model(),
                     BIC = ems_models.Bi_directional_conveter_model(),
                     LOAD_AC = ems_models.AC_load_type_I_model(),
                     LOAD_AC_NC = ems_models.AC_load_type_I_model(),
                     LOAD_DC = ems_models.DC_load_type_I_model(),
                     LOAD_DC_NC = ems_models.DC_load_type_I_model(),
                     RESOURCE_MANAGER = ems_models.Resource_manager_model(),
                     UNCERTAINTY = ems_models.Uncertainty_model(),
                     UC_day_ahead = ems_models.unit_committment_day_ahead2unit_committment_intra_day(),
                     Load_profile_ac=[2.77124, 2.80655, 2.83889, 2.88904, 3.02493, 3.22008],
                     Load_profile_ac_nc=[2.77124, 2.80655, 2.83889, 2.88904, 3.02493, 3.22008],
                     Load_profile_dc=[0.253247, 0.253413, 0.263015, 0.254706, 2.49743, 2.92667],
                     Load_profile_dc_nc=[0.253247, 0.253413, 0.263015, 0.254706, 2.49743, 2.92667],
                     PV_profile=[0, 0, 0, 0, 0, 0],
                     WP_profile=[0, 0, 0, 0, 0, 0],
                     Price_profile=[0.08, 0.08, 0.08, 0.08, 0.08, 0.08]):
   #The default databse settings
   T=len(Load_profile_ac)
   db_str = default_local_database.db_str
   engine = create_engine(db_str, echo=False)
   Session = sessionmaker(bind=engine)
   session = Session()
   #4)Obtain the solution

   result_uc= uc_intra_day.unit_committment(DG,UG,PV,WP,ESS,BIC,LOAD_AC,LOAD_AC_NC,LOAD_DC,LOAD_DC_NC,RESOURCE_MANAGER,
                                            UNCERTAINTY,UC_day_ahead,Load_profile_ac,Load_profile_ac_nc,Load_profile_dc,Load_profile_dc_nc,
                                            PV_profile,WP_profile,Price_profile)

   db_str = default_local_database.db_str
   engine = create_engine(db_str, echo=False)

   Session = sessionmaker(bind=engine)

   session = Session()

   for i in range(T):
       Unit_committment_intra_day_result=Unit_committment_intra_day_database_format(TIME_STAMP=Start_time+(i-1)*default_time.Time_step_uc,
                                                                                    CAC3PL_ACTPOW=Load_profile_ac[i],
                                                                                    UCAC3PL_ACTPOW=Load_profile_ac_nc[i],
                                                                                    CDCL_POW=Load_profile_dc[i],
                                                                                    UCDCL_POW=Load_profile_dc_nc[i],
                                                                                    PV_CAP=RESOURCE_MANAGER.PV_CAP,
                                                                                    PV_FORECASTING=PV_profile[i],
                                                                                    WP_CAP=RESOURCE_MANAGER.WP_CAP,
                                                                                    WP_FORECASTING=WP_profile[i],
                                                                                    ELEC_PRICE=Price_profile[i],
                                                                                    DG_STATUS=result_uc.Ig[i],
                                                                                    DG_ACTPOW=result_uc.Pg[i],
                                                                                    DG_FUEL=result_uc.DG_fuel[i],
                                                                                    UG_STATUS=result_uc.Iug[i],
                                                                                    UG_X_ACTPOW=result_uc.Pug[i],
                                                                                    BAT_POW=result_uc.Pess[i],
                                                                                    BAT_SOC=result_uc.SOC[i],
                                                                                    BIC_ACTPOW=result_uc.Pbic[i],
                                                                                    PV_CURTAIL=result_uc.Ipv[i],
                                                                                    WP_CURTAIL=result_uc.Iwp[i],
                                                                                    CAC3PL_SHED=result_uc.Iac[i],
                                                                                    UCAC3PL_SHED=result_uc.Iac_nc[i],
                                                                                    CDCL_SHED=result_uc.Idc[i],
                                                                                    UCDCL_SHED=result_uc.Idc_nc[i])

       if session.query(Unit_committment_intra_day_database_format).filter_by(TIME_STAMP = (Start_time+i *default_time.Time_step_uc)).count() == 0:#if the data do not exist, the data will be inserted
          session.add(Unit_committment_intra_day_result)
          session.commit()
       else:#if the data has already existed, this data will be updated.
          row=session.query(Unit_committment_intra_day_database_format).filter_by(TIME_STAMP = (Start_time+i *default_time.Time_step_uc)).first()
          row.CAC3PL_ACTPOW = Load_profile_ac[i]
          row.UCAC3PL_ACTPOW = Load_profile_ac_nc[i]
          row.CDCL_POW = Load_profile_dc[i]
          row.UCDCL_POW = Load_profile_dc_nc[i]
          row.PV_CAP = RESOURCE_MANAGER.PV_CAP
          row.PV_FORECASTING = PV_profile[i]
          row.WP_CAP = RESOURCE_MANAGER.WP_CAP
          row.WP_FORECASTING = WP_profile[i]
          row.ELEC_PRICE = Price_profile[i]
          row.DG_STATUS = result_uc.Ig[i]
          row.DG_ACTPOW = result_uc.Pg[i]
          row.DG_FUEL = result_uc.DG_fuel[i],
          row.UG_STATUS = result_uc.Iug[i]
          row.UG_X_ACTPOW = result_uc.Pug[i]
          row.BAT_POW = result_uc.Pess[i]
          row.BAT_SOC = result_uc.SOC[i]
          row.BIC_ACTPOW = result_uc.Pbic[i]
          row.PV_CURTAIL = result_uc.Ipv[i]
          row.WP_CURTAIL = result_uc.Iwp[i]
          row.CAC3PL_SHED = result_uc.Iac[i],
          row.UCAC3PL_SHED = result_uc.Iac_nc[i]
          row.CDCL_SHED = result_uc.Idc[i]
          row.UCDCL_SHED = result_uc.Idc_nc[i]
          session.commit()

   return  result_uc.Solution_status


if __name__=="__main__":
    Start_time = default_time.Base_time
    DG = ems_models.Diesel_generator_type_I_model()
    UG = ems_models.Utility_grid_model()
    PV = ems_models.Photovoltaic_generation_model()
    WP = ems_models.Wind_turbine_model()
    ESS = ems_models.Energy_storage_system_model()
    BIC = ems_models.Bi_directional_conveter_model()
    LOAD_AC = ems_models.AC_load_type_I_model()
    LOAD_AC_NC = ems_models.AC_load_type_I_model()
    LOAD_DC = ems_models.DC_load_type_I_model()
    LOAD_DC_NC = ems_models.DC_load_type_I_model()
    RESOURCE_MANAGER = ems_models.Resource_manager_model()
    UNCERTAINTY = ems_models.Uncertainty_model()
    Load_profile_ac = [2.77124, 2.80655, 2.83889, 2.88904, 3.02493, 3.22008]
    Load_profile_ac_nc = [2.77124, 2.80655, 2.83889, 2.88904, 3.02493, 3.22008]
    Load_profile_dc = [0.253247, 0.253413, 0.263015, 0.254706, 2.49743, 2.92667]
    Load_profile_dc_nc = [0.253247, 0.253413, 0.263015, 0.254706, 2.49743, 2.92667]
    PV_profile = [0, 0, 0, 0, 0, 0]
    WP_profile = [0, 0, 0, 0, 0, 0]
    Price_profile = [0.08, 0.08, 0.08, 0.08, 0.08, 0.08]

    result_uc= unit_committment_day_ahead(Start_time,DG,UG,PV,WP,ESS,BIC,LOAD_AC,LOAD_AC_NC,LOAD_DC,LOAD_DC_NC,RESOURCE_MANAGER,
                                          UNCERTAINTY,Load_profile_ac,Load_profile_ac_nc,Load_profile_dc,Load_profile_dc_nc,
                                          PV_profile,WP_profile,Price_profile)

    print("The unit committment test result are shown as follows", result_uc)
