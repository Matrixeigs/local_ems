#This function is the data prepration for the dynamic economic dispatch
#Date: 19/Feb/2017
#Author: Zhao TY
#This should be a general query function for UC and ED
from data_management.database_format import Unit_committment_day_ahead_database_format,Day_ahead_forecasting
from sqlalchemy import create_engine, and_
from sqlalchemy.orm import sessionmaker
from default_setting import default_local_database
from default_setting import default_time
from modelling.modelling_local_ems import unit_committment_day_ahead2unit_committment_intra_day,default_resourcec_manager_parameters

#The information from resource manager

def unit_committment_intra_day_set_up(Time=default_time.Base_time,
                                      Look_ahead_time_uc=default_time.Look_ahead_time_uc_intra_day):
    if Time % 3600 == 0:  # The operation of ED is to realize for half an hour later
        Start_time_uc = Time  # The qurey model
    else:
        print('This is not the implement time of unit committment')
        Start_time_uc = Time- Time%default_time.Time_step_uc  # The qurey model

    #Local the base time of this time slot
    Base_time_uc = Start_time_uc
    Base_time_uc_look_ahead = Base_time_uc + Look_ahead_time_uc

    T = round(Look_ahead_time_uc/default_time.Time_step_uc)


    db_str = default_local_database.db_str
    engine = create_engine(db_str, echo=False)
    Session = sessionmaker(bind=engine)
    session = Session()

    Ig = session.query(Unit_committment_day_ahead_database_format.DG_STATUS).filter(
        and_(Unit_committment_day_ahead_database_format.TIME_STAMP >= Base_time_uc,
             Unit_committment_day_ahead_database_format.TIME_STAMP <= Base_time_uc_look_ahead)).all()

    Pg = session.query(Unit_committment_day_ahead_database_format.DG_ACTPOW).filter(
        and_(Unit_committment_day_ahead_database_format.TIME_STAMP >= Base_time_uc,
             Unit_committment_day_ahead_database_format.TIME_STAMP <= Base_time_uc_look_ahead)).all()

    DG_fuel = session.query(Unit_committment_day_ahead_database_format.DG_FUEL).filter(
        and_(Unit_committment_day_ahead_database_format.TIME_STAMP >= Base_time_uc,
             Unit_committment_day_ahead_database_format.TIME_STAMP <= Base_time_uc_look_ahead)).all()

    Iug = session.query(Unit_committment_day_ahead_database_format.UG_STATUS).filter(
        and_(Unit_committment_day_ahead_database_format.TIME_STAMP >= Base_time_uc,
             Unit_committment_day_ahead_database_format.TIME_STAMP <= Base_time_uc_look_ahead)).all()

    Pug = session.query(Unit_committment_day_ahead_database_format.UG_X_ACTPOW).filter(
        and_(Unit_committment_day_ahead_database_format.TIME_STAMP >= Base_time_uc,
             Unit_committment_day_ahead_database_format.TIME_STAMP <= Base_time_uc_look_ahead)).all()

    SOC = session.query(Unit_committment_day_ahead_database_format.BAT_SOC).filter(
        and_(Unit_committment_day_ahead_database_format.TIME_STAMP >= Base_time_uc,
             Unit_committment_day_ahead_database_format.TIME_STAMP <= Base_time_uc_look_ahead)).all()

    Iac = session.query(Unit_committment_day_ahead_database_format.CAC3PL_SHED).filter(
        and_(Unit_committment_day_ahead_database_format.TIME_STAMP >= Base_time_uc,
             Unit_committment_day_ahead_database_format.TIME_STAMP <= Base_time_uc_look_ahead)).all()

    Iac_nc = session.query(Unit_committment_day_ahead_database_format.UCAC3PL_SHED).filter(
        and_(Unit_committment_day_ahead_database_format.TIME_STAMP >= Base_time_uc,
             Unit_committment_day_ahead_database_format.TIME_STAMP <= Base_time_uc_look_ahead)).all()

    Idc = session.query(Unit_committment_day_ahead_database_format.CDCL_SHED).filter(
        and_(Unit_committment_day_ahead_database_format.TIME_STAMP >= Base_time_uc,
             Unit_committment_day_ahead_database_format.TIME_STAMP <= Base_time_uc_look_ahead)).all()

    Idc_nc = session.query(Unit_committment_day_ahead_database_format.UCDCL_SHED).filter(
        and_(Unit_committment_day_ahead_database_format.TIME_STAMP >= Base_time_uc,
             Unit_committment_day_ahead_database_format.TIME_STAMP <= Base_time_uc_look_ahead)).all()

    Ipv = session.query(Unit_committment_day_ahead_database_format.PV_CURTAIL).filter(
        and_(Unit_committment_day_ahead_database_format.TIME_STAMP >= Base_time_uc,
             Unit_committment_day_ahead_database_format.TIME_STAMP <= Base_time_uc_look_ahead)).all()

    Iwp = session.query(Unit_committment_day_ahead_database_format.WP_CURTAIL).filter(
        and_(Unit_committment_day_ahead_database_format.TIME_STAMP >= Base_time_uc,
             Unit_committment_day_ahead_database_format.TIME_STAMP <= Base_time_uc_look_ahead)).all()

    session.close()

    result = unit_committment_day_ahead2unit_committment_intra_day()

    Ig_real = [ ]
    Pg_real = [ ]
    DG_fuel_real = [ ]
    Iug_real = [ ]
    Pug_real = [ ]
    SOC_real = [ ]
    Iac_real = [ ]
    Idc_real = [ ]
    Iac_nc_real = [ ]
    Idc_nc_real =  [ ]
    Ipv_real = [ ]
    Iwp_real = [ ]
    for i in range(T):
        Ig_real.append(Ig[i][0])
        Pg_real.append(Pg[i][0] )
        DG_fuel_real.append(DG_fuel[i][0])
        Iug_real.append(Iug[i][0])
        Pug_real.append(Pug[i][0])
        SOC_real.append(SOC[i][0])

        Iac_real.append(Iac[i][0])
        Idc_real.append(Idc[i][0])

        Iac_nc_real.append(Iac_nc[i][0])
        Idc_nc_real.append(Idc_nc[i][0])
        Ipv_real.append(Ipv[i][0])
        Iwp_real.append(Iwp[i][0])

    result.Ig=Ig_real
    result.Pg = Pg_real
    result.DG_fuel = DG_fuel_real

    result.Iug = Iug_real
    result.Pug = Pug_real

    result.SOC = SOC_real
    result.Iac = Iac_real
    result.Iac_nc = Iac_nc_real
    result.Idc = Idc_real
    result.Idc_nc= Idc_nc_real
    result.Ipv = Ipv_real
    result.Iwp = Iwp_real

    #According to the time
    return result

def forecasting_result_query(Time = default_time.Base_time,
                             Look_ahead_time_uc = default_time.Look_ahead_time_uc_intra_day,
                             RESOURCE_MANAGER = default_resourcec_manager_parameters()):
    if Time % 3600 == 0:  # The operation of ED is to realize for half an hour later
        Start_time_uc = Time  # The qurey model
    else:
        print('This is not the implement time of economic dispatch')
        Start_time_uc = Time- Time%default_time.Time_step_uc  # The qurey model

    End_time_uc = Start_time_uc + Look_ahead_time_uc

    db_str = default_local_database.db_str
    engine = create_engine(db_str, echo=False)
    Session = sessionmaker(bind=engine)
    session = Session()

    Load_ac_profile = session.query(Day_ahead_forecasting.CAC3PL_ACTPOW).filter(
        and_(Day_ahead_forecasting.TIME_STAMP >= Start_time_uc,
             Day_ahead_forecasting.TIME_STAMP < End_time_uc)).all()

    Load_ac_nc_profile = session.query(Day_ahead_forecasting.UCAC3PL_ACTPOW).filter(
        and_(Day_ahead_forecasting.TIME_STAMP >= Start_time_uc,
             Day_ahead_forecasting.TIME_STAMP < End_time_uc)).all()

    Load_dc_profile = session.query(Day_ahead_forecasting.CDCL_POW).filter(
        and_(Day_ahead_forecasting.TIME_STAMP >= Start_time_uc,
             Day_ahead_forecasting.TIME_STAMP < End_time_uc)).all()

    Load_dc_nc_profile = session.query(Day_ahead_forecasting.UCDCL_POW).filter(
        and_(Day_ahead_forecasting.TIME_STAMP >= Start_time_uc,
             Day_ahead_forecasting.TIME_STAMP < End_time_uc)).all()

    WP_profile = session.query(Day_ahead_forecasting.WP_FORECASTING).filter(
        and_(Day_ahead_forecasting.TIME_STAMP >= Start_time_uc,
             Day_ahead_forecasting.TIME_STAMP < End_time_uc)).all()

    PV_profile = session.query(Day_ahead_forecasting.PV_FORECASTING).filter(
        and_(Day_ahead_forecasting.TIME_STAMP >= Start_time_uc,
             Day_ahead_forecasting.TIME_STAMP < End_time_uc)).all()

    Price_profile = session.query(Day_ahead_forecasting.ELEC_PRICE).filter(
        and_(Day_ahead_forecasting.TIME_STAMP >= Start_time_uc,
             Day_ahead_forecasting.TIME_STAMP < End_time_uc)).all()

    session.close()

    Load_ac_profile_real = [ ]
    Load_ac_nc_profile_real = [ ]
    Load_dc_profile_real = [ ]
    Load_dc_nc_profile_real = [ ]
    WP_profile_real = [ ]
    PV_profile_real = [ ]
    Price_profile_real = [ ]

    for i in range (len(Load_ac_profile)):
      Load_ac_profile_real.append(Load_ac_profile[i][0] * RESOURCE_MANAGER.CAC3PL_ACTPOW_CAP)
      Load_ac_nc_profile_real.append(Load_ac_nc_profile[i][0]* RESOURCE_MANAGER.UCAC3PL_ACTPOW_CAP)
      Load_dc_profile_real.append(Load_dc_profile[i][0]* RESOURCE_MANAGER.CDCL_POW_CAP)
      Load_dc_nc_profile_real.append(Load_dc_nc_profile[i][0]* RESOURCE_MANAGER.UCDCL_POW_CAP)
      WP_profile_real.append(WP_profile[i][0]* RESOURCE_MANAGER.WP_CAP)
      PV_profile_real.append(PV_profile[i][0]* RESOURCE_MANAGER.PV_CAP)
      Price_profile_real.append(Price_profile[i][0])

    return Load_ac_profile_real,Load_ac_nc_profile_real,Load_dc_profile_real,Load_dc_nc_profile_real,WP_profile_real,PV_profile_real,\
           Price_profile_real