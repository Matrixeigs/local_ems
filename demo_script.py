#This script is the demonstration of the RTC

from default_setting import default_resources_settings
from default_setting import default_time
from data_management.database_format import Real_time_operation_database_format
from data_management.database_format import Optimal_power_flow_database_format
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from default_setting import default_local_database

db_str=default_local_database.db_str

for Time in range(default_time.Base_time, default_time.Base_time + 8760*3600):
    if Time % default_time.Time_step_ed == 0:
        engine = create_engine(db_str, echo=False)
        Session = sessionmaker(bind=engine)
        session = Session()
        #The results in optimal power flow
        if session.query(Optimal_power_flow_database_format).filter_by(TIME_STAMP=Time).count()==0:
            updated_items = Real_time_operation_database_format(TIME_STAMP = Time,
                                                                DG_ACTPOW = 1.00 * default_resources_settings.BASE_VA,
                                                                UG_X_ACTPOW = 0)
            if session.query(Real_time_operation_database_format).filter_by(TIME_STAMP=Time).count() == 0:
                session.add(updated_items)
            else:
                row = session.query(Real_time_operation_database_format).filter_by(TIME_STAMP=Time).first()
                row.DG_ACTPOW = 1.00 * default_resources_settings.BASE_VA
                row.UG_X_ACTPOW = 0.00

        else:
            optimal_power_flow_result=session.query(Optimal_power_flow_database_format).filter_by(TIME_STAMP=Time).first()
            updated_items = Real_time_operation_database_format(TIME_STAMP=Time,
                                                            DG_ACTPOW= optimal_power_flow_result.UG_X_ACTPOW * default_resources_settings.BASE_VA,
                                                            UG_X_ACTPOW= 0)

            if session.query(Real_time_operation_database_format).filter_by(TIME_STAMP=Time).count() == 0:
                session.add(updated_items)
            else:
                row = session.query(Real_time_operation_database_format).filter_by(TIME_STAMP=Time).first()
                row.DG_ACTPOW = optimal_power_flow_result.UG_X_ACTPOW * default_resources_settings.BASE_VA
                row.UG_X_ACTPOW = 0.00

        session.commit()