#This function is the data prepration for the dynamic economic dispatch
#Date: 16/Feb/2017
#Author: Zhao TY
#This should be a general query function for UC and ED

from data_management.database_format import Real_time_forecasting,Economic_dispatch_database_format
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from default_setting import default_local_database
from default_setting import default_time
from modelling.modelling_local_ems import default_resourcec_manager_parameters
from modelling.modelling_local_ems import economic_dispatch2optimal_power_flow_information
#The information from resource manager

def optimla_power_flow_command_from_economic_dispatch(Time=default_time.Base_time ,):
    if Time % 300 == 0:  # The operation of ED is to realize for half an hour later
        Start_time_opf = Time  # The qurey model
    else:
        print('This is not the implement time of optimal power flow')
        Start_time_opf = Time- Time%default_time.Time_step_ed + default_time.Time_step_ed  # The qurey model

    #Local the base time of this time slot

    db_str = default_local_database.db_str
    engine = create_engine(db_str, echo=False)
    Session = sessionmaker(bind=engine)
    session = Session()

    Ig = session.query(Economic_dispatch_database_format.DG_STATUS).filter(Economic_dispatch_database_format.TIME_STAMP == Start_time_opf).first()

    Pg = session.query(Economic_dispatch_database_format.DG_ACTPOW).filter(Economic_dispatch_database_format.TIME_STAMP == Start_time_opf).first()

    DG_fuel = session.query(Economic_dispatch_database_format.DG_FUEL).filter(Economic_dispatch_database_format.TIME_STAMP == Start_time_opf).first()

    Iug = session.query(Economic_dispatch_database_format.UG_STATUS).filter(Economic_dispatch_database_format.TIME_STAMP == Start_time_opf).first()

    Pug = session.query(Economic_dispatch_database_format.UG_X_ACTPOW).filter(Economic_dispatch_database_format.TIME_STAMP == Start_time_opf).first()

    SOC = session.query(Economic_dispatch_database_format.BAT_SOC).filter(Economic_dispatch_database_format.TIME_STAMP == Start_time_opf).first()

    Iac = session.query(Economic_dispatch_database_format.CAC3PL_SHED).filter(Economic_dispatch_database_format.TIME_STAMP == Start_time_opf).first()

    Iac_nc = session.query(Economic_dispatch_database_format.UCAC3PL_SHED).filter(Economic_dispatch_database_format.TIME_STAMP == Start_time_opf).first()

    Idc = session.query(Economic_dispatch_database_format.CDCL_SHED).filter(Economic_dispatch_database_format.TIME_STAMP == Start_time_opf).first()

    Idc_nc = session.query(Economic_dispatch_database_format.UCDCL_SHED).filter(Economic_dispatch_database_format.TIME_STAMP == Start_time_opf).first()

    Ipv = session.query(Economic_dispatch_database_format.PV_CURTAIL).filter(Economic_dispatch_database_format.TIME_STAMP == Start_time_opf).first()

    Iwp = session.query(Economic_dispatch_database_format.WP_CURTAIL).filter(Economic_dispatch_database_format.TIME_STAMP == Start_time_opf).first()

    session.close()

    result = economic_dispatch2optimal_power_flow_information()


    result.Ig = Ig[0]
    result.Pg = Pg[0]
    result.DG_fuel = DG_fuel[0]

    result.Iug = Iug[0]
    result.Pug = Pug[0]

    result.SOC = SOC[0]

    result.Iac = Iac[0]
    result.Iac_nc = Iac_nc[0]
    result.Idc = Idc[0]
    result.Idc_nc = Idc_nc[0]

    result.Ipv = Ipv[0]
    result.Iwp = Iwp[0]

    #According to the time
    return result

def optimal_power_flow_information_query(Time = default_time.Base_time, RESOURCE_MANAGER = default_resourcec_manager_parameters()):
    if Time % 300 == 0:  # The operation of ED is to realize for half an hour later
        Start_time_opf = Time  # The qurey model
    else:
        print('This is not the implement time of optimal power flow')
        Start_time_opf = Time- Time%default_time.Time_step_ed + default_time.Time_step_ed  # The qurey model

    db_str = default_local_database.db_str
    engine = create_engine(db_str, echo=False)
    Session = sessionmaker(bind=engine)
    session = Session()

    Load_ac_profile = session.query(Real_time_forecasting.CAC3PL_ACTPOW).filter(Real_time_forecasting.TIME_STAMP == Start_time_opf).first()

    Load_ac_nc_profile = session.query(Real_time_forecasting.UCAC3PL_ACTPOW).filter(Real_time_forecasting.TIME_STAMP == Start_time_opf).first()

    Load_dc_profile = session.query(Real_time_forecasting.CDCL_POW).filter(Real_time_forecasting.TIME_STAMP == Start_time_opf).first()

    Load_dc_nc_profile = session.query(Real_time_forecasting.UCDCL_POW).filter(Real_time_forecasting.TIME_STAMP == Start_time_opf).first()

    WP_profile = session.query(Real_time_forecasting.WP_FORECASTING).filter(Real_time_forecasting.TIME_STAMP == Start_time_opf).first()

    PV_profile = session.query(Real_time_forecasting.PV_FORECASTING).filter(Real_time_forecasting.TIME_STAMP == Start_time_opf).first()

    Price_profile = session.query(Real_time_forecasting.ELEC_PRICE).filter(Real_time_forecasting.TIME_STAMP == Start_time_opf).first()

    session.close()

    Load_ac_profile_real = Load_ac_profile[0] * RESOURCE_MANAGER.CAC3PL_ACTPOW_CAP
    Load_ac_nc_profile_real = Load_ac_nc_profile[0]* RESOURCE_MANAGER.UCAC3PL_ACTPOW_CAP
    Load_dc_profile_real = Load_dc_profile[0]* RESOURCE_MANAGER.CDCL_POW_CAP
    Load_dc_nc_profile_real = Load_dc_nc_profile[0]* RESOURCE_MANAGER.UCDCL_POW_CAP
    WP_profile_real = WP_profile[0]* RESOURCE_MANAGER.WP_CAP
    PV_profile_real = PV_profile[0]* RESOURCE_MANAGER.PV_CAP
    Price_profile_real = Price_profile[0]

    return Load_ac_profile_real,Load_ac_nc_profile_real,Load_dc_profile_real,Load_dc_nc_profile_real,WP_profile_real,\
           PV_profile_real,Price_profile_real
