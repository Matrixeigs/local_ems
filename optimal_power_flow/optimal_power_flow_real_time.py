#This functions is to gurantee the secure operation of BIC
#This is still based on the cvxopt, in the further, this function will be replaced with scipopt
from cvxopt.modeling import variable, op
from cvxopt import solvers
from math import acos, tan, ceil
from default_setting import default_time
import modelling.modelling_local_ems as modelling_ems
from default_setting import default_resources_settings
from modelling.modelling_local_ems import optimal_power_flow_result
from default_setting import default_penalty_factors

def optimal_power_flow_single_step(DG = modelling_ems.Diesel_generator_type_I_model(),
                                   UG = modelling_ems.Utility_grid_model(),
                                   PV = modelling_ems.Photovoltaic_generation_model(),
                                   WP = modelling_ems.Wind_turbine_model(),
                                   ESS = modelling_ems.Energy_storage_system_model(),
                                   BIC = modelling_ems.Bi_directional_conveter_model(),
                                   LOAD_AC = modelling_ems.AC_load_type_I_model(),
                                   LOAD_AC_NC = modelling_ems.AC_load_type_I_model(),
                                   LOAD_DC = modelling_ems.DC_load_type_I_model(),
                                   LOAD_DC_NC = modelling_ems.DC_load_type_I_model(),
                                   RESOURCE_MANAGER = modelling_ems.Resource_manager_model(),
                                   UNCERTAINTY = modelling_ems.Uncertainty_model(),
                                   ED = modelling_ems.economic_dispatch2optimal_power_flow_information(),
                                   Load_profile_ac= 0,
                                   Load_profile_ac_nc = 0,
                                   Load_profile_dc = 0,
                                   Load_profile_dc_nc = 0,
                                   PV_profile = 0,
                                   WP_profile = 0,
                                   Price_profile = 0.08):
    #Define the decision variables firstly
    #Pg,Pug,Pess_c,Pess_dc,Pa2d,Pd2a
    #Ru_dg,Rd_dg,Ru_ug,Rd_ug,Ru_ess,Rd_ess
    #Eess, DG_fuel
    Delta_t = default_time.Time_step_opf

    DG_status = min(RESOURCE_MANAGER.Dg_status, ED.Ig)
    UG_status = min(RESOURCE_MANAGER.Ug_status, ED.Iug)

    Pg_min = DG_status * DG.Pmin
    Pg_max = DG_status * DG.Pmax

    Pg=variable(1)
    Cg_boudary_lower = (Pg >= Pg_min)
    Cg_boudary_upper = (Pg <= Pg_max)

    Qg_min = -DG_status * DG.Capacity
    Qg_max = DG_status * DG.Capacity

    Qg=variable(1)

    CQg_boudary_lower = (Qg >= Qg_min)
    CQg_boudary_upper = (Qg <= Qg_max)
    #CQg_boudary_upper = (Pg+Qg <= Qg_max)

    Ru_dg_min = DG_status * DG.Pmin
    Ru_dg_max = DG_status * DG.Pmax

    Ru_dg = variable(1)
    Ru_dg_boudary_lower = (Ru_dg >= Ru_dg_min)
    Ru_dg_boudary_upper = (Ru_dg <= Ru_dg_max)

    Rd_dg_min = DG_status * DG.Pmin
    Rd_dg_max = DG_status * DG.Pmax

    Rd_dg = variable(1)
    Rd_dg_boudary_lower = (Rd_dg >= Rd_dg_min)
    Rd_dg_boudary_upper = (Rd_dg <= Rd_dg_max)

    Pug_min = UG_status * UG.Pmin
    Pug_max = UG_status * UG.Pmax

    Pug = variable(1)
    Pug_boudary_lower = (Pug >= Pug_min)
    Pug_boudary_upper = (Pug <= Pug_max)


    Qug_min = - UG_status * UG.Pmax * tan(acos(UG.Power_factor_limitation))
    Qug_max = UG_status * UG.Pmax * tan(acos(UG.Power_factor_limitation))

    Qug = variable(1)
    CQug_boudary_lower = (Qug >= Qug_min)
    CQug_boudary_upper = (Qug <= Qug_max)

    Ru_ug_min = UG_status * UG.Pmin
    Ru_ug_max = UG_status * UG.Pmax

    Ru_ug = variable(1)
    Ru_ug_boudary_lower = (Ru_ug >= Ru_ug_min)
    Ru_ug_boudary_upper = (Ru_ug <= Ru_ug_max)

    Rd_ug_min = UG_status * UG.Pmin
    Rd_ug_max = UG_status * UG.Pmax

    Rd_ug = variable(1)
    Rd_ug_boudary_lower = (Rd_ug >= Rd_ug_min)
    Rd_ug_boudary_upper = (Rd_ug <= Rd_ug_max)

    Pess_c_min = 0
    Pess_c_max = ESS.Pmax_charging

    Pess_c = variable(1)
    Pess_c_boudary_lower = (Pess_c >= Pess_c_min)
    Pess_c_boudary_upper = (Pess_c <= Pess_c_max)

    Pess_dc_min = 0
    Pess_dc_max = ESS.Pmax_discharging

    Pess_dc = variable(1)
    Pess_dc_boudary_lower = (Pess_dc >= Pess_dc_min)
    Pess_dc_boudary_upper = (Pess_dc <= Pess_dc_max)

    Ru_ess_min = 0
    Ru_ess_max =  ESS.Pmax_discharging+ESS.Pmax_charging

    Ru_ess = variable(1)
    Ru_ess_boudary_lower = (Ru_ess >= Ru_ess_min)
    Ru_ess_boudary_upper = (Ru_ess <= Ru_ess_max)

    Rd_ess_min = 0
    Rd_ess_max = ESS.Pmax_discharging+ESS.Pmax_charging

    Rd_ess = variable(1)
    Rd_ess_boudary_lower = (Rd_ess >= Rd_ess_min)
    Rd_ess_boudary_upper = (Rd_ess <= Rd_ess_max)

    Pa2d_min = 0
    Pa2d_max = BIC.Capacity

    Pa2d = variable(1)
    Pa2d_boudary_lower = (Pa2d >= Pa2d_min)
    Pa2d_boudary_upper = (Pa2d <= Pa2d_max)

    Pd2a_min = 0
    Pd2a_max = BIC.Capacity

    Pd2a = variable(1)
    Pd2a_boudary_lower = (Pd2a >= Pd2a_min)
    Pd2a_boudary_upper = (Pd2a <= Pd2a_max)

    Qbic = variable(1)
 #   CQbic_boudary = (Pd2a*Pd2a+Qbic*Qbic <= BIC.Capacity**2)

    Eess_min = ESS.SOC_min * ESS.Capacity
    Eess_max = ESS.SOC_max * ESS.Capacity

    Eess = variable(1)
    Eess_boudary_lower = (Eess >= Eess_min)
    Eess_boudary_upper = (Eess <= Eess_max)


    DG_fuel_min = DG.Fuel_limitation
    DG_fuel_max = DG.Fuel0

    DG_fuel = variable(1)
    DG_fuel_boudary_lower = (DG_fuel >= DG_fuel_min)
    DG_fuel_boudary_upper = (DG_fuel <= DG_fuel_max)

    Pl_ac_min = 0
    Pl_ac_max = Load_profile_ac

    Pl_ac = variable(1)
    Pl_ac_boundary_lower = (Pl_ac >= Pl_ac_min)
    Pl_ac_boundary_upper = (Pl_ac <= Pl_ac_max)

    Pl_dc_min = 0
    Pl_dc_max = Load_profile_dc

    Pl_dc = variable(1)
    Pl_dc_boundary_lower = (Pl_dc >= Pl_dc_min)
    Pl_dc_boundary_upper = (Pl_dc <= Pl_dc_max)

    Pl_ac_nc_min = 0
    Pl_ac_nc_max = Load_profile_ac_nc

    Pl_ac_nc = variable(1)
    Pl_ac_nc_boundary_lower = (Pl_ac_nc >= Pl_ac_nc_min)
    Pl_ac_nc_boundary_upper = (Pl_ac_nc <= Pl_ac_nc_max)

    Pl_dc_nc_min = 0
    Pl_dc_nc_max = Load_profile_dc_nc

    Pl_dc_nc = variable(1)
    Pl_dc_nc_boundary_lower = (Pl_dc_nc >= Pl_dc_nc_min)
    Pl_dc_nc_boundary_upper = (Pl_dc_nc <= Pl_dc_nc_max)

    Ppv_min = 0
    Ppv_max = PV_profile

    Ppv = variable(1)
    Ppv_boundary_lower = (Ppv >= Ppv_min)
    Ppv_boundary_upper = (Ppv <= Ppv_max)

    Pwp_min = 0
    Pwp_max = WP_profile

    Pwp = variable(1)
    Pwp_boundary_lower = (Pwp >= Pwp_min)
    Pwp_boundary_upper = (Pwp <= Pwp_max)

    # 3) The power balance constraint
    Power_balance_constraint_ac = (Pg + Pug + Pd2a * BIC.Efficiency_AC2DC - Pa2d == Pl_ac + Pl_ac_nc)

    Power_balance_constraint_dc = (
    Pess_dc - Pess_c + Pa2d * BIC.Efficiency_DC2AC - Pd2a == Pl_dc + Pl_dc_nc - Ppv - Pwp)

    Power_balance_constraint_ac_q = ( Qg + Qbic + Qug == Pl_ac * tan(acos(LOAD_AC.Power_factor)) +Pl_ac_nc * LOAD_AC_NC.Power_factor)

    # 4)Energy tranfer
    Ess_dynamic = (
    Eess == Pess_c * ESS.Efficiency_charging * Delta_t - Pess_dc / ESS.Efficiency_discharging * Delta_t + ESS.ESS0)

    DG_fuel_dynamic = (DG_fuel == DG.Fuel0 - DG_status * DG.b_g * Delta_t - Pg * Delta_t * DG.a_g)

    # 5)Pg+Pu_dg<=Pmax
    Ru_cap = (Pg + Ru_dg <= Pg_max)

    # 6)Pg+Pu_dg<=Pmax
    Rd_cap = (Pg - Rd_dg >= Pg_min)

    # 7) -Pess_c+Pess_dc+Ru_ess<=Pess_dc_max
    Ress_u_cap = (Pess_dc - Pess_c + Ru_ess <= Pess_dc_max)
    # 8) Pess_c-Pess_dc+Rd_ess<=Pess_c_max
    Ress_d_cap = (Pess_c - Pess_dc + Rd_ess <= Pess_c_max)
    # 9)Pug+Ru_ug<=Pmax
    Ru_ug_cap = (Pug + Ru_ug <= Pug_max)
    # 10)Pug-Rd_ug>=Pmin
    Rd_ug_cap = (Pug - Rd_ug >= Pug_min)
    # 11)
    Ru_ess_energy = (Eess - Ru_ess * Delta_t / ESS.Efficiency_discharging >= ESS.SOC_min*ESS.Capacity)
    Rd_ess_energy = (Eess + Rd_ess * Delta_t * ESS.Efficiency_charging <= ESS.SOC_max*ESS.Capacity)
    # 12)
    Ru_dg_energy = (
    DG_fuel - Ru_dg * Delta_t * DG.a_g - DG_status * DG.b_g * Delta_t >= DG.Fuel_limitation)
    # 13)
    # Reserve_demand = LOAD_AC['Forecast'] * UNCERTAINTY['Disturbance_range_ac'] + LOAD_AC_NC['Forecast'] * UNCERTAINTY['Disturbance_range_ac_nc'] + LOAD_DC['Forecast'] * UNCERTAINTY['Disturbance_range_dc'] \
    # + LOAD_DC_NC['Forecast'] * UNCERTAINTY['Disturbance_range_dc_nc'] + PV['Forecast']*UNCERTAINTY['Disturbance_range_pv'] + WP['Forecast']*UNCERTAINTY['Disturbance_range_wp']
    Up_reserve_limitation = (
    Ru_dg + Ru_ug + Ru_ess >= Pl_ac * UNCERTAINTY.Disturbance_range_ac_ed + Pl_ac_nc * UNCERTAINTY.Disturbance_range_ac_nc_ed + Pl_dc * UNCERTAINTY.Disturbance_range_dc_ed+ Pl_dc_nc * UNCERTAINTY.Disturbance_range_dc_nc_ed + Ppv * UNCERTAINTY.Disturbance_range_pv_ed + Pwp * UNCERTAINTY.Disturbance_range_wp_ed)
    Down_reserve_limitation = (
    Rd_dg + Rd_ug + Rd_ess >= Pl_ac * UNCERTAINTY.Disturbance_range_ac_ed + Pl_ac_nc * UNCERTAINTY.Disturbance_range_ac_nc_ed + Pl_dc * UNCERTAINTY.Disturbance_range_dc_ed+ Pl_dc_nc * UNCERTAINTY.Disturbance_range_dc_nc_ed + Ppv * UNCERTAINTY.Disturbance_range_pv_ed + Pwp * UNCERTAINTY.Disturbance_range_wp_ed)

    # Economic dispatch model
    lp = op(default_resources_settings.FUEL_COST * DG.a_g * Delta_t * Pg + Price_profile * Delta_t * Pug + ESS.Cess_charging * Delta_t * Pess_c + ESS.Cess_discharging * Delta_t * Pess_dc + default_resources_settings.FUEL_COST * DG.b_g * Delta_t * DG_status + default_resources_settings.PV_CURTAILMENT_COST * (PV_profile - Ppv) * Delta_t
            + default_resources_settings.WP_CURTAILMENT_COST * (WP_profile - Pwp) * Delta_t + LOAD_AC_NC.Cost_shedding * (Load_profile_ac_nc - Pl_ac_nc) * Delta_t + LOAD_DC_NC.Cost_shedding * (Load_profile_dc_nc - Pl_dc_nc) * Delta_t + LOAD_AC.Cost_shedding * (Load_profile_ac - Pl_ac) * Delta_t + LOAD_DC.Cost_shedding * (Load_profile_dc - Pl_dc) * Delta_t,
            [Cg_boudary_lower, Cg_boudary_upper, Ru_dg_boudary_lower, Ru_dg_boudary_upper, Rd_dg_boudary_lower,
             Rd_dg_boudary_upper, Pug_boudary_lower, Pug_boudary_upper, Ru_ug_boudary_lower, Ru_ug_boudary_upper,
             Rd_ug_boudary_lower, Rd_ug_boudary_upper,
             Pess_c_boudary_lower, Pess_c_boudary_upper, Pess_dc_boudary_lower, Pess_dc_boudary_upper,
             Ru_ess_boudary_lower, Ru_ess_boudary_upper, Rd_ess_boudary_lower, Rd_ess_boudary_upper,
             Pa2d_boudary_lower, Pa2d_boudary_upper, Pd2a_boudary_lower, Pd2a_boudary_upper, Eess_boudary_lower,
             Eess_boudary_upper, DG_fuel_boudary_lower, DG_fuel_boudary_upper,
             Power_balance_constraint_ac, Power_balance_constraint_dc, Ess_dynamic, DG_fuel_dynamic,
             Ru_cap, Rd_cap, Ress_u_cap, Ress_d_cap, Ru_ug_cap, Rd_ug_cap, Ru_ess_energy,
             Pl_ac_boundary_lower, Pl_ac_boundary_upper,
             Pl_dc_boundary_lower, Pl_dc_boundary_upper,
             Pl_ac_nc_boundary_lower, Pl_ac_nc_boundary_upper,
             Pl_dc_nc_boundary_lower, Pl_dc_nc_boundary_upper,
             Ppv_boundary_lower, Ppv_boundary_upper,
             Pwp_boundary_lower, Pwp_boundary_upper,
             Rd_ess_energy, Ru_dg_energy,
             Up_reserve_limitation,
             Down_reserve_limitation,
             CQg_boudary_lower,CQg_boudary_upper,
             CQug_boudary_lower, CQug_boudary_upper,
             Power_balance_constraint_ac_q
             ])

    solvers.options['show_progress'] = False

    lp.solve()

    optimal_power_flow_solution = optimal_power_flow_result()
    optimal_power_flow_solution.Solution_status = 'infeasible'

    if lp.status == 'optimal':  # A feasible and optimal solution has been obtained
        optimal_power_flow_solution.Pg = Pg.value[0]
        optimal_power_flow_solution.Qg = Qg.value[0]
        optimal_power_flow_solution.Ig = DG_status
        optimal_power_flow_solution.Pug = Pug.value[0]
        optimal_power_flow_solution.Qug = Qug.value[0]
        optimal_power_flow_solution.Iug = UG_status
        optimal_power_flow_solution.Pess = Pess_dc.value[0] - Pess_c.value[0]
        optimal_power_flow_solution.SOC = Eess.value[0] / ESS.Capacity
        optimal_power_flow_solution.Pbic = Pd2a.value[0] - Pa2d.value[0]
        optimal_power_flow_solution.Qbic = Qbic.value[0]

        # We can also obtain the Langrange mutilper
        # Detect which type of load should be curtailed
        # The curtailment scale of PV
        if PV_profile != 0:
            if Ppv.value[0] < PV_profile:
                optimal_power_flow_solution.Ipv = max(RESOURCE_MANAGER.NPV - ceil(RESOURCE_MANAGER.NPV * Ppv.value[0] / PV_profile),0)
            else:
                optimal_power_flow_solution.Ipv = ED.Ipv
        else:
            optimal_power_flow_solution.Ipv = 0

        if WP_profile != 0:
            if Pwp.value[0] < PV_profile:
                optimal_power_flow_solution.Iwp = max(RESOURCE_MANAGER.NWP - ceil(RESOURCE_MANAGER.NWP * Pwp.value[0] / WP_profile),0)
            else:
                optimal_power_flow_solution.Iwp = ED.Iwp
        else:
            optimal_power_flow_solution.Iwp= 0

        Pl_ac_real = Pl_ac.value[0]
        Pl_dc_real = Pl_dc.value[0]
        Pl_ac_nc_real = Pl_ac_nc.value[0]
        Pl_dc_nc_real = Pl_dc_nc.value[0]

        # The shedding of AC load
        if abs(Pl_ac_real + Pl_ac_nc_real - Load_profile_ac - Load_profile_ac_nc) <= default_penalty_factors.Equaility_relation:
            optimal_power_flow_solution.Iac = 0
            optimal_power_flow_solution.Iac_nc = 0
            if ED.Iac == 1:
                optimal_power_flow_solution.Iac = 1
            if ED.Iac_nc == 1:
                optimal_power_flow_solution.Iac_nc = 1
        elif Pl_ac_real + Pl_ac_nc_real - Load_profile_ac>= 0:
            optimal_power_flow_solution.Iac = 0
            optimal_power_flow_solution.Iac_nc = 1
            if ED.Iac == 1:
                optimal_power_flow_solution.Iac = 1
        else:
            optimal_power_flow_solution.Iac = 1
            optimal_power_flow_solution.Iac_nc = 1

        # The shedding of DC load
        if abs(Pl_dc_real + Pl_dc_nc_real - Load_profile_dc - Load_profile_dc_nc) <= default_penalty_factors.Equaility_relation:
            optimal_power_flow_solution.Idc = 0
            optimal_power_flow_solution.Idc_nc = 0
            if ED.Idc == 1:
                optimal_power_flow_solution.Idc = 1
            if ED.Idc_nc == 1:
                optimal_power_flow_solution.Idc_nc = 1
        elif Pl_dc_real + Pl_dc_nc_real - Load_profile_dc>= 0:
            optimal_power_flow_solution.Idc = 0
            optimal_power_flow_solution.Idc_nc = 1
            if ED.Idc == 1:
                optimal_power_flow_solution.Idc = 1
        else:
            optimal_power_flow_solution.Idc = 1
            optimal_power_flow_solution.Idc_nc = 1

        optimal_power_flow_solution.Solution_status ='optimal'

    # Return the solutions
    return optimal_power_flow_solution

if __name__=="__main__":
    DG = modelling_ems.Diesel_generator_type_I_model()
    UG = modelling_ems.Utility_grid_model()
    PV = modelling_ems.Photovoltaic_generation_model()
    WP = modelling_ems.Wind_turbine_model()
    ESS = modelling_ems.Energy_storage_system_model()
    BIC = modelling_ems.Bi_directional_conveter_model()
    LOAD_AC = modelling_ems.AC_load_type_I_model()
    LOAD_AC_NC = modelling_ems.AC_load_type_I_model()
    LOAD_DC = modelling_ems.DC_load_type_I_model()
    LOAD_DC_NC = modelling_ems.DC_load_type_I_model()
    RESOURCE_MANAGER = modelling_ems.Resource_manager_model()
    UNCERTAINTY = modelling_ems.Uncertainty_model()
    ED = modelling_ems.economic_dispatch2optimal_power_flow_information()
    Load_profile_ac = 0
    Load_profile_ac_nc = 0
    Load_profile_dc = 0
    Load_profile_dc_nc = 0
    PV_profile = 0
    WP_profile = 0
    Price_profile = 0.08

    result = optimal_power_flow_single_step(DG,UG,PV,WP,ESS,BIC,LOAD_AC,LOAD_AC_NC,LOAD_DC,LOAD_DC_NC,RESOURCE_MANAGER,
                                            UNCERTAINTY, ED, Load_profile_ac, Load_profile_ac_nc,Load_profile_dc_nc,PV_profile,WP_profile,Price_profile)

    print("The real time optimal power flow test result is", result.Solution_status)
