# This functions is to gurantee the secure operation of BIC
# This is still based on the cvxopt, in the further, this function will be replaced with scipopt
# The set point tracing functions are added 24 Mar 2017

from pyscipopt import Model
from math import acos, tan, ceil
from default_setting import default_time
import modelling.modelling_local_ems as modelling_ems
from default_setting import default_resources_settings
from modelling.modelling_local_ems import optimal_power_flow_result
from default_setting import default_penalty_factors

def optimal_power_flow_tracing(DG = modelling_ems.Diesel_generator_type_I_model(),
                               UG = modelling_ems.Utility_grid_model(),
                               PV = modelling_ems.Photovoltaic_generation_model(),
                               WP = modelling_ems.Wind_turbine_model(),
                               ESS = modelling_ems.Energy_storage_system_model(),
                               BIC = modelling_ems.Bi_directional_conveter_model(),
                               LOAD_AC = modelling_ems.AC_load_type_I_model(),
                               LOAD_AC_NC = modelling_ems.AC_load_type_I_model(),
                               LOAD_DC = modelling_ems.DC_load_type_I_model(),
                               LOAD_DC_NC = modelling_ems.DC_load_type_I_model(),
                               RESOURCE_MANAGER = modelling_ems.Resource_manager_model(),
                               UNCERTAINTY = modelling_ems.Uncertainty_model(),
                               ED = modelling_ems.economic_dispatch2optimal_power_flow_information(),
                               Load_profile_ac= 0,
                               Load_profile_ac_nc = 0,
                               Load_profile_dc = 0,
                               Load_profile_dc_nc = 0,
                               PV_profile = 0,
                               WP_profile = 0,
                               Price_profile = 0.08):
    #Define the decision variables firstly
    #Pg,Pug,Pess_c,Pess_dc,Pa2d,Pd2a
    #Ru_dg,Rd_dg,Ru_ug,Rd_ug,Ru_ess,Rd_ess
    #Eess, DG_fuel
    Delta_t = default_time.Time_step_opf/3600

    model = Model("optimal power flow model")

    Pg = model.addVar(lb=DG.Pmin * min(ED.Ig, RESOURCE_MANAGER.Dg_status),
                      ub=DG.Pmax * min(ED.Ig, RESOURCE_MANAGER.Dg_status))

    Pg_positive = model.addVar(obj=default_penalty_factors.Penalty_Output,
                               lb=0,
                               ub=DG.Pmax * min(ED.Ig, RESOURCE_MANAGER.Dg_status))

    Pg_negative = model.addVar(obj=default_penalty_factors.Penalty_Output,
                               lb=0,
                               ub=DG.Pmax * min(ED.Ig, RESOURCE_MANAGER.Dg_status))

    Qg = model.addVar(lb=-DG.Pmax * min(ED.Ig, RESOURCE_MANAGER.Dg_status),
                      ub=DG.Pmax * min(ED.Ig, RESOURCE_MANAGER.Dg_status))

    Qg_abs = model.addVar(obj=0.001,lb=0,ub=DG.Pmax * min(ED.Ig, RESOURCE_MANAGER.Dg_status))

    Ru_dg = model.addVar(lb=0, ub=DG.Pmax * min(ED.Ig, RESOURCE_MANAGER.Dg_status))
    Rd_dg = model.addVar(lb=0, ub=DG.Pmax * min(ED.Ig, RESOURCE_MANAGER.Dg_status))

    Pug = model.addVar(lb = UG.Pmin * min(ED.Iug, RESOURCE_MANAGER.Ug_status),
                       ub = UG.Pmax * min(ED.Iug, RESOURCE_MANAGER.Ug_status))

    Pug_positive = model.addVar(obj = default_penalty_factors.Penalty_Output,
                       lb = 0,
                       ub = UG.Pmax * min(ED.Iug, RESOURCE_MANAGER.Ug_status))

    Pug_negative = model.addVar(obj = default_penalty_factors.Penalty_Output,
                                lb = 0,
                                ub = UG.Pmax * min(ED.Iug, RESOURCE_MANAGER.Ug_status))

    Qug = model.addVar(lb= -UG.Pmax * min(ED.Iug, RESOURCE_MANAGER.Ug_status) * tan(acos(UG.Power_factor_limitation)),
                       ub= UG.Pmax * min(ED.Iug, RESOURCE_MANAGER.Ug_status)* tan(acos(UG.Power_factor_limitation)))

    Qug_abs = model.addVar(obj=0.001,lb=0,ub=UG.Pmax * min(ED.Iug, RESOURCE_MANAGER.Ug_status) * tan(acos(UG.Power_factor_limitation)))

    Ru_ug = model.addVar(lb=0, ub=(UG.Pmax - UG.Pmin) * min(ED.Iug, RESOURCE_MANAGER.Ug_status))
    Rd_ug = model.addVar(lb=0, ub=(UG.Pmax - UG.Pmin) * min(ED.Iug, RESOURCE_MANAGER.Ug_status))

    Pess_c = model.addVar(obj = default_penalty_factors.Penalty_ESS,lb=0,ub=ESS.Pmax_charging)
    Pess_dc= model.addVar(obj = default_penalty_factors.Penalty_ESS,lb=0,ub=ESS.Pmax_discharging)
    Eess = model.addVar( lb = ESS.SOC_min*ESS.Capacity, ub=ESS.SOC_max*ESS.Capacity)

    Ru_ess = model.addVar( lb = 0, ub = ESS.Pmax_charging+ESS.Pmax_discharging)
    Rd_ess = model.addVar( lb = 0, ub = ESS.Pmax_charging+ESS.Pmax_discharging)

    Pa2d = model.addVar( obj = default_penalty_factors.Penalty_BIC,lb=0, ub=BIC.Capacity)
    Pd2a = model.addVar( obj = default_penalty_factors.Penalty_BIC,lb=0, ub=BIC.Capacity)
    Qbic = model.addVar( lb=-BIC.Capacity, ub=BIC.Capacity)

    Pac = model.addVar(obj = -LOAD_AC.Cost_shedding * Delta_t ,lb=0,ub= Load_profile_ac)
    Pdc = model.addVar(obj = -LOAD_DC.Cost_shedding * Delta_t ,lb=0,ub= Load_profile_dc)
    Pac_nc = model.addVar(obj = -LOAD_AC_NC.Cost_shedding * Delta_t, lb=0,ub= Load_profile_ac_nc)
    Pdc_nc = model.addVar(obj = -LOAD_DC_NC.Cost_shedding * Delta_t, lb=0,ub= Load_profile_dc_nc)

    Pwp = model.addVar(obj = -WP.Cost_curtailment * Delta_t , lb=0,ub=WP_profile)
    Ppv = model.addVar(obj = -PV.Cost_curtailment * Delta_t , lb=0,ub=PV_profile)

    ESS_discharging_efficiency = 1.0 / ESS.Efficiency_discharging

    SOC_derivation_positive = model.addVar(obj=default_penalty_factors.Penalty_SOC, lb=0, ub=1)
    SOC_derivation_negative = model.addVar(obj=default_penalty_factors.Penalty_SOC, lb=0, ub=1)

    #The constraints set

    model.addCons(Qg_abs >= Qg)
    model.addCons(Qg_abs >= -Qg)
    model.addCons(Qug_abs >= Qug)
    model.addCons(Qug_abs >= -Qug)

    model.addCons(Pg_positive >= ED.Pg-Pg)
    model.addCons(Pg_negative >= Pg-ED.Pg)
    model.addCons(Pug_positive >= ED.Pug - Pug)
    model.addCons(Pug_negative >= Pug - ED.Pug)

    model.addCons(Pg + Ru_dg <= DG.Pmax * min(ED.Ig, RESOURCE_MANAGER.Dg_status))
    model.addCons(Pg - Rd_dg >= DG.Pmin * min(ED.Ig, RESOURCE_MANAGER.Dg_status))
    model.addCons(Pg**2 +Qg**2 <= DG.Capacity*DG.Capacity * min(ED.Ig, RESOURCE_MANAGER.Dg_status))

    # The utility grid part
    model.addCons(Pug + Ru_ug <= min(ED.Iug, RESOURCE_MANAGER.Ug_status) * UG.Pmax)
    model.addCons(Pug - Rd_ug >= min(ED.Iug, RESOURCE_MANAGER.Ug_status) * UG.Pmin)

    # The Energy storage part
    model.addCons(Pess_dc - Pess_c + Ru_ess <= ESS.Pmax_discharging)
    model.addCons(Pess_c - Pess_dc + Rd_ess <= ESS.Pmax_charging)
    model.addCons(Eess == ESS.ESS0 - Pess_dc * Delta_t * ESS_discharging_efficiency + Pess_c * ESS.Efficiency_charging * Delta_t)

    model.addCons(Eess + Rd_ess * ESS.Efficiency_charging * Delta_t <= ESS.SOC_max * ESS.Capacity)
    model.addCons(Eess - Ru_ess * ESS_discharging_efficiency * Delta_t >= ESS.SOC_min * ESS.Capacity)
    # The BIC part
    model.addCons(Pa2d**2+Qbic**2 <= BIC.Capacity**2)
    # Power balance constraints
    # AC side
    model.addCons(Pug + Pg + Pd2a * BIC.Efficiency_DC2AC == Pac + Pac_nc + Pa2d)
    model.addCons(Pess_dc + Ppv + Pwp + Pa2d * BIC.Efficiency_AC2DC == Pdc + Pdc_nc + Pess_c + Pd2a)
    model.addCons(Qug + Qg + Qbic == Pac*tan(acos(LOAD_AC.Power_factor)) + Pac_nc*tan(acos(LOAD_AC.Power_factor)))
    # Reserve constraints

    model.addCons(Ru_ess + Ru_dg + Ru_ug >= Pac * UNCERTAINTY.Disturbance_range_ac_ed + Pac_nc * UNCERTAINTY.Disturbance_range_ac_nc_ed + Pdc * UNCERTAINTY.Disturbance_range_dc_ed
                  + Pdc_nc* UNCERTAINTY.Disturbance_range_dc_nc_ed + Ppv * UNCERTAINTY.Disturbance_range_pv_ed
                  + Pwp * UNCERTAINTY.Disturbance_range_wp_ed)

    model.addCons(Rd_ess + Rd_dg + Rd_ug >= Pac * UNCERTAINTY.Disturbance_range_ac_ed + Pac_nc * UNCERTAINTY.Disturbance_range_ac_nc_ed + Pdc * UNCERTAINTY.Disturbance_range_dc_ed
                  + Pdc_nc* UNCERTAINTY.Disturbance_range_dc_nc_ed + Ppv * UNCERTAINTY.Disturbance_range_pv_ed
                  + Pwp * UNCERTAINTY.Disturbance_range_wp_ed)


    Capacity_reciprocal = 1 / ESS.Capacity
    model.addCons(SOC_derivation_positive >= Eess * Capacity_reciprocal - ED.SOC)
    model.addCons(SOC_derivation_negative >= ED.SOC - Eess * Capacity_reciprocal)

    model.hideOutput(True)  # The output commamnd

    model.optimize()

    sol = model.getStatus()

    optimal_power_flow_solution = optimal_power_flow_result()
    optimal_power_flow_solution.Solution_status = sol

    if sol == 'optimal':  # A feasible and optimal solution has been obtained
        optimal_power_flow_solution.Pg = model.getVal(Pg)
        optimal_power_flow_solution.Qg = model.getVal(Qg)
        optimal_power_flow_solution.Ig = ED.Ig
        optimal_power_flow_solution.Pug = model.getVal(Pug)
        optimal_power_flow_solution.Qug = model.getVal(Qug)
        optimal_power_flow_solution.Iug = ED.Iug
        optimal_power_flow_solution.Pess = model.getVal(Pess_dc) - model.getVal(Pess_c)
        optimal_power_flow_solution.SOC = model.getVal(Eess) / ESS.Capacity
        optimal_power_flow_solution.Pbic = model.getVal(Pd2a) - model.getVal(Pa2d)
        optimal_power_flow_solution.Qbic = model.getVal(Qbic)

        # We can also obtain the Langrange mutilper
        # Detect which type of load should be curtailed
        # The curtailment scale of PV
        if PV_profile != 0:
            if model.getVal(Ppv) < PV_profile:
                optimal_power_flow_solution.Ipv = max(RESOURCE_MANAGER.NPV - ceil(RESOURCE_MANAGER.NPV * model.getVal(Ppv) / PV_profile),0)
            else:
                optimal_power_flow_solution.Ipv = ED.Ipv
        else:
            optimal_power_flow_solution.Ipv = 0

        if WP_profile != 0:
            if model.getVal(Pwp) < PV_profile:
                optimal_power_flow_solution.Iwp = max(RESOURCE_MANAGER.NWP - ceil(RESOURCE_MANAGER.NWP * model.getVal(Pwp) / WP_profile),0)
            else:
                optimal_power_flow_solution.Iwp = ED.Iwp
        else:
            optimal_power_flow_solution.Iwp= 0

        Pl_ac_real = model.getVal(Pac)
        Pl_dc_real = model.getVal(Pdc)
        Pl_ac_nc_real = model.getVal(Pac_nc)
        Pl_dc_nc_real = model.getVal(Pdc_nc)
        if Load_profile_ac == 0:
            optimal_power_flow_solution.Iac = max(ED.Iac,0)
        elif Pl_ac_real<Load_profile_ac:
            optimal_power_flow_solution.Iac = 1

        if Load_profile_ac_nc == 0:
            optimal_power_flow_solution.Iac_nc = max(ED.Iac_nc,0)
        elif Pl_ac_nc_real<Load_profile_ac_nc:
            optimal_power_flow_solution.Iac_nc = 1

        if Load_profile_dc == 0:
            optimal_power_flow_solution.Idc = max(ED.Idc,0)
        elif Pl_dc_real<Load_profile_dc:
            optimal_power_flow_solution.Idc = 1

        if Load_profile_dc_nc == 0:
            optimal_power_flow_solution.Idc_nc = max(ED.Idc_nc,0)
        elif Pl_dc_nc_real<Load_profile_dc_nc:
            optimal_power_flow_solution.Idc_nc = 1

        optimal_power_flow_solution.Solution_status = 'optimal'

    # Return the solutions
    return optimal_power_flow_solution

if __name__=="__main__":
    DG = modelling_ems.Diesel_generator_type_I_model()
    UG = modelling_ems.Utility_grid_model()
    PV = modelling_ems.Photovoltaic_generation_model()
    WP = modelling_ems.Wind_turbine_model()
    ESS = modelling_ems.Energy_storage_system_model()
    BIC = modelling_ems.Bi_directional_conveter_model()
    LOAD_AC = modelling_ems.AC_load_type_I_model()
    LOAD_AC_NC = modelling_ems.AC_load_type_I_model()
    LOAD_DC = modelling_ems.DC_load_type_I_model()
    LOAD_DC_NC = modelling_ems.DC_load_type_I_model()
    RESOURCE_MANAGER = modelling_ems.Resource_manager_model()
    UNCERTAINTY = modelling_ems.Uncertainty_model()
    ED = modelling_ems.economic_dispatch2optimal_power_flow_information()
    Load_profile_ac = 0
    Load_profile_ac_nc = 0
    Load_profile_dc = 0
    Load_profile_dc_nc = 0
    PV_profile = 0
    WP_profile = 0
    Price_profile = 0.08

    result = optimal_power_flow_tracing(DG,UG,PV,WP,ESS,BIC,LOAD_AC,LOAD_AC_NC,LOAD_DC,LOAD_DC_NC,RESOURCE_MANAGER,
                                        UNCERTAINTY, ED, Load_profile_ac, Load_profile_ac_nc,Load_profile_dc_nc,PV_profile,WP_profile,Price_profile)

    print("The real time optimal power flow test result is", result.Solution_status)