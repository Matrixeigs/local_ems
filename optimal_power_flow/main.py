from sqlalchemy import create_engine,and_
from sqlalchemy.orm import sessionmaker

from default_setting import default_time
from default_setting import default_local_database
from data_management.database_format import Optimal_power_flow_database_format
import modelling.modelling_local_ems as modelling
#According to the different kind of informations (time horizen and existing of UC)
import optimal_power_flow.optimal_power_flow_real_time as opf
import optimal_power_flow.optimal_power_flow_tracing as opf_ver
from math import tan, acos
from default_setting import default_data_format
def optimal_power_flow(Start_time=default_time.Base_time,
                       DG=modelling.Diesel_generator_type_I_model(),
                       UG=modelling.Utility_grid_model(),
                       PV=modelling.Photovoltaic_generation_model(),
                       WP=modelling.Wind_turbine_model(),
                       ESS=modelling.Energy_storage_system_model(),
                       BIC=modelling.Bi_directional_conveter_model(),
                       LOAD_AC=modelling.AC_load_type_I_model(),
                       LOAD_AC_NC=modelling.AC_load_type_I_model(),
                       LOAD_DC=modelling.DC_load_type_I_model(),
                       LOAD_DC_NC=modelling.DC_load_type_I_model(),
                       RESOURCE_MANAGER=modelling.Resource_manager_model(),
                       UNCERTAINTY=modelling.Uncertainty_model(),
                       ED = modelling.economic_dispatch2optimal_power_flow_information(),
                       Load_profile_ac=0,
                       Load_profile_ac_nc=0,
                       Load_profile_dc=0,
                       Load_profile_dc_nc=0,
                       PV_profile=0,
                       WP_profile=0,
                       Price_profile=0.08):
    #Implement the economic dispatch
    result_opf = opf_ver.optimal_power_flow_tracing(DG,UG,PV,WP,ESS,BIC,LOAD_AC,LOAD_AC_NC,LOAD_DC,LOAD_DC_NC,RESOURCE_MANAGER,
                                              UNCERTAINTY,ED,Load_profile_ac,Load_profile_ac_nc,Load_profile_dc,Load_profile_dc_nc,
                                              PV_profile,WP_profile,Price_profile)
    db_str = default_local_database.db_str
    engine = create_engine(db_str, echo=False)

    Session = sessionmaker(bind=engine)

    session = Session()
    # 5) save the results to database
    Optimal_power_flow_result = Optimal_power_flow_database_format(
        TIME_STAMP = Start_time,
        CAC3PL_ACTPOW = round(Load_profile_ac,default_data_format.opf_accuracy),
        CAC3PL_REACTPOW = round(Load_profile_ac * tan( acos(LOAD_AC.Power_factor)),default_data_format.opf_accuracy),
        UCAC3PL_ACTPOW = round(Load_profile_ac_nc,default_data_format.opf_accuracy),
        UCAC3PL_REACTPOW = round(Load_profile_ac_nc * tan( acos(LOAD_AC_NC.Power_factor)),default_data_format.opf_accuracy),
        CDCL_POW = round(Load_profile_dc,default_data_format.opf_accuracy),
        UCDCL_POW = round(Load_profile_dc_nc,default_data_format.opf_accuracy),

        PV_CAP=RESOURCE_MANAGER.PV_CAP,
        PV_FORECASTING=round(PV_profile,default_data_format.opf_accuracy),
        WP_CAP=RESOURCE_MANAGER.WP_CAP,
        WP_FORECASTING=round(WP_profile,default_data_format.opf_accuracy),
        DG_STATUS=result_opf.Ig,
        DG_ACTPOW=round(result_opf.Pg,default_data_format.opf_accuracy),
        DG_REACTPOW=round(result_opf.Qg,default_data_format.opf_accuracy),

        UG_STATUS=result_opf.Iug,
        UG_X_ACTPOW=round(result_opf.Pug,default_data_format.opf_accuracy),
        UG_X_REACTPOW=round(result_opf.Qug,default_data_format.opf_accuracy),

        BAT_POW=round(result_opf.Pess,default_data_format.opf_accuracy),
        BAT_SOC=round(result_opf.SOC,default_data_format.opf_accuracy),

        BIC_ACTPOW=round(result_opf.Pbic,default_data_format.opf_accuracy),
        BIC_REACTPOW=round(result_opf.Qbic,default_data_format.opf_accuracy),

        PV_CURTAIL=result_opf.Ipv,
        WP_CURTAIL=result_opf.Iwp,
        CAC3PL_SHED=result_opf.Iac,
        UCAC3PL_SHED=result_opf.Iac_nc,
        CDCL_SHED=result_opf.Idc,
        UCDCL_SHED=result_opf.Idc_nc)

    if session.query(Optimal_power_flow_database_format).filter_by(TIME_STAMP=Start_time).count() == 0:  # if the data do not exist, the data will be inserted
        session.add(Optimal_power_flow_result)
        session.commit()
    else:  # if the data has already existed, this data will be updated.
        row = session.query(Optimal_power_flow_database_format).filter_by(TIME_STAMP = Start_time).first()
        row.CAC3PL_ACTPOW = round(Load_profile_ac,default_data_format.opf_accuracy)
        row.CAC3PL_REACTPOW = round(Load_profile_ac * tan(acos(LOAD_AC.Power_factor)),default_data_format.opf_accuracy)
        row.UCAC3PL_ACTPOW = round(Load_profile_ac_nc,default_data_format.opf_accuracy)
        row.UCAC3PL_REACTPOW = round(Load_profile_ac_nc * tan(acos(LOAD_AC_NC.Power_factor)),default_data_format.opf_accuracy)
        row.CDCL_POW = round(Load_profile_dc,default_data_format.opf_accuracy)
        row.UCDCL_POW = round(Load_profile_dc_nc,default_data_format.opf_accuracy)

        row.PV_CAP = RESOURCE_MANAGER.PV_CAP,
        row.PV_FORECASTING = round(PV_profile,default_data_format.opf_accuracy)
        row.WP_CAP = RESOURCE_MANAGER.WP_CAP,
        row.WP_FORECASTING = round(WP_profile,default_data_format.opf_accuracy)
        row.DG_STATUS = result_opf.Ig,
        row.DG_ACTPOW = round(result_opf.Pg,default_data_format.opf_accuracy)
        row.DG_REACTPOW = round(result_opf.Qg,default_data_format.opf_accuracy)

        row.UG_STATUS = result_opf.Iug,
        row.UG_X_ACTPOW = round(result_opf.Pug,default_data_format.opf_accuracy)
        row.UG_X_REACTPOW = round(result_opf.Qug,default_data_format.opf_accuracy)

        row.BAT_POW = round(result_opf.Pess,default_data_format.opf_accuracy)
        row.BAT_SOC = round(result_opf.SOC,default_data_format.opf_accuracy)

        row.BIC_ACTPOW = round(result_opf.Pbic,default_data_format.opf_accuracy)
        row.BIC_REACTPOW = round(result_opf.Qbic,default_data_format.opf_accuracy)

        row.PV_CURTAIL = result_opf.Ipv,
        row.WP_CURTAIL = result_opf.Iwp,
        row.CAC3PL_SHED = result_opf.Iac,
        row.UCAC3PL_SHED = result_opf.Iac_nc,
        row.CDCL_SHED = result_opf.Idc,
        row.UCDCL_SHED = result_opf.Idc_nc
        session.commit()
    #Return the economic dispatch result
    return result_opf

if __name__=="__main__":
    Start_time = default_time.Base_time
    DG = modelling.Diesel_generator_type_I_model()
    UG = modelling.Utility_grid_model()
    PV = modelling.Photovoltaic_generation_model()
    WP = modelling.Wind_turbine_model()
    ESS = modelling.Energy_storage_system_model()
    BIC = modelling.Bi_directional_conveter_model()
    LOAD_AC = modelling.AC_load_type_I_model()
    LOAD_AC_NC = modelling.AC_load_type_I_model()
    LOAD_DC = modelling.DC_load_type_I_model()
    LOAD_DC_NC = modelling.DC_load_type_I_model()
    RESOURCE_MANAGER = modelling.Resource_manager_model()
    UNCERTAINTY = modelling.Uncertainty_model()
    ED = modelling.economic_dispatch2optimal_power_flow_information()
    Load_profile_ac = 0
    Load_profile_ac_nc = 0
    Load_profile_dc = 0
    Load_profile_dc_nc = 0
    PV_profile = 0
    WP_profile = 0
    Price_profile = 0.08

    result_opf = optimal_power_flow(Start_time,DG,UG,PV,WP,ESS,BIC,LOAD_AC,LOAD_AC_NC,LOAD_DC,LOAD_DC_NC,RESOURCE_MANAGER,UNCERTAINTY,
                                    ED,Load_profile_ac,Load_profile_ac_nc,Load_profile_dc,Load_profile_dc_nc,PV_profile,WP_profile,Price_profile)

    print("The optimal power flow test result is", result_opf.Solution_status)