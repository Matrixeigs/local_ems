#This package is to solve the linear programming problems
#The linear programming is the following style
# min f(x)=c*x
# s.t. A*x<=b
#      Aeq*x==beq
#      lb<=x<=ub

#A default problem is given as follows
#Minimize: f = -1*x[0] + 4*x[1]

#Subject to: -3*x[0] + 1*x[1] <= 6
             #1*x[0] + 2*x[1] <= 4
             #x[1] >= -3

from scipy.optimize import linprog
def linear_programming_sovler(c=[-1, 4],A = [[-3, 1], [1, 2]],b = [6, 4],x_bounds = ((None, None), (-3, None))):

    res = linprog(c, A_ub=A, b_ub=b, bounds=x_bounds,options = {"disp": False})

    return res