TIME_LABEL = 1

LOAD_AC = 2
LOAD_AC_Q = 3
LOAD_AC_NC = 4
LOAD_AC_NC_Q = 5

LOAD_DC = 6
LOAD_DC_NC = 7

PV_CAPACITY = 8
PV_FORECAST = 9
PV_REAL = 10

WP_CAPACITY = 11
WP_FORECAST = 12
WP_REAL = 13

PRICE = 14

DG_STATUS = 15
DG_OUTPUT = 16
DG_OUTPUT_Q = 17
DG_FUEL = 18

GRID_STATUS = 19
GRID_OUTPUT = 20
GRID_OUTPUT_Q = 21

ESS_OUTPUT = 22
ESS_SOC = 23

BIC_POWER = 24
BIC_POWER_Q = 25
DC_VOL = 26

PV_CURTAILE = 27
WP_CURTAILE = 28

LOAD_AC_SHED = 29
LOAD_DC_SHED = 30
LOAD_AC_NC_SHED = 31
LOAD_DC_NC_SHED = 32