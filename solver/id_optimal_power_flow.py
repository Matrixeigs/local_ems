TIME_LABEL=1
LOAD_AC=2
LOAD_AC_Q = 3

LOAD_AC_NC = 4
LOAD_AC_NC_Q = 5

LOAD_DC = 6
LOAD_DC_NC = 7

PV_CAPACITY = 8
PV_FORECAST = 9

WP_CAPACITY = 10
WP_FORECAST = 11

DG_STATUS = 12
DG_OUTPUT = 13

GRID_STATUS = 14
GRID_OUTPUT = 15

ESS_OUTPUT = 16
BIC_OUTPUT = 17

PV_CURTAILE = 18
WP_CURTAILE = 19

LOAD_AC_SHED = 20
LOAD_DC_SHED = 21

LOAD_AC_NC_SHED = 22
LOAD_DC_NC_SHED = 23
