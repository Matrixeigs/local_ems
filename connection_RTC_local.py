from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from default_setting import default_local_database
from data_management.database_format import Real_time_operation_database_format,Optimal_power_flow_database_format,Economic_dispatch_database_format
from default_setting import  default_time
import random
from default_setting import default_resources_settings

def connection_RTC_local(Time = default_time.Base_time):

    db_str = default_local_database.db_str
    engine = create_engine(db_str, echo=False)
    Session = sessionmaker(bind=engine)
    session = Session()

    row_opf = session.query(Optimal_power_flow_database_format).filter_by(TIME_STAMP=Time).first()
    row_ed = session.query(Economic_dispatch_database_format).filter_by(TIME_STAMP=Time).first()

    updated_items = Real_time_operation_database_format(
        TIME_STAMP = Time,
        CAC3PL_ACTPOW = row_opf.CAC3PL_ACTPOW * default_resources_settings.BASE_VA,
        CAC3PL_REACTPOW = row_opf.CAC3PL_REACTPOW*default_resources_settings.BASE_VA,
        UCAC3PL_ACTPOW = row_opf.UCAC3PL_ACTPOW*default_resources_settings.BASE_VA,
        UCAC3PL_REACTPOW = row_opf.UCAC3PL_REACTPOW*default_resources_settings.BASE_VA,

        CDCL_MAX_POW =  row_opf.CDCL_POW*default_resources_settings.BASE_VA,
        UCDCL_MAX_POW =  row_opf.UCDCL_POW*default_resources_settings.BASE_VA,

        PV_CAP =  row_opf.PV_CAP*default_resources_settings.BASE_VA,
        PV_FORECASTING =  row_ed.PV_FORECASTING*default_resources_settings.BASE_VA,
        PV_POW =  row_opf.PV_FORECASTING*default_resources_settings.BASE_VA,

        WP_CAP =  row_opf.WP_CAP*default_resources_settings.BASE_VA,
        WP_FORECASTING =  row_ed.WP_FORECASTING*default_resources_settings.BASE_VA,
        WP_POW =  row_opf.WP_FORECASTING*default_resources_settings.BASE_VA,

        ELEC_PRICE =  row_ed.ELEC_PRICE,
        #They should be exchanged in the next step
        DG_STATUS =  row_opf.DG_STATUS,
        DG_ACTPOW =  row_opf.DG_ACTPOW*default_resources_settings.BASE_VA,
        DG_REACTPOW =  row_opf.DG_REACTPOW*default_resources_settings.BASE_VA,
        DG_FUEL =  row_ed.DG_FUEL,

        UG_STATUS =  row_opf.UG_STATUS,
        UG_X_ACTPOW =  row_opf.UG_X_ACTPOW*default_resources_settings.BASE_VA,
        UG_X_REACTPOW =  row_opf.UG_X_REACTPOW*default_resources_settings.BASE_VA,

        BAT_POW =  row_opf.BAT_POW*default_resources_settings.BASE_VA,
        BAT_SOC =  row_ed.BAT_SOC,

        BIC_ACTPOW =  row_opf.BIC_ACTPOW*default_resources_settings.BASE_VA,
        BIC_REACTPOW =  row_opf.BIC_REACTPOW*default_resources_settings.BASE_VA,
        DC_VOL =  220+random.random(),

        PV_CURTAIL =  row_ed.PV_CURTAIL,
        WP_CURTAIL =  row_ed.WP_CURTAIL,

        CAC3PL_SHED =  row_ed.CAC3PL_SHED,
        UCAC3PL_SHED =  row_ed.UCAC3PL_SHED,
        CDCL_SHED =  row_ed.CDCL_SHED,
        UCDCL_SHED =  row_ed.UCDCL_SHED,)

    if session.query(Real_time_operation_database_format).filter_by(TIME_STAMP=Time).count()==0:
        session.add(updated_items)

    else:
        row_rtc=session.query(Real_time_operation_database_format).filter_by(TIME_STAMP=Time).first()
        row_rtc.TIME_STAMP = Time
        row_rtc.CAC3PL_ACTPOW = row_opf.CAC3PL_ACTPOW*default_resources_settings.BASE_VA
        row_rtc.CAC3PL_REACTPOW = row_opf.CAC3PL_REACTPOW*default_resources_settings.BASE_VA
        row_rtc.UCAC3PL_ACTPOW = row_opf.UCAC3PL_ACTPOW*default_resources_settings.BASE_VA
        row_rtc.UCAC3PL_REACTPOW = row_opf.UCAC3PL_REACTPOW*default_resources_settings.BASE_VA

        row_rtc.CDCL_MAX_POW = row_opf.CDCL_POW*default_resources_settings.BASE_VA
        row_rtc.UCDCL_MAX_POW = row_opf.UCDCL_POW*default_resources_settings.BASE_VA

        row_rtc.PV_CAP = row_opf.PV_CAP*default_resources_settings.BASE_VA
        row_rtc.PV_FORECASTING = row_ed.PV_FORECASTING*default_resources_settings.BASE_VA
        row_rtc.PV_POW = row_opf.PV_FORECASTING*default_resources_settings.BASE_VA

        row_rtc.WP_CAP = row_opf.WP_CAP*default_resources_settings.BASE_VA
        row_rtc.WP_FORECASTING = row_ed.WP_FORECASTING*default_resources_settings.BASE_VA
        row_rtc.WP_POW = row_opf.WP_FORECASTING*default_resources_settings.BASE_VA

        row_rtc.ELEC_PRICE = row_ed.ELEC_PRICE

        row_rtc.DG_STATUS = row_opf.DG_STATUS
        row_rtc.DG_ACTPOW = row_opf.DG_ACTPOW*default_resources_settings.BASE_VA
        row_rtc.DG_REACTPOW = row_opf.DG_REACTPOW*default_resources_settings.BASE_VA
        row_rtc.DG_FUEL = row_ed.DG_FUEL

        row_rtc.UG_STATUS = row_opf.UG_STATUS
        row_rtc.UG_X_ACTPOW = row_opf.UG_X_ACTPOW*default_resources_settings.BASE_VA
        row_rtc.UG_X_REACTPOW = row_opf.UG_X_REACTPOW*default_resources_settings.BASE_VA

        row_rtc.BAT_POW = row_opf.BAT_POW*default_resources_settings.BASE_VA
        row_rtc.BAT_SOC = row_ed.BAT_SOC

        row_rtc.BIC_ACTPOW = row_opf.BIC_ACTPOW*default_resources_settings.BASE_VA
        row_rtc.BIC_REACTPOW = row_opf.BIC_REACTPOW*default_resources_settings.BASE_VA
        row_rtc.DC_VOL = 220 + random.random()

        row_rtc.PV_CURTAIL = row_ed.PV_CURTAIL
        row_rtc.WP_CURTAIL = row_ed.WP_CURTAIL

        row_rtc.CAC3PL_SHED = row_ed.CAC3PL_SHED
        row_rtc.UCAC3PL_SHED = row_ed.UCAC3PL_SHED
        row_rtc.CDCL_SHED = row_ed.CDCL_SHED
        row_rtc.UCDCL_SHED = row_ed.UCDCL_SHED

    session.commit()
