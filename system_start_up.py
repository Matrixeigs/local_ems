#This is the verification package of the whole EMS system for the microgrid
#The general types include the following functions

from modelling.modelling_local_ems import Diesel_generator_type_I_model, Utility_grid_model, \
    Wind_turbine_model, Photovoltaic_generation_model, \
    Energy_storage_system_model,Bi_directional_conveter_model,\
    AC_load_type_I_model,DC_load_type_I_model,\
    Resource_manager_model,Uncertainty_model

from default_setting import default_resources_settings
import  unit_committment.unit_committment_setup as UC_setup
from default_setting import default_time
import unit_committment.main as UC
import forecasting.main as FORECASTING
#Import modelling of local EMS
DG = Diesel_generator_type_I_model()
UG = Utility_grid_model()

WP = Wind_turbine_model()
PV = Photovoltaic_generation_model()

ESS = Energy_storage_system_model()

BIC = Bi_directional_conveter_model()

LOAD_AC = AC_load_type_I_model()
LOAD_AC.Cost_shedding = default_resources_settings.CRITICAL_LOAD_SHEDDING_COST

LOAD_AC_NC = AC_load_type_I_model()
LOAD_AC_NC.Cost_shedding = default_resources_settings.UNCRITICAL_LOAD_SHEDDING_COST

LOAD_DC = DC_load_type_I_model()
LOAD_DC.Cost_shedding = default_resources_settings.CRITICAL_LOAD_SHEDDING_COST

LOAD_DC_NC = DC_load_type_I_model()
LOAD_DC_NC.Cost_shedding = default_resources_settings.UNCRITICAL_LOAD_SHEDDING_COST

RES = Resource_manager_model()
UNCERTAINTY = Uncertainty_model()

def system_start_up(Time = default_time.Base_time,
                    Look_ahead_time_uc = default_time.Look_ahead_time_uc,
                    RESOURCE_MANAGER = Resource_manager_model()):

    FORECASTING.forecasting_day_ahead(Time, Look_ahead_time_uc, default_time.Time_step_uc)# Implement the day_ahead forecasting
    FORECASTING.forecasting_real_time(Time, default_time.Look_ahead_time_ed, default_time.Time_step_uc)

    [Load_profile_ac,Load_profile_ac_nc,Load_profile_dc,Load_profile_dc_nc,WP_profile,PV_profile,Price_profile] \
            = UC_setup.forecasting_result_query(Time,Look_ahead_time_uc,RESOURCE_MANAGER)

    result_uc = UC.unit_committment_day_ahead(Time,DG,UG,PV,WP,ESS,BIC,LOAD_AC,LOAD_AC_NC,LOAD_DC,LOAD_DC_NC,RESOURCE_MANAGER,UNCERTAINTY,Load_profile_ac,Load_profile_ac_nc,Load_profile_dc,Load_profile_dc_nc,PV_profile,WP_profile,Price_profile)

    return UG,DG,WP,PV,ESS,BIC,LOAD_AC,LOAD_AC_NC,LOAD_DC,LOAD_DC_NC,RES,UNCERTAINTY,result_uc

if __name__ == "__main__":

    Time = default_time.Base_time

    Look_ahead_time_uc = default_time.Look_ahead_time_uc

    RESOURCE_MANAGER = Resource_manager_model()

    (UG,DG,WP,PV,ESS,BIC,LOAD_AC,LOAD_AC_NC,LOAD_DC,LOAD_DC_NC,RES,UNCERTAINTY,result_uc) = system_start_up(Time,Look_ahead_time_uc,RESOURCE_MANAGER)

    print("The local EMS start-up test result is", result_uc)
