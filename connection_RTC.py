from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from default_setting import default_network_database
from data_management.database_format import Real_time_operation_database_format
from modelling.modelling_local_ems import optimal_power_flow_result
from default_setting import  default_time
from default_setting import  default_resources_settings

def connection_RTC(Time = default_time.Base_time,
                   resultopf = optimal_power_flow_result()):

    db_str = default_network_database.db_str_microgrid
    engine = create_engine(db_str, echo=False)
    Session = sessionmaker(bind=engine)
    session = Session()
    updated_items=Real_time_operation_database_format(TIME_STAMP = Time,
                                                      DG_ACTPOW = resultopf.Pug*default_resources_settings.BASE_VA,
                                                      UG_X_ACTPOW = 0)

    if session.query(Real_time_operation_database_format).filter_by(TIME_STAMP=Time).count()==0:
        session.add(updated_items)
    else:
        row=session.query(Real_time_operation_database_format).filter_by(TIME_STAMP=Time).first()
        row.DG_ACTPOW = resultopf.Pug*default_resources_settings.BASE_VA
        row.UG_X_ACTPOW = 0.00

    session.commit()
