#All pv output should be converted into [0,1]
#This function is based on the following assumptions:
#1) The history now can predict further.
from default_setting import default_local_database
from sqlalchemy import create_engine, and_
from sqlalchemy.orm import sessionmaker
from data_management.database_format import History_data_pv
from default_setting import default_time
#The first function, history database unification
def pv_history_data_unification():

    engine = create_engine(default_local_database.db_str)
    DBsession = sessionmaker(bind=engine)
    session = DBsession()
    solar_data = session.query(History_data_pv.SOLAR_RAD).filter(History_data_pv.TIME_STAMP >= default_time.Base_time).all()
    pv_data = session.query(History_data_pv.SOLAR_POW).filter(
        History_data_pv.TIME_STAMP >= default_time.Base_time).all()

    data_len=len(solar_data)
    solar_data_max = max(solar_data)[0]
    solar_data_min = min(solar_data)[0]
    pv_data_max = max(pv_data)[0]
    pv_data_min = min(pv_data)[0]
    for i in range(data_len):
        if session.query(History_data_pv).filter_by(TIME_STAMP = default_time.Base_time + i*60).count()!=0:
            row = session.query(History_data_pv).filter_by(TIME_STAMP = default_time.Base_time + i*60).first()
            if pv_data_max!= pv_data_min:
                row.SOLAR_POW = (row.SOLAR_POW-pv_data_min)/(pv_data_max-pv_data_min)
            if solar_data_min!=solar_data_max:
                row.SOLAR_RAD = (row.SOLAR_RAD-solar_data_min)/(solar_data_max-solar_data_min)
    session.commit()

    session.close()
