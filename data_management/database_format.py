#Database format announcement
#Date:22 Feb 2017
#Author: Zhao TY

#Notes:
#1) The data format of forecasting has been defined
#2) The data format of unit committment
#3) History data of weather station has been added

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, FLOAT, INTEGER,DATETIME

Base = declarative_base()

####################### 1) Data format of forecasting #################
class Weather_station_database(Base):
    __tablename__ = 'real_time_forecasting_history_data'

    TIME_STAMP = Column(INTEGER, primary_key=True)

    ENV_TMP = Column(FLOAT)
    ENV_HUM = Column(FLOAT)
    SOLAR_RAD = Column(FLOAT)

    SOLAR_POW = Column(FLOAT) #The solar power output should be unified.
    WIND_SPD = Column(FLOAT)
    WIND_DIR = Column(FLOAT)
    WIND_POW = Column(FLOAT) #The wind power output should be unified.

class Day_ahead_forecasting(Base):
    __tablename__ = 'day_ahead_forecasting'

    TIME_STAMP = Column(INTEGER, primary_key=True)

    CAC3PL_ACTPOW = Column(FLOAT)
    UCAC3PL_ACTPOW = Column(FLOAT)
    CDCL_POW = Column(FLOAT)
    UCDCL_POW = Column(FLOAT)

    PV_FORECASTING = Column(FLOAT)
    WP_FORECASTING = Column(FLOAT)

    ELEC_PRICE = Column(FLOAT)

class Intra_day_forecasting(Base):
    __tablename__ = 'intra_day_forecasting'

    TIME_STAMP = Column(INTEGER, primary_key=True)

    CAC3PL_ACTPOW = Column(FLOAT)
    UCAC3PL_ACTPOW = Column(FLOAT)
    CDCL_POW = Column(FLOAT)
    UCDCL_POW = Column(FLOAT)

    PV_FORECASTING = Column(FLOAT)
    WP_FORECASTING = Column(FLOAT)

    ELEC_PRICE = Column(FLOAT)


class Real_time_forecasting(Base):
    __tablename__ = 'real_time_forecasting'

    TIME_STAMP = Column(INTEGER, primary_key=True)

    CAC3PL_ACTPOW = Column(FLOAT)
    UCAC3PL_ACTPOW = Column(FLOAT)
    CDCL_POW = Column(FLOAT)
    UCDCL_POW = Column(FLOAT)

    PV_FORECASTING = Column(FLOAT)
    WP_FORECASTING = Column(FLOAT)

    ELEC_PRICE = Column(FLOAT)

####################### 2) Data format of economic dispatch #################

class Economic_dispatch_database_format(Base):
    __tablename__ = 'economic_dispatch_intra_day'

    TIME_STAMP = Column(INTEGER, primary_key=True)
    CAC3PL_ACTPOW = Column(FLOAT)
    UCAC3PL_ACTPOW = Column(FLOAT)
    CDCL_POW = Column(FLOAT)
    UCDCL_POW = Column(FLOAT)

    PV_CAP = Column(FLOAT)
    PV_FORECASTING = Column(FLOAT)

    WP_CAP = Column(FLOAT)
    WP_FORECASTING = Column(FLOAT)

    ELEC_PRICE = Column(FLOAT)

    DG_STATUS = Column(INTEGER)
    DG_ACTPOW = Column(FLOAT)
    DG_FUEL = Column(FLOAT)

    UG_STATUS = Column(INTEGER)
    UG_X_ACTPOW = Column(FLOAT)
    BAT_POW = Column(FLOAT)
    BAT_SOC = Column(FLOAT)

    BIC_ACTPOW = Column(INTEGER)
    PV_CURTAIL = Column(INTEGER)
    WP_CURTAIL = Column(INTEGER)

    CAC3PL_SHED = Column(INTEGER)
    UCAC3PL_SHED = Column(INTEGER)
    CDCL_SHED = Column(INTEGER)
    UCDCL_SHED = Column(INTEGER)

####################### 3) Data format of unit committment #################
class Unit_committment_intra_day_database_format(Base):
    __tablename__ = 'unit_committment_intra_day'

    TIME_STAMP=Column(INTEGER, primary_key=True)
    CAC3PL_ACTPOW=Column(FLOAT)
    UCAC3PL_ACTPOW=Column(FLOAT)
    CDCL_POW=Column(FLOAT)
    UCDCL_POW=Column(FLOAT)

    PV_CAP=Column(FLOAT)
    PV_FORECASTING= Column(FLOAT)

    WP_CAP = Column(FLOAT)
    WP_FORECASTING = Column(FLOAT)

    ELEC_PRICE = Column(FLOAT)

    DG_STATUS = Column(INTEGER)
    DG_ACTPOW = Column(FLOAT)
    DG_FUEL = Column(FLOAT)

    UG_STATUS = Column(INTEGER)
    UG_X_ACTPOW = Column(FLOAT)
    BAT_POW = Column(FLOAT)
    BAT_SOC = Column(FLOAT)

    BIC_ACTPOW = Column(INTEGER)
    PV_CURTAIL = Column(INTEGER)
    WP_CURTAIL = Column(INTEGER)

    CAC3PL_SHED = Column(INTEGER)
    UCAC3PL_SHED = Column(INTEGER)
    CDCL_SHED = Column(INTEGER)
    UCDCL_SHED = Column(INTEGER)

class Unit_committment_day_ahead_database_format(Base):
    __tablename__ = 'unit_committment_day_ahead'

    TIME_STAMP=Column(INTEGER, primary_key=True)
    CAC3PL_ACTPOW=Column(FLOAT)
    UCAC3PL_ACTPOW=Column(FLOAT)
    CDCL_POW=Column(FLOAT)
    UCDCL_POW=Column(FLOAT)

    PV_CAP=Column(FLOAT)
    PV_FORECASTING= Column(FLOAT)

    WP_CAP = Column(FLOAT)
    WP_FORECASTING = Column(FLOAT)

    ELEC_PRICE = Column(FLOAT)

    DG_STATUS = Column(INTEGER)
    DG_ACTPOW = Column(FLOAT)
    DG_FUEL = Column(FLOAT)

    UG_STATUS = Column(INTEGER)
    UG_X_ACTPOW = Column(FLOAT)
    BAT_POW = Column(FLOAT)
    BAT_SOC = Column(FLOAT)

    BIC_ACTPOW = Column(INTEGER)
    PV_CURTAIL = Column(INTEGER)
    WP_CURTAIL = Column(INTEGER)

    CAC3PL_SHED = Column(INTEGER)
    UCAC3PL_SHED = Column(INTEGER)
    CDCL_SHED = Column(INTEGER)
    UCDCL_SHED = Column(INTEGER)

####################### 4) Data format of optimal power flow #################
class Optimal_power_flow_database_format(Base):
    __tablename__ = 'optimal_power_flow'

    TIME_STAMP = Column(INTEGER, primary_key=True)
    CAC3PL_ACTPOW = Column(FLOAT)
    CAC3PL_REACTPOW = Column(FLOAT)
    UCAC3PL_ACTPOW = Column(FLOAT)
    UCAC3PL_REACTPOW = Column(FLOAT)
    CDCL_POW = Column(FLOAT)
    UCDCL_POW = Column(FLOAT)

    PV_CAP = Column(FLOAT)
    PV_FORECASTING = Column(FLOAT)

    WP_CAP = Column(FLOAT)
    WP_FORECASTING = Column(FLOAT)

    DG_STATUS = Column(INTEGER)
    DG_ACTPOW = Column(FLOAT)
    DG_REACTPOW = Column(FLOAT)

    UG_STATUS = Column(INTEGER)
    UG_X_ACTPOW = Column(FLOAT)
    UG_X_REACTPOW = Column(FLOAT)

    BAT_POW = Column(FLOAT)
    BAT_SOC = Column(FLOAT)

    BIC_ACTPOW = Column(INTEGER)
    BIC_REACTPOW = Column(INTEGER)

    PV_CURTAIL = Column(INTEGER)
    WP_CURTAIL = Column(INTEGER)

    CAC3PL_SHED = Column(INTEGER)
    UCAC3PL_SHED = Column(INTEGER)
    CDCL_SHED = Column(INTEGER)
    UCDCL_SHED = Column(INTEGER)

####################### 5) Data format of real-time operation #################
class Real_time_operation_database_format(Base):
    __tablename__ = 'real_time_operation_data'

    TIME_STAMP=Column(INTEGER, primary_key=True)
    CAC3PL_ACTPOW=Column(FLOAT)
    CAC3PL_REACTPOW = Column(FLOAT)
    UCAC3PL_ACTPOW=Column(FLOAT)
    UCAC3PL_REACTPOW = Column(FLOAT)

    CDCL_MAX_POW=Column(FLOAT)
    UCDCL_MAX_POW=Column(FLOAT)

    PV_CAP=Column(FLOAT)
    PV_FORECASTING= Column(FLOAT)
    PV_POW = Column(FLOAT)

    WP_CAP = Column(FLOAT)
    WP_FORECASTING = Column(FLOAT)
    WP_POW = Column(FLOAT)

    ELEC_PRICE = Column(FLOAT)

    DG_STATUS = Column(INTEGER)
    DG_ACTPOW = Column(FLOAT)
    DG_REACTPOW = Column(FLOAT)
    DG_FUEL = Column(FLOAT)

    UG_STATUS = Column(INTEGER)
    UG_X_ACTPOW = Column(FLOAT)
    UG_X_REACTPOW = Column(FLOAT)

    BAT_POW = Column(FLOAT)
    BAT_SOC = Column(FLOAT)

    BIC_ACTPOW = Column(FLOAT)
    BIC_REACTPOW = Column(FLOAT)
    DC_VOL = Column(FLOAT)

    PV_CURTAIL = Column(INTEGER)
    WP_CURTAIL = Column(INTEGER)

    CAC3PL_SHED = Column(INTEGER)
    UCAC3PL_SHED = Column(INTEGER)
    CDCL_SHED = Column(INTEGER)
    UCDCL_SHED = Column(INTEGER)
####################### 6) Data format of test data #################
class Test_data_database_format(Base):
    __tablename__ = 'test_data'

    TIME_STAMP = Column(INTEGER, primary_key=True)
    LOAD_COMMERCIAL = Column(FLOAT) # Typical profile of commercial user within one year
    LOAD_RESIDENT = Column(FLOAT) # Typical profile of resident user within one year
    PV_OUTPUT = Column(FLOAT) # Typical profile of photovoltaic generator within one year
    ELEC_PRICE = Column(FLOAT) # Typical profile of electric prices within one year
####################### 7) Data format of forecasting performance #################
class History_data_pv(Base):
    __tablename__ = 'history_data_pv'

    TIME_STAMP = Column(INTEGER, primary_key=True)
    ENV_TEMP = Column(FLOAT)
    SOLAR_RAD = Column(FLOAT)
    SOLAR_POW = Column(FLOAT)

####################### 8) Data format of weather station on the Upboard #################
class weather_station_data(Base):
    __tablename__ = 'ISSData'

    ReceiverRecID = Column(INTEGER)
    ChannelIndex = Column(INTEGER)
    RecDateTime= Column(DATETIME,primary_key=True)
    TempOut= Column(INTEGER)
    HiTempOut = Column(INTEGER)
    LowTempOut = Column(INTEGER)
    HumOut =  Column(INTEGER)
    WindSpeed =  Column(INTEGER)
    ScalerAvgWindDir= Column(INTEGER)
    HiWindSpeed = Column(INTEGER)
    HiWindDir = Column(INTEGER)
    DominantDir = Column(INTEGER)
    DewPoint = Column(INTEGER)
    LowWindChill = Column(INTEGER)
    HeatIndex = Column(INTEGER)
    THSWIndex = Column(INTEGER)
    RainCollectorType = Column(INTEGER)
    RainCollectorInc = Column(FLOAT)
    TotalRainClicks = Column(INTEGER)
    HiRainRate = Column(INTEGER)
    ET =  Column(INTEGER)
    UV = Column(INTEGER)
    HiUV = Column(INTEGER)
    SolarRad = Column(INTEGER)
    HiSolarRad = Column(INTEGER)
    IntervalIndex = Column(INTEGER)

class PV_history_data_hourly(Base):
    __tablename__ = 'history_data_pv_hourly'

    TIME_STAMP = Column(INTEGER, primary_key=True)

    ENV_TEMP = Column(FLOAT)
    SOLAR_RAD = Column(FLOAT)
    SOLAR_POW = Column(FLOAT)  # The solar power output should be unified.

class PV_history_data_minutely(Base):
    __tablename__ = 'history_data_pv_minutely'

    TIME_STAMP = Column(INTEGER, primary_key=True)

    ENV_TEMP = Column(FLOAT)
    SOLAR_RAD = Column(FLOAT)
    SOLAR_POW = Column(FLOAT)  # The solar power output should be unified.

class PV_history_data_real_time(Base):
    __tablename__ = 'history_data_pv_real_time'

    TIME_STAMP = Column(INTEGER, primary_key=True)

    ENV_TEMP = Column(FLOAT)
    SOLAR_RAD = Column(FLOAT)
    SOLAR_POW = Column(FLOAT)  # The solar power output should be unified.