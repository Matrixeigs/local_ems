#This function is to manage the PV history data in Singapore
#By queryin the database on the Server
#This procedure will be implemented every one hour or every 5 minutes, these informatin are for the UC or ED
#In the further, this function will be implemented automaticly.

import os
import time
from data_management.database_format import  weather_station_data,PV_history_data_hourly,PV_history_data_minutely,PV_history_data_real_time
from sqlalchemy import create_engine, and_
from sqlalchemy.orm import sessionmaker
from numpy import mean

db_str_pv = 'mysql+pymysql://' + 'microgrid' + ':' + 'microgrid' + '@' + '155.69.218.214' + '/' + 'weather_station'
db_str = 'mysql+pymysql://' + 'root' + ':' + 'Ntu@1003' + '@' + 'localhost' + '/' + 'emsdb'

Time = 1
Start_time='2017-03-21 00:00:00'
End_time='2017-03-29 23:59:00'

Start_time_int = int(time.mktime(time.strptime(Start_time, '%Y-%m-%d %H:%M:%S')))

End_time_int= int(time.mktime(time.strptime(End_time, '%Y-%m-%d %H:%M:%S')))

engine = create_engine(db_str_pv, echo=False)
Session = sessionmaker(bind=engine)
session = Session()

engine_BBB = create_engine(db_str, echo=False)
Session_BBB = sessionmaker(bind=engine_BBB)
session_BBB = Session_BBB()
T = 6
Solar_minutely = []
Solar_hourly = []
Solar_per5min= []

History_data = session.query(weather_station_data.SolarRad).filter(and_(weather_station_data.RecDateTime >= Start_time, weather_station_data.RecDateTime <= End_time)).all()


for i in range(T*1440):
    Solar_minutely.append(History_data[i][0])

Solar_max = max(Solar_minutely)
Solar_min = min(Solar_minutely)

for i in range(T*288):
    Solar_per5min.append(float(mean(Solar_minutely[i*5:(i+1)*5])))

for i in range(T*24):
    Solar_hourly.append(float(mean(Solar_minutely[i*60:(i+1)*60])))

#Save datas to the database
for i in range(1,T*24+1):
    PV_hourly_data = PV_history_data_hourly(
        TIME_STAMP = i,
        ENV_TEMP=0,
        SOLAR_RAD = (Solar_hourly[i-1] - Solar_min)/(Solar_max-Solar_min),
        SOLAR_POW = 0,
        )
    if session_BBB.query(PV_history_data_hourly).filter_by(TIME_STAMP = i ).count() == 0:  # if the data do not exist, the data will be inserted
        session_BBB.add(PV_hourly_data)
        session_BBB.commit()
    else:
        row = session_BBB.query(PV_history_data_hourly).filter_by(TIME_STAMP = ( i )).first()
        row.SOLAR_RAD = (Solar_hourly[i-1] - Solar_min)/(Solar_max-Solar_min)
        session_BBB.commit()

for i in range(1,T*288+1):
    PV_real_time_data = PV_history_data_real_time(
        TIME_STAMP = i ,
        ENV_TEMP=0,
        SOLAR_RAD = (Solar_per5min[i-1] - Solar_min)/(Solar_max-Solar_min),
        SOLAR_POW = 0)

    if session_BBB.query(PV_history_data_real_time).filter_by(TIME_STAMP =  i ).count() == 0:  # if the data do not exist, the data will be inserted
        session_BBB.add(PV_real_time_data)
        session_BBB.commit()
    else:
        row = session_BBB.query(PV_history_data_real_time).filter_by(TIME_STAMP = (i)).first()
        row.SOLAR_RAD = (Solar_per5min[i-1] - Solar_min)/(Solar_max-Solar_min)
        session_BBB.commit()

for i in range(1,T*1440+1):
    PV_minutely_data = PV_history_data_minutely(
        TIME_STAMP = Start_time_int + i*60,
        ENV_TEMP = 0,
        SOLAR_RAD = (Solar_minutely[i] - Solar_min)/(Solar_max-Solar_min),
        SOLAR_POW = 0)

    if session_BBB.query(PV_history_data_minutely).filter_by(TIME_STAMP = i * 60).count() == 0:  # if the data do not exist, the data will be inserted
        session_BBB.add(PV_minutely_data)
        session_BBB.commit()
    else:
        row = session_BBB.query(PV_history_data_minutely).filter_by(TIME_STAMP = ( i * 60)).first()
        row.SOLAR_RAD = (Solar_minutely[i] - Solar_min)/(Solar_max-Solar_min)
        session_BBB.commit()


print(Solar_minutely)