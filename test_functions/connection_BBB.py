#Delta_t_unit_committment=3600
#for i in range(1000):

#    Day_ahead_forecasting_result=Day_ahead_forecasting(TIME_STAMP=Base_time+i*Delta_t_unit_committment,CAC3PL_ACTPOW=0,UCAC3PL_ACTPOW=0,CDCL_MAX_POW=0,
#                                                   UCDCL_MAX_POW=0,PV_FORECASTING=0,WP_FORECASTING=0,ELEC_PRICE=0)

#    session.add(Day_ahead_forecasting_result)
    #session.query(Day_ahead_forecasting).delete('TIME_STAMP'==Base_time+i*Delta_t_unit_committment)
#    session.commit()
#Modify the real time operation data and
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from default_setting import default_BBB_database,default_local_database
from database_format import Test_data_database_format

db_str_BBB = default_BBB_database.db_str

db_str = default_local_database.db_str

engine = create_engine(db_str_BBB, echo=False)
engine_local = create_engine(db_str, echo=False)

Session = sessionmaker(bind=engine)
Session_local = sessionmaker(bind=engine_local)

session = Session()
session_local = Session_local()

for i in range(8760,8761):
    row=session_local.query(Test_data_database_format).filter_by(TIME_STAMP=i).first()

    updated_items=Test_data_database_format(TIME_STAMP = i,
                                            LOAD_COMMERCIAL=row.LOAD_COMMERCIAL,
                                            LOAD_RESIDENT=row.LOAD_RESIDENT,
                                            PV_OUTPUT=row.PV_OUTPUT,
                                            ELEC_PRICE=row.ELEC_PRICE)
    session.add(updated_items)

    if i%100==0: print(i)
    session.commit()
