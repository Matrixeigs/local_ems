from cvxopt.modeling import variable,op
from math import ceil
from cvxopt import solvers

def economic_dispatch_recovery(Delta_t,DG,UG,PV,WP,ESS,BIC,LOAD_AC,LOAD_AC_NC,LOAD_DC,LOAD_DC_NC,COST,RESOURCE_MANAGER,UNCERTAINTY,UC):
    #Define the decision variables firstly
    #Pg,Pug,Pess_c,Pess_dc,Pa2d,Pd2a
    #Ru_dg,Rd_dg,Ru_ug,Rd_ug,Ru_ess,Rd_ess
    #Eess, DG_fuel
    # In this model, additional variables should be integrated
    DG_status = min(RESOURCE_MANAGER['DG_STATUS'],UC['DG'])
    UG_status = min(RESOURCE_MANAGER['UG_STATUS'], UC['UG'])

    Pg_min = DG_status * DG['Pmin']
    Pg_max = DG_status * DG['Pmax']

    Pg=variable(1)
    Cg_boudary_lower = (Pg >= Pg_min)
    Cg_boudary_upper = (Pg <= Pg_max)

    Ru_dg_min = DG_status * DG['Pmin']
    Ru_dg_max = DG_status * DG['Pmax']

    Ru_dg = variable(1)
    Ru_dg_boudary_lower = (Ru_dg >= Ru_dg_min)
    Ru_dg_boudary_upper = (Ru_dg <= Ru_dg_max)

    Rd_dg_min = DG_status * DG['Pmin']
    Rd_dg_max = DG_status * DG['Pmax']

    Rd_dg = variable(1)
    Rd_dg_boudary_lower = (Rd_dg >= Ru_dg_min)
    Rd_dg_boudary_upper = (Rd_dg <= Rd_dg_max)

    Pug_min = UG_status * UG['Pmin']
    Pug_max = UG_status * UG['Pmax']

    Pug = variable(1)
    Pug_boudary_lower = (Pug >= Pug_min)
    Pug_boudary_upper = (Pug <= Pug_max)

    Ru_ug_min = UG_status * UG['Pmin']
    Ru_ug_max = UG_status * UG['Pmax']

    Ru_ug = variable(1)
    Ru_ug_boudary_lower = (Ru_ug >= Ru_ug_min)
    Ru_ug_boudary_upper = (Ru_ug <= Ru_ug_max)

    Rd_ug_min = UG_status * UG['Pmin']
    Rd_ug_max = UG_status * UG['Pmax']

    Rd_ug = variable(1)
    Rd_ug_boudary_lower = (Rd_ug >= Rd_ug_min)
    Rd_ug_boudary_upper = (Rd_ug <= Rd_ug_max)

    Pess_c_min = 0
    Pess_c_max = RESOURCE_MANAGER['ESS_STATUS'] * ESS['Pmax_charging']

    Pess_c = variable(1)
    Pess_c_boudary_lower = (Pess_c >= Pess_c_min)
    Pess_c_boudary_upper = (Pess_c <= Pess_c_max)

    Pess_dc_min = 0
    Pess_dc_max = RESOURCE_MANAGER['ESS_STATUS'] * ESS['Pmax_discharging']

    Pess_dc = variable(1)
    Pess_dc_boudary_lower = (Pess_dc >= Pess_dc_min)
    Pess_dc_boudary_upper = (Pess_dc <= Pess_dc_max)

    Ru_ess_min = 0
    Ru_ess_max = RESOURCE_MANAGER['ESS_STATUS'] * (ESS['Pmax_discharging']+ESS['Pmax_charging'])

    Ru_ess = variable(1)
    Ru_ess_boudary_lower = (Ru_ess >= Ru_ess_min)
    Ru_ess_boudary_upper = (Ru_ess <= Ru_ess_max)

    Rd_ess_min = 0
    Rd_ess_max = RESOURCE_MANAGER['ESS_STATUS'] * (ESS['Pmax_discharging']+ESS['Pmax_charging'])

    Rd_ess = variable(1)
    Rd_ess_boudary_lower = (Rd_ess >= Rd_ess_min)
    Rd_ess_boudary_upper = (Rd_ess <= Rd_ess_max)

    Pa2d_min = 0
    Pa2d_max = BIC['Capacity']

    Pa2d = variable(1)
    Pa2d_boudary_lower = (Pa2d >= Pa2d_min)
    Pa2d_boudary_upper = (Pa2d <= Pa2d_max)

    Pd2a_min = 0
    Pd2a_max = BIC['Capacity']

    Pd2a = variable(1)
    Pd2a_boudary_lower = (Pd2a >= Pd2a_min)
    Pd2a_boudary_upper = (Pd2a <= Pd2a_max)

    Eess_min = RESOURCE_MANAGER['ESS_STATUS'] * ESS['ESS_min']
    Eess_max = RESOURCE_MANAGER['ESS_STATUS'] * ESS['ESS_max']

    Eess = variable(1)
    Eess_boudary_lower = (Eess >= Eess_min)
    Eess_boudary_upper = (Eess <= Eess_max)


    DG_fuel_min = DG['Fuel_limitation']
    DG_fuel_max = DG['Fuel0']

    DG_fuel = variable(1)
    DG_fuel_boudary_lower = (DG_fuel >= DG_fuel_min)
    DG_fuel_boudary_upper = (DG_fuel <= DG_fuel_max)

    Pl_ac_min= 0
    Pl_ac_max = LOAD_AC['Forecast']

    Pl_ac = variable(1)
    Pl_ac_boundary_lower = (Pl_ac >= Pl_ac_min)
    Pl_ac_boundary_upper = (Pl_ac <= Pl_ac_max)

    Pl_dc_min = 0
    Pl_dc_max = LOAD_DC['Forecast']

    Pl_dc=variable(1)
    Pl_dc_boundary_lower = (Pl_dc >= Pl_dc_min)
    Pl_dc_boundary_upper = (Pl_dc <= Pl_dc_max)

    Pl_ac_nc_min = 0
    Pl_ac_nc_max = LOAD_AC_NC['Forecast']

    Pl_ac_nc = variable(1)
    Pl_ac_nc_boundary_lower = (Pl_ac_nc >= Pl_ac_nc_min)
    Pl_ac_nc_boundary_upper = (Pl_ac_nc <= Pl_ac_nc_max)

    Pl_dc_nc_min = 0
    Pl_dc_nc_max = LOAD_DC_NC['Forecast']

    Pl_dc_nc = variable(1)
    Pl_dc_nc_boundary_lower = (Pl_dc_nc >= Pl_dc_nc_min)
    Pl_dc_nc_boundary_upper = (Pl_dc_nc <= Pl_dc_nc_max)

    Ppv_min = 0
    Ppv_max = PV['Forecast']

    Ppv = variable(1)
    Ppv_boundary_lower = (Ppv >= Ppv_min)
    Ppv_boundary_upper = (Ppv <= Ppv_max)

    Pwp_min = 0
    Pwp_max = WP['Forecast']

    Pwp = variable(1)
    Pwp_boundary_lower = (Pwp >= Pwp_min)
    Pwp_boundary_upper = (Pwp <= Pwp_max)
    # 3) The power balance constraint
    Power_balance_constraint_ac=(Pg+Pug+Pd2a*BIC['Efficiency_D2A']-Pa2d== Pl_ac+ Pl_ac_nc)

    Power_balance_constraint_dc = (Pess_dc - Pess_c + Pa2d * BIC['Efficiency_A2D'] - Pd2a == Pl_dc + Pl_dc_nc - Ppv- Pwp)

    # 4)Energy tranfer
    Ess_dynamic=(Eess==Pess_c*ESS['Efficiency_charging'] * Delta_t-Pess_dc/ESS['Efficiency_charging'] * Delta_t+ESS['ESS0'])

    DG_fuel_dynamic=(DG_fuel==DG['Fuel0'] - DG_status * COST['b_g'] * Delta_t-Pg * Delta_t * COST['a_g'])

    # 5)Pg+Pu_dg<=Pmax
    Ru_cap=(Pg+Ru_dg<=Pg_max)

    # 6)Pg+Pu_dg<=Pmax
    Rd_cap = (Pg - Rd_dg >= Pg_min)

    # 7) -Pess_c+Pess_dc+Ru_ess<=Pess_dc_max
    Ress_u_cap=(Pess_dc -Pess_c +Ru_ess<=Pess_dc_max)
    # 8) Pess_c-Pess_dc+Rd_ess<=Pess_c_max
    Ress_d_cap = (Pess_c - Pess_dc + Rd_ess <= Pess_c_max)
    # 9)Pug+Ru_ug<=Pmax
    Ru_ug_cap = (Pug + Ru_ug <= Pug_max)
    # 10)Pug-Rd_ug>=Pmin
    Rd_ug_cap = (Pug - Rd_ug >= Pug_min)
    # 11)
    Ru_ess_energy = (Eess - Ru_ess * Delta_t / ESS['Efficiency_discharging'] >= ESS['ESS_min'])
    Rd_ess_energy = (Eess + Rd_ess * Delta_t * ESS['Efficiency_charging'] <= ESS['ESS_max'])
    #12)
    Ru_dg_energy = (DG_fuel- Ru_dg * Delta_t * COST['a_g']-DG_status * COST['b_g']*Delta_t>=DG['Fuel_limitation'])
    #13)
    #Reserve_demand = LOAD_AC['Forecast'] * UNCERTAINTY['Disturbance_range_ac'] + LOAD_AC_NC['Forecast'] * UNCERTAINTY['Disturbance_range_ac_nc'] + LOAD_DC['Forecast'] * UNCERTAINTY['Disturbance_range_dc'] \
                     #+ LOAD_DC_NC['Forecast'] * UNCERTAINTY['Disturbance_range_dc_nc'] + PV['Forecast']*UNCERTAINTY['Disturbance_range_pv'] + WP['Forecast']*UNCERTAINTY['Disturbance_range_wp']
    Up_reserve_limitation = (Ru_dg + Ru_ug + Ru_ess >= Pl_ac * UNCERTAINTY['Disturbance_range_ac'] + Pl_ac_nc * UNCERTAINTY['Disturbance_range_ac_nc'] + Pl_dc * UNCERTAINTY['Disturbance_range_dc'] + Pl_dc_nc * UNCERTAINTY['Disturbance_range_dc_nc'] + Ppv*UNCERTAINTY['Disturbance_range_pv'] + Pwp * UNCERTAINTY['Disturbance_range_wp'])
    Down_reserve_limitation = (Rd_dg + Rd_ug + Rd_ess >= Pl_ac * UNCERTAINTY['Disturbance_range_ac'] + Pl_ac_nc * UNCERTAINTY['Disturbance_range_ac_nc'] + Pl_dc * UNCERTAINTY['Disturbance_range_dc'] + Pl_dc_nc * UNCERTAINTY['Disturbance_range_dc_nc'] + Ppv*UNCERTAINTY['Disturbance_range_pv'] + Pwp * UNCERTAINTY['Disturbance_range_wp'])

    # Economic dispatch model
    lp=op(COST['Cfuel']*COST['a_g']*Delta_t*Pg+COST['Ug_price']*Delta_t*Pug+COST['Cess']*Delta_t*Pess_c+COST['Cess']*Delta_t*Pess_dc+COST['Cfuel']*COST['b_g']*Delta_t*DG_status+COST['PV_curtailment']*(PV['Forecast']-Ppv)*Delta_t+COST['WP_curtailment']*(WP['Forecast']-Pwp)*Delta_t+COST['Cac_nc']*(LOAD_AC_NC['Forecast']-Pl_ac_nc)*Delta_t+COST['Cdc_nc']*(LOAD_DC_NC['Forecast']-Pl_dc_nc)*Delta_t+COST['Cac']*(LOAD_AC['Forecast']-Pl_ac)*Delta_t+COST['Cdc']*(LOAD_DC['Forecast']-Pl_dc)*Delta_t,
          [Cg_boudary_lower,Cg_boudary_upper,Ru_dg_boudary_lower,Ru_dg_boudary_upper,Rd_dg_boudary_lower,Rd_dg_boudary_upper,Pug_boudary_lower,Pug_boudary_upper,Ru_ug_boudary_lower,Ru_ug_boudary_upper,Rd_ug_boudary_lower,Rd_ug_boudary_upper,
           Pess_c_boudary_lower,Pess_c_boudary_upper,Pess_dc_boudary_lower,Pess_dc_boudary_upper,Ru_ess_boudary_lower,Ru_ess_boudary_upper,Rd_ess_boudary_lower,Rd_ess_boudary_upper,
           Pa2d_boudary_lower,Pa2d_boudary_upper,Pd2a_boudary_lower,Pd2a_boudary_upper,Eess_boudary_lower,Eess_boudary_upper,DG_fuel_boudary_lower,DG_fuel_boudary_upper,
           Power_balance_constraint_ac,Power_balance_constraint_dc,Ess_dynamic,DG_fuel_dynamic,
           Ru_cap,Rd_cap,Ress_u_cap,Ress_d_cap,Ru_ug_cap,Rd_ug_cap,Ru_ess_energy,
           Pl_ac_boundary_lower,Pl_ac_boundary_upper,
           Pl_dc_boundary_lower,Pl_dc_boundary_upper,
           Pl_ac_nc_boundary_lower,Pl_ac_nc_boundary_upper,
           Pl_dc_nc_boundary_lower, Pl_dc_nc_boundary_upper,
           Ppv_boundary_lower,Ppv_boundary_upper,
           Pwp_boundary_lower,Pwp_boundary_upper,
           Rd_ess_energy,Ru_dg_energy,Up_reserve_limitation,Down_reserve_limitation])

    solvers.options['show_progress'] = False

    lp.solve()

    if lp.status=='optimal':#A feasible and optimal solution has been obtained
        economic_dispatch_solution = {'Pg': Pg.value[0]}
        economic_dispatch_solution['Ig'] =  DG_status
        economic_dispatch_solution['Pug'] = Pug.value[0]
        economic_dispatch_solution['Iug'] = UG_status
        economic_dispatch_solution['Pess'] = Pess_dc.value[0]-Pess_c.value[0]
        economic_dispatch_solution['Pbic'] = Pa2d.value[0]-Pd2a.value[0]
        economic_dispatch_solution['Eess'] = Eess.value[0]
        economic_dispatch_solution['DG_fuel'] = DG_fuel.value[0]
        economic_dispatch_solution['obj'] = lp.objective.value()[0]
        #We can also obtain the Langrange mutilper
        #Detect which type of load should be curtailed
        #The curtailment scale of PV
        if PV['Forecast']!=0:
            economic_dispatch_solution['Ipv'] = ceil(RESOURCE_MANAGER.NPV-RESOURCE_MANAGER.NPV*Ppv.value/PV['Forecast'])
        else:
            economic_dispatch_solution['Ipv'] = 0

        if WP['Forecast']!=0:
            economic_dispatch_solution['Iwp'] = ceil(RESOURCE_MANAGER.NWP-RESOURCE_MANAGER.NPV*Pwp.value/WP['Forecast'])
        else:
            economic_dispatch_solution['Iwp'] = 0

        Pl_ac_real=Pl_ac.value[0]
        Pl_dc_real = Pl_dc.value[0]
        Pl_ac_nc_real = Pl_ac_nc.value[0]
        Pl_dc_nc_real = Pl_dc_nc.value[0]

        #The shedding of AC load
        if abs(Pl_ac_real+Pl_ac_nc_real-LOAD_AC['Forecast']-LOAD_AC_NC['Forecast'])<=0.1:
            economic_dispatch_solution['Iac'] = 0
            economic_dispatch_solution['Iac_nc'] = 0
            if UC['Iac'] == 1:
                economic_dispatch_solution['Iac'] = 1
            if UC['Iac_nc'] == 1:
                economic_dispatch_solution['Iac_nc'] = 1
        elif Pl_ac_real+Pl_ac_nc_real-LOAD_AC['Forecast']>=0:
            economic_dispatch_solution['Iac'] = 0
            economic_dispatch_solution['Iac_nc'] = 1
            if UC['Iac'] == 1:
                economic_dispatch_solution['Iac'] = 1
        else:
            economic_dispatch_solution['Iac'] = 1
            economic_dispatch_solution['Iac_nc'] = 1

        #The shedding of DC load
        if abs(Pl_dc_real+Pl_dc_nc_real-LOAD_DC['Forecast']-LOAD_DC_NC['Forecast'])<=0.1:
            economic_dispatch_solution['Idc'] = 0
            economic_dispatch_solution['Idc_nc'] = 0
            if UC['Idc'] == 1:
                economic_dispatch_solution['Idc'] = 1
            if UC['Idc_nc'] == 1:
                economic_dispatch_solution['Idc_nc'] = 1
        elif Pl_dc_real+Pl_dc_nc_real-LOAD_DC['Forecast']>=0:
            economic_dispatch_solution['Idc'] = 0
            economic_dispatch_solution['Idc_nc'] = 1
            if UC['Idc'] == 1:
                economic_dispatch_solution['Idc'] = 1
        else:
            economic_dispatch_solution['Idc'] = 1
            economic_dispatch_solution['Idc_nc'] = 1
    else:
        economic_dispatch_solution = {'Pg': 0}
        economic_dispatch_solution['Ig'] = 0
        economic_dispatch_solution['Pug'] = 0
        economic_dispatch_solution['Iug'] = 0
        economic_dispatch_solution['Pess'] = 0
        economic_dispatch_solution['Pbic'] = 0
        economic_dispatch_solution['Eess'] = 0
        economic_dispatch_solution['DG_fuel'] = 0
        economic_dispatch_solution['obj'] = 0
        economic_dispatch_solution['Iac'] = 0
        economic_dispatch_solution['Iac_nc'] = 0
        economic_dispatch_solution['Idc'] = 0
        economic_dispatch_solution['Idc_nc'] = 0
        economic_dispatch_solution['Ipv'] = 0
        economic_dispatch_solution['Iwp'] = 0

    #Return the solutions
    return economic_dispatch_solution