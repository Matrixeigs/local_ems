from pyscipopt import Model
#Input informations
DG={'Capacity':9.6,
    'Pmax':9.6,
    'Pmin':0,
    'Fuel_capacity':9.3,
    'Fuel_limitation':0,
    'Fuel_warning':4,
    'Fuel0':9.3,
     }

UG={'Capacity':10,
    'Power_factor_limitation':0.9,
    'Pmax':10,
    'Pmin':0,
    }

WP={'Capacity':0,
    'NWP':0,
    'Forecast':0,
    }

PV={'Capacity':10,
    'NPV':6,
    'Forecast':0,
    }

###2)Energy storage systems
ESS={'Capacity':10,
     'Pmax_discharging':5,
     'Pmax_charging':5,
     'Efficiency_discharging':0.95,
     'Efficiency_charging':0.95,
     'ESS_max':10,
     'ESS_min':2,
     'SOC_max':1,
     'SOC_min':0.2,
     'ESS0':5,
     }

###3)BIC information
#The positive is from AC 2 DC on BIC
BIC={'Capacity':5,
     'Efficiency_A2D':0.95,
     'Efficiency_D2A':0.95,
     }

###4)Load informations
LOAD_AC={'Capacity':5,
     'Power_factor':0.9,
     'Maximal_curtailment_time':10,
     'Forecast':3,
     }

LOAD_AC_NC={'Capacity':5,
     'Power_factor':0.9,
     'Maximal_curtailment_time':10,
     'Forecast':3,
     }

LOAD_DC={'Capacity':5,
     'Maximal_curtailment_time':10,
     'Forecast':3,
     }

LOAD_DC_NC={'Capacity':5,
     'Maximal_curtailment_time':10,
     'Forecast':3,
     }

###5)Cost informations
COST={'Cfuel':1.3,
      'Cess':0.02,
      'a_g':0.26,
      'b_g':0.6,
      'PV_curtailment':2,
      'WP_curtailment':2,
      'Cac_nc':10,
      'Cdc_nc':10,
      'Cac':100,
      'Cdc':100,
      'Ug_price':0.08,
      }

###6)Resources manager informaition,by default, all devicecs are online
RESOURCE_MANAGER={
    'NPV':6,
    'NWP':0,
    'DG_STATUS':1,
    'UG_STATUS':1,
    'ESS_STATUS':1,
    'BIC_STATUS':1,
      }
###7) Uncertainty information
UNCERTAINTY={
    'Disturbance_range_ac':0.1,
    'Disturbance_range_ac_nc':0.1,
    'Disturbance_range_dc':0.1,
    'Disturbance_range_dc_nc':0.1,
    'Disturbance_range_pv':0.1,
    'Disturbance_range_wp':0.1,
}

#Obtain the forecasting information from the forecasting database
Load_profile_ac=[2.77124,2.80655,2.83889,2.88904,3.02493,3.22008]
Load_profile_ac_nc=[2.77124,2.80655,2.83889,2.88904,3.02493,3.22008]
Load_profile_dc=[0.253247,0.253413,0.263015,0.254706,2.49743,2.92667]
Load_profile_dc_nc=[0.253247,0.253413,0.263015,0.254706,2.49743,2.92667]
PV_profile=[0,0,0,0,0,0]
WP_profile=[0,0,0,0,0,0]
#Obtain the maintance information from the maintance databse




model=Model("IUC")
Ng=1
T=6



Ig={}#The start-up and shut down of diesel generators
Pg={}#Output of generators
Ru_dg={}#Ramp up reserve
Rd_dg={}#Ramp down reserve
DG_fuel={}#Fuel state of the diesel generators

Iug={}
Pug={}
Ru_ug={}
Rd_ug={}

Pess_c={}
Pess_dc={}
Eess={}
Ru_ess={}
Rd_ess={}

Pa2d={}
Pd2a={}
Ia2d={}
Iess={}

Delta_t=1

Reserve_demand={}


#Variables announcement
for i in range(T):
    Ig[i]= model.addVar(vtype="BINARY",obj=COST['a_g']*COST['Cfuel'])
    Pg[i] = model.addVar(obj=COST['b_g']*COST['Cfuel'],lb=DG['Pmin'],ub=DG['Pmax'])
    Ru_dg[i] = model.addVar(lb=0,ub=DG['Pmax'])
    Rd_dg[i] = model.addVar(lb=0,ub=DG['Pmax'])
    DG_fuel[i]  = model.addVar(lb=DG['Fuel_limitation'],ub=DG['Fuel0'])

    Iug[i] = model.addVar(vtype="BINARY")#The fuel cost
    Pug[i] = model.addVar(obj=COST['Ug_price'],lb=UG['Pmin'],ub=UG['Pmax'])
    Ru_ug[i] = model.addVar(lb=0,ub=UG['Pmax']-UG['Pmin'])
    Rd_ug[i] = model.addVar(lb=0,ub=UG['Pmax']-UG['Pmin'])

    Iess[i] = model.addVar(vtype="BINARY")
    Pess_c[i] = model.addVar(obj=COST['Cess'],lb=0,ub=ESS['Pmax_charging'])
    Pess_dc[i]= model.addVar(obj=COST['Cess'],lb=0,ub=ESS['Pmax_discharging'])
    Eess[i] = model.addVar( lb=ESS['ESS_min'], ub=ESS['ESS_max'])
    Ru_ess[i] = model.addVar( lb=0, ub=ESS['Pmax_discharging']+ESS['Pmax_charging'])
    Rd_ess[i] = model.addVar( lb=0, ub=ESS['Pmax_discharging']+ESS['Pmax_charging'])

    Pa2d[i] = model.addVar( lb=0, ub=BIC['Capacity'])
    Pd2a[i] = model.addVar( lb=0, ub=BIC['Capacity'])
    Ia2d[i] = model.addVar(vtype="BINARY")

ESS_discharging_efficiency=1/ESS['Efficiency_discharging']

for i in range(T):
    model.addCons(Iug[i] + Ig[i]<=1)#C
    model.addCons(Pg[i] + Ru_dg[i] <= Ig[i] * DG['Pmax'])
    model.addCons(Pg[i] - Rd_dg[i] >= Ig[i] * DG['Pmin'])
    if i==0:
        model.addCons(DG_fuel[i] == DG['Fuel0'] - Ig[i] * COST['a_g'] * Delta_t - Pg[i] * COST['b_g'] * Delta_t)
    else:
        model.addCons(DG_fuel[i] == DG_fuel[i-1] - Ig[i] * COST['a_g'] * Delta_t - Pg[i] * COST['b_g'] * Delta_t)

    model.addCons(DG_fuel[i]- Ru_dg[i] * COST['b_g'] * Delta_t>=DG['Fuel_limitation'])#Reserve maintance constraint

    #The utility grid part
    model.addCons(Pug[i] + Ru_ug[i] <= Iug[i] * UG['Pmax'])
    model.addCons(Pug[i] - Rd_ug[i] >= Iug[i] * UG['Pmin'])

    #The Energy storage part
    model.addCons(Pess_c[i] <= Iess[i] * ESS['Pmax_charging'])
    model.addCons(Pess_dc[i] <= (1-Iess[i]) * ESS['Pmax_discharging'])
    model.addCons(Pess_dc[i]- Pess_c[i] + Ru_ess[i] <=  ESS['Pmax_discharging'])
    model.addCons(Pess_c[i] - Pess_dc[i] + Rd_ess[i] <= ESS['Pmax_charging'])
    if i==0:
        model.addCons(Eess[i] == ESS['ESS0'] - Pess_dc[i] * Delta_t * ESS_discharging_efficiency  + Pess_c[i] * ESS['Efficiency_charging'] * Delta_t)
    else:
        model.addCons(Eess[i] == Eess[i-1] - Pess_dc[i] * ESS_discharging_efficiency * Delta_t + Pess_c[i] * ESS['Efficiency_charging'] * Delta_t)

    model.addCons(Eess[i] + Rd_ess[i] * ESS['Efficiency_charging'] * Delta_t <= ESS['ESS_max'])
    model.addCons(Eess[i] - Ru_ess[i] * ESS_discharging_efficiency * Delta_t >= ESS['ESS_min'])

    #The BIC part
    model.addCons(Pa2d[i] <=Ia2d[i] * BIC['Capacity'])
    model.addCons(Pd2a[i] <= (1-Ia2d[i]) * BIC['Capacity'])

    #Power balance constraints
    #AC side
    model.addCons(Pug[i] + Pg[i] + Pd2a[i] * BIC['Efficiency_D2A'] == Load_profile_ac[i] + Load_profile_ac_nc[i] + Pa2d[i])
    model.addCons(Pess_dc[i] + PV_profile[i] + WP_profile[i] + Pa2d[i] * BIC['Efficiency_A2D'] == Load_profile_dc[i] + Load_profile_dc_nc[i] + Pess_c[i] + Pd2a[i])

    #Reserve constraints
    Reserve_demand[i]=Load_profile_ac[i] * UNCERTAINTY['Disturbance_range_ac'] + Load_profile_ac_nc[i] * UNCERTAINTY['Disturbance_range_ac_nc'] + Load_profile_dc[i] *UNCERTAINTY ['Disturbance_range_dc'] \
                      + Load_profile_dc_nc[i] * UNCERTAINTY['Disturbance_range_dc_nc'] + PV_profile[i] * UNCERTAINTY ['Disturbance_range_pv'] + WP_profile[i]*UNCERTAINTY['Disturbance_range_wp']
    model.addCons(Ru_ess[i] + Ru_dg[i] + Ru_ug[i]>=Reserve_demand[i])
    model.addCons(Rd_ess[i] + Rd_dg[i] + Rd_ug[i] >= Reserve_demand[i])


model.hideOutput(False)#The output commamnd

model.optimize()

sol=model.getStatus()
print(sol=='optimal')

Ig_real={}
Iug_real={}

for i in range(T):
    Ig_real[i]=model.getVal(Ig[i])
    Iug_real[i] = model.getVal(Iug[i])

print(Ig_real)
print(Iug_real)
