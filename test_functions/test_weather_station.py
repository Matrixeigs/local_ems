from sqlalchemy import create_engine,and_
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, FLOAT, INTEGER, DATETIME
import time

db_str_microgrid = 'mysql+pymysql://' + 'microgrid' + ':' + 'microgrid' + '@' + '155.69.227.32:3306/weather_station'

Base = declarative_base()

class weather_station_dataformat(Base):
    __tablename__ = 'ISSData'

    ReceiverRecID = Column(INTEGER)

    ChannelIndex = Column(INTEGER)
    RecDateTime = Column(DATETIME, primary_key=True)

    TempOut = Column(INTEGER)

    HiTempOut = Column(INTEGER)  # The solar power output should be unified.
    LowTempOut = Column(INTEGER)
    HumOut = Column(INTEGER)
    WindSpeed = Column(INTEGER)
    ScalerAvgWindDir = Column(INTEGER)
    HiWindSpeed = Column(INTEGER)
    HiWindDir = Column(INTEGER)
    DominantDir = Column(INTEGER)
    DewPoint = Column(INTEGER)
    LowWindChill = Column(INTEGER)
    HeatIndex = Column(INTEGER)
    THSWIndex = Column(INTEGER)
    RainCollectorType = Column(INTEGER)
    RainCollectorInc = Column(INTEGER)  # The solar power output should be unified.
    TotalRainClicks = Column(INTEGER)
    HiRainRate = Column(INTEGER)
    ET = Column(INTEGER)
    UV = Column(INTEGER)
    HiUV = Column(INTEGER)
    SolarRad = Column(INTEGER)
    HiSolarRad = Column(INTEGER)
    IntervalIndex = Column(INTEGER)


engine = create_engine(db_str_microgrid, echo=False)
Session = sessionmaker(bind=engine)
session = Session()
time_now=time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

pv = session.query(weather_station_dataformat.SolarRad).filter( weather_station_dataformat.RecDateTime < time_now).all()

print(pv)