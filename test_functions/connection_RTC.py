#Delta_t_unit_committment=3600
#for i in range(1000):

#    Day_ahead_forecasting_result=Day_ahead_forecasting(TIME_STAMP=Base_time+i*Delta_t_unit_committment,CAC3PL_ACTPOW=0,UCAC3PL_ACTPOW=0,CDCL_MAX_POW=0,
#                                                   UCDCL_MAX_POW=0,PV_FORECASTING=0,WP_FORECASTING=0,ELEC_PRICE=0)

#    session.add(Day_ahead_forecasting_result)
    #session.query(Day_ahead_forecasting).delete('TIME_STAMP'==Base_time+i*Delta_t_unit_committment)
#    session.commit()
#Modify the real time operation data and
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from default_setting import default_network_database
from database_format import Real_time_operation_database_format

db_str = default_network_database.db_str_microgrid

engine = create_engine(db_str, echo=False)
Delta_t_economic_dispatch=60
Session = sessionmaker(bind=engine)
Base_time_test=1487293200
session = Session()
import random
from default_setting import default_resources_settings
for i in range(2,20170):

    updated_items=Real_time_operation_database_format(TIME_STAMP = Base_time_test + (i - 2) * Delta_t_economic_dispatch,
                                                      DG_ACTPOW = random.randint(2,4)*default_resources_settings.BASE_VA,
                                                      UG_X_ACTPOW = 0)
    if session.query(Real_time_operation_database_format).filter_by(TIME_STAMP=Base_time_test + (i - 2) * Delta_t_economic_dispatch).count()==0:
        session.add(updated_items)
    else:
        row=session.query(Real_time_operation_database_format).filter_by(TIME_STAMP=Base_time_test + (i - 2) * Delta_t_economic_dispatch).first()
        row.DG_ACTPOW = random.randint(2,4)*default_resources_settings.BASE_VA
        row.UG_X_ACTPOW=0.00
    #
    if i%100==0: print(i)
    session.commit()
