##Formulate the mathmatical model of economic dispatch
#The modeling is based on the cvxopt
#http://cvxopt.org/
from cvxopt.modeling import variable, op
from economic_dispatch_recovery import economic_dispatch_recovery
from cvxopt import solvers

def economic_dispatch_model_formulaiton_soluation(Delta_t,DG,UG,PV,WP,ESS,BIC,LOAD_AC,LOAD_AC_NC,LOAD_DC,LOAD_DC_NC,COST,RESOURCE_MANAGER,UNCERTAINTY,UC):
    #Define the decision variables firstly
    #Pg,Pug,Pess_c,Pess_dc,Pa2d,Pd2a
    #Ru_dg,Rd_dg,Ru_ug,Rd_ug,Ru_ess,Rd_ess
    #Eess, DG_fuel
    DG_status = min(RESOURCE_MANAGER['DG_STATUS'],UC['DG'])
    UG_status = min(RESOURCE_MANAGER['UG_STATUS'], UC['UG'])

    Pg_min = DG_status * DG['Pmin']
    Pg_max = DG_status * DG['Pmax']

    Pg=variable(1)
    Cg_boudary_lower = (Pg >= Pg_min)
    Cg_boudary_upper = (Pg <= Pg_max)

    Ru_dg_min = DG_status * DG['Pmin']
    Ru_dg_max = DG_status * DG['Pmax']

    Ru_dg = variable(1)
    Ru_dg_boudary_lower = (Ru_dg >= Ru_dg_min)
    Ru_dg_boudary_upper = (Ru_dg <= Ru_dg_max)

    Rd_dg_min = DG_status * DG['Pmin']
    Rd_dg_max = DG_status * DG['Pmax']

    Rd_dg = variable(1)
    Rd_dg_boudary_lower = (Rd_dg >= Ru_dg_min)
    Rd_dg_boudary_upper = (Rd_dg <= Rd_dg_max)

    Pug_min = UG_status * UG['Pmin']
    Pug_max = UG_status * UG['Pmax']

    Pug = variable(1)
    Pug_boudary_lower = (Pug >= Pug_min)
    Pug_boudary_upper = (Pug <= Pug_max)

    Ru_ug_min = UG_status * UG['Pmin']
    Ru_ug_max = UG_status * UG['Pmax']

    Ru_ug = variable(1)
    Ru_ug_boudary_lower = (Ru_ug >= Ru_ug_min)
    Ru_ug_boudary_upper = (Ru_ug <= Ru_ug_max)

    Rd_ug_min = UG_status * UG['Pmin']
    Rd_ug_max = UG_status * UG['Pmax']

    Rd_ug = variable(1)
    Rd_ug_boudary_lower = (Rd_ug >= Rd_ug_min)
    Rd_ug_boudary_upper = (Rd_ug <= Rd_ug_max)

    Pess_c_min = 0
    Pess_c_max = RESOURCE_MANAGER['ESS_STATUS'] * ESS['Pmax_charging']

    Pess_c = variable(1)
    Pess_c_boudary_lower = (Pess_c >= Pess_c_min)
    Pess_c_boudary_upper = (Pess_c <= Pess_c_max)

    Pess_dc_min = 0
    Pess_dc_max = RESOURCE_MANAGER['ESS_STATUS'] * ESS['Pmax_discharging']

    Pess_dc = variable(1)
    Pess_dc_boudary_lower = (Pess_dc >= Pess_dc_min)
    Pess_dc_boudary_upper = (Pess_dc <= Pess_dc_max)

    Ru_ess_min = 0
    Ru_ess_max = RESOURCE_MANAGER['ESS_STATUS'] * (ESS['Pmax_discharging']+ESS['Pmax_charging'])

    Ru_ess = variable(1)
    Ru_ess_boudary_lower = (Ru_ess >= Ru_ess_min)
    Ru_ess_boudary_upper = (Ru_ess <= Ru_ess_max)

    Rd_ess_min = 0
    Rd_ess_max = RESOURCE_MANAGER['ESS_STATUS'] * (ESS['Pmax_discharging']+ESS['Pmax_charging'])

    Rd_ess = variable(1)
    Rd_ess_boudary_lower = (Rd_ess >= Rd_ess_min)
    Rd_ess_boudary_upper = (Rd_ess <= Rd_ess_max)

    Pa2d_min = 0
    Pa2d_max = BIC['Capacity']

    Pa2d = variable(1)
    Pa2d_boudary_lower = (Pa2d >= Pa2d_min)
    Pa2d_boudary_upper = (Pa2d <= Pa2d_max)

    Pd2a_min = 0
    Pd2a_max = BIC['Capacity']

    Pd2a = variable(1)
    Pd2a_boudary_lower = (Pd2a >= Pd2a_min)
    Pd2a_boudary_upper = (Pd2a <= Pd2a_max)

    Eess_min = RESOURCE_MANAGER['ESS_STATUS'] * ESS['ESS_min']
    Eess_max = RESOURCE_MANAGER['ESS_STATUS'] * ESS['ESS_max']

    Eess = variable(1)
    Eess_boudary_lower = (Eess >= Eess_min)
    Eess_boudary_upper = (Eess <= Eess_max)


    DG_fuel_min = DG['Fuel_limitation']
    DG_fuel_max = DG['Fuel0']

    DG_fuel = variable(1)
    DG_fuel_boudary_lower = (DG_fuel >= DG_fuel_min)
    DG_fuel_boudary_upper = (DG_fuel <= DG_fuel_max)


    # 3) The power balance constraint
    Power_balance_constraint_ac=(
        Pg+Pug+Pd2a*BIC['Efficiency_D2A']-Pa2d==LOAD_AC['Forecast'] + LOAD_AC_NC['Forecast'])

    Power_balance_constraint_dc = (
        Pess_dc - Pess_c + Pa2d * BIC['Efficiency_A2D'] - Pd2a == LOAD_DC['Forecast'] + LOAD_DC_NC['Forecast'] - PV['Forecast'] - WP['Forecast'])

    # 4)Energy tranfer
    Ess_dynamic=(Eess==Pess_c*ESS['Efficiency_charging'] * Delta_t-Pess_dc/ESS['Efficiency_charging'] * Delta_t+ESS['ESS0'])
    DG_fuel_dynamic=(DG_fuel==DG['Fuel0'] - DG_status * COST['b_g'] * Delta_t-Pg * Delta_t * COST['a_g'])

    # 5)Pg+Pu_dg<=Pmax
    Ru_cap=(Pg+Ru_dg<=Pg_max)

    # 6)Pg+Pu_dg<=Pmax
    Rd_cap = (Pg - Rd_dg >= Pg_min)

    # 7) -Pess_c+Pess_dc+Ru_ess<=Pess_dc_max
    Ress_u_cap=(Pess_dc -Pess_c +Ru_ess<=Pess_dc_max)
    # 8) Pess_c-Pess_dc+Rd_ess<=Pess_c_max
    Ress_d_cap = (Pess_c - Pess_dc + Rd_ess <= Pess_c_max)
    # 9)Pug+Ru_ug<=Pmax
    Ru_ug_cap = (Pug + Ru_ug <= Pug_max)
    # 10)Pug-Rd_ug>=Pmin
    Rd_ug_cap = (Pug - Rd_ug >= Pug_min)
    # 11)
    Ru_ess_energy = (Eess - Ru_ess * Delta_t / ESS['Efficiency_discharging'] >= ESS['ESS_min'])
    Rd_ess_energy = (Eess + Rd_ess * Delta_t * ESS['Efficiency_charging'] <= ESS['ESS_max'])
    #12)
    Ru_dg_energy = (DG_fuel- Ru_dg * Delta_t * COST['a_g']-DG_status * COST['b_g']*Delta_t>=DG['Fuel_limitation'])
    #13)
    Reserve_demand = LOAD_AC['Forecast'] * UNCERTAINTY['Disturbance_range_ac'] + LOAD_AC_NC['Forecast'] * UNCERTAINTY['Disturbance_range_ac_nc'] + LOAD_DC['Forecast'] * UNCERTAINTY['Disturbance_range_dc'] \
                     + LOAD_DC_NC['Forecast'] * UNCERTAINTY['Disturbance_range_dc_nc'] + PV['Forecast']*UNCERTAINTY['Disturbance_range_pv'] + WP['Forecast']*UNCERTAINTY['Disturbance_range_wp']
    Up_reserve_limitation = (Ru_dg + Ru_ug + Ru_ess >= Reserve_demand)
    Down_reserve_limitation = (Rd_dg + Rd_ug + Rd_ess >= Reserve_demand)


#Solve this model
    lp=op(COST['Cfuel']*COST['a_g']*Delta_t*Pg+COST['Ug_price']*Delta_t*Pug+COST['Cess']*Delta_t*Pess_c+COST['Cess']*Delta_t*Pess_dc+COST['Cfuel']*COST['b_g']*Delta_t*DG_status,
          [Cg_boudary_lower,Cg_boudary_upper,Ru_dg_boudary_lower,Ru_dg_boudary_upper,Rd_dg_boudary_lower,Rd_dg_boudary_upper,Pug_boudary_lower,Pug_boudary_upper,Ru_ug_boudary_lower,Ru_ug_boudary_upper,Rd_ug_boudary_lower,Rd_ug_boudary_upper,
             Pess_c_boudary_lower,Pess_c_boudary_upper,Pess_dc_boudary_lower,Pess_dc_boudary_upper,Ru_ess_boudary_lower,Ru_ess_boudary_upper,Rd_ess_boudary_lower,Rd_ess_boudary_upper,
             Pa2d_boudary_lower,Pa2d_boudary_upper,Pd2a_boudary_lower,Pd2a_boudary_upper,Eess_boudary_lower,Eess_boudary_upper,DG_fuel_boudary_lower,DG_fuel_boudary_upper,
             Power_balance_constraint_ac,Power_balance_constraint_dc,Ess_dynamic,DG_fuel_dynamic,
             Ru_cap,Rd_cap,Ress_u_cap,Ress_d_cap,Ru_ug_cap,Rd_ug_cap,Ru_ess_energy,Rd_ess_energy,Up_reserve_limitation,Down_reserve_limitation])

    solvers.options['show_progress'] = False

    lp.solve()



    if lp.status=='optimal':#A feasible and optimal solution has been obtained
        economic_dispatch_solution = {'Pg': Pg.value[0]}
        economic_dispatch_solution['Ig'] =  DG_status
        economic_dispatch_solution['Pug'] = Pug.value[0]
        economic_dispatch_solution['Iug'] = UG_status
        economic_dispatch_solution['Pess'] = Pess_dc.value[0]-Pess_c.value[0]
        economic_dispatch_solution['Pbic'] = Pa2d.value[0]-Pd2a.value[0]
        economic_dispatch_solution['Eess'] = Eess.value[0]
        economic_dispatch_solution['DG_fuel'] = DG_fuel.value[0]
        economic_dispatch_solution['obj'] = lp.objective.value()[0]
        #We can also obtain the Langrange mutilper
        economic_dispatch_solution['Iac'] = 0
        economic_dispatch_solution['Iac_nc'] = 0
        economic_dispatch_solution['Idc'] = 0
        economic_dispatch_solution['Idc_nc'] = 0
        economic_dispatch_solution['Ipv'] = 0
        economic_dispatch_solution['Iwp'] = 0
    else:#if the solution dose not exist and the recovery economic dispatch method can be applied
        economic_dispatch_solution=economic_dispatch_recovery(Delta_t,DG,UG,PV,WP,ESS,BIC,LOAD_AC,LOAD_AC_NC,LOAD_DC,LOAD_DC_NC,COST,RESOURCE_MANAGER,UNCERTAINTY,UC)

    return economic_dispatch_solution

#In the further, a generalized model will be provided