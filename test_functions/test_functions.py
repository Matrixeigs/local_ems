#This functions is built to test the functions of local EMS
#Built on 06/Dce/2016

#It is noted that all the outout of load,PV and WP are given in kW
from sqlalchemy import Column, FLOAT
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

import forecasting_load as forcast_process
from modelling_local_ems import DG
from solver import random_generation as rand

x = DG


Base = declarative_base()
#Define the class in each database
class Real_time_db_data(Base):
    __tablename__ = 'testcsv'

    load = Column(FLOAT, primary_key=True)
    wind = Column(FLOAT)




db_str = 'mysql+pymysql://' + 'root' + ':' + 'Ntu@1003' + '@' + 'localhost' + '/' + 'emsdb'
engine = create_engine(db_str, echo=False)
Session = sessionmaker(bind=engine)
session = Session()
Base.metadata.create_all(engine, checkfirst=True)

row=session.query(Real_time_db_data).filter_by(load = 4.000).first()
row.wind=23.333
session.commit()
session.close()


load_profile=forcast_process.load_forecasting_simulated()
pv_profile=forcast_process.pv_output_forecasting_simulated()
wp_profile=forcast_process.wp_output_forecasting_simulated()


#Gnerate time information

import  time
#Geenerate time
time0=int(time.time())
Nt=288*100#The sequence of timeline
Time_step=300
Time_line= [ 0 for i in range(Nt) ]#Predefine a time step table

Data_test=[ 0 for i in range(Nt)]

for i in range(Nt):
    Time_line[i]=time0+Time_step*i
#The second step is to generate the dispacth line
#To treate as the power
for i in range(Nt):
    Data_test[i]=rand.randint(0,100)

#Store the generated information to database


#Generate load information

print(Time_line)

#Implement economic dispatch

#c=[-1, 4]
#A = [[-3, 1],[1, 2]]
#b = [6, 4]
#x_bounds = ((None, None), (-3, None))
#res = lpopt.linprog(c, A_ub=A, b_ub=b, bounds=x_bounds,options = {"disp": False})

#Gen_capacity=100#The capacity of utility grid
#T=len(load_profile)

#result=[]
#Define the linear progamming
#X:=[Pg,Pw,Ppv,Load]
#The lower bounary for these foure variables are zeros
#C=[3,0,0]
#Aeq=num.matrix([1,1,1])
#Nvar=3
#Aeq=num.matrix.transpose(Aeq)
#for i in range(T):#The simulation is carried at each time step
  #  x_bounday = []
  #  lb=[]
  #  for j in range(Nvar):
  #      lb.append(0)
 #   ub=[]
 #   ub.append(Gen_capacity)
 #   ub.append(wp_profile[i])
 #   ub.append(pv_profile[i])
 #   for k in range(Nvar):
#        x_bounday.append((lb[k],ub[k]))

 #   result_temp=lpopt.linprog(C, A_eq=Aeq, b_eq=load_profile[i], bounds=x_bounday,options = {"disp": False})

#    result.append(result_temp['x'])

#Test dynamic economic dispatch
#By using a single time period simulaiton
#This is still a linear porgramming

#Aeq_update=[ [ 0 for y in range( T*Nvar ) ] for x in range( T ) ]#Define the full matrix with size T*(T*Nvar)


#for i in range(T):
#    for j in range(Nvar):
#        Aeq_update[i][i*Nvar+j]=1

#Aeq_update = num.matrix(Aeq_update)
#This is the test function for dynamic economic dispacth
#The variables are defined as follows
#X:=[Pg(t),]
#Lb=[]
#Ub=[]
#x0_bounds=[]
#C_update=[]
#for i in range(T):
#Lb.append(0)
    #Lb.append(0)
    #Lb.append(0)
    #Ub.append(Gen_capacity)
    #Ub.append(wp_profile[i])
    #    Ub.append(pv_profile[i])

#for i in range(len(Lb)):
#    x0_bounds.append((Lb[i],Ub[i]))

#for i in range(T):
#    C_update.append(C)

#C_update=num.matrix(C_update)
#C_update=num.asarray(C_update).reshape(-1)

#result_update=lpopt.linprog(C_update, A_eq=Aeq_update, b_eq=load_profile, bounds=x0_bounds,options = {"disp": False})

#plt.plot(result_update['x'])
#plt.show()

attrib
#Test unit committment
#The start up and shut down cost is not taken into consideration
#When the DG is small, we do not need to take the start up and shut down into consideration
#We can test this function
#In this function, DGs might always be on-line, when there is no power losses.
#The state of charge (SoC) should be taken into consideration
#



#Test optimal power flow
#In the optimal power flow, the BIC model is simplified to a current source model
#There are two methods to test this
#Firstly, based on CVXOPT to provide the initial point
#Secondly, based on IPOPT




#plt.plot(result)
#plt.show()

#print(load_profile)
#print(pv_profile)
#print(wp_profile)

#Do real time dispatch simulation
#

#Test on
#Test the linear programming solver
#x=lpopt.linear_programming_sovler()

#print(x)

#xx=qpopt.qpsolver_cvx_test()

#xxx=xx['status']


#print(xx['x'])

#from pyscipopt import Model, quicksum
#from pyscipopt.scip import Expr, ExprCons

#ED=Model()
#xxxx=ED.addVar()
#zzz=ED.addVar()
#hhh=ED.addVar()
#ED.addCons(xxxx>=0)
#ED.addCons(xxxx<=100)
#ED.addCons(zzz>=10)
#ED.addCons(zzz<=100)
#ED.addCons(hhh>=0)
#ED.addCons(hhh<=10000)

#ED.addCons(xxxx+zzz==156.78)
#ED.addCons(xxxx**2<=hhh)
#obj_ED=0.1*hhh+1*zzz
#ED.setObjective(obj_ED, 'minimize')

#result=ED.optimize()
#print(result)

#print(ED.getSolVal(result,xxxx))
#print(ED.getSolVal(result,zzz))
#print(ED.getSolVal(result,hhh))
#Verified by

#Date
