#This is the demo version, the main differences are listed as follows:
#1) The systtem cost is shown
#2) DG fuel limitation is omited
#3) Full version of multiple process control in the EMS.
from multiprocessing import Process,Queue
import os
import time

from default_setting import default_resources_settings
from default_setting import default_time
from modelling.modelling_local_ems import Resource_manager_model,Diesel_generator_type_I_model,Energy_storage_system_model
import forecasting.main as FORECASTING

import optimal_power_flow.main as OPF
import economic_dispatch.main as ED
import unit_committment.main as UC
import optimal_power_flow.optimal_power_flow_setup as OPF_setup
import economic_dispatch.economic_dispatch_setup as ED_setup
import unit_committment.unit_committment_setup as UC_setup
import system_start_up
import connection_RTC_local


DG_ed = Diesel_generator_type_I_model()
DG_uc = Diesel_generator_type_I_model()

ESS_ed = Energy_storage_system_model()
ESS_uc = Energy_storage_system_model()



#################################################1) System start up ####################################################
[UG,DG,WP,PV,ESS,BIC,LOAD_AC,LOAD_AC_NC,LOAD_DC,LOAD_DC_NC,RES,UNCERTAINTY,result_uc] = system_start_up.system_start_up()
if result_uc == 'optimal':
    print("The local energy management system has been estiblished successfully!")
else:
    print('The local energy management system has not been estiblished successfully!')

#############################################2) Running along the time period ##########################################
for Time in range(default_time.Base_time, default_time.Base_time + 8760*3600):#The simulation is carried out every one second
    if Time%default_time.Time_step_ed == 0:#Do the real-time forecasting for the next time slot, economic_dispatch and optimal power flow for this time slot
        print('The real-time forecasting,economic dispatch, optimal power flow scheduling for the following time slot start!')
        print(time.ctime(Time+default_time.Time_step_ed))
        #Update the SOC, fuel state and resource manager models
        Forecasting_result_process = Process(target=FORECASTING.forecasting_real_time, args=(Time+default_time.Time_step_ed, default_time.Look_ahead_time_ed, default_time.Time_step_uc,))
        Forecasting_result_process.start()
        Forecasting_result_process.join()
 #       FORECASTING.forecasting_real_time(Time+default_time.Time_step_ed, default_time.Look_ahead_time_ed, default_time.Time_step_uc)
        print('The end of real time forecasting!')
        RESOURCE_MANAGER = Resource_manager_model()
        #1) Implement the economic dispatch, optimal power flow and
        #1.1) Implement the economic dispatch for this time slot
        #1.1.1) Obtain the unit committment plan

        UC_result = ED_setup.dynamic_economic_dispatch_set_up(Time + default_time.Time_step_ed,default_time.Look_ahead_time_ed)

        #1.1.2) Update the resource manager
        [Load_profile_ac_ed, Load_profile_ac_nc_ed, Load_profile_dc_ed, Load_profile_dc_nc_ed, WP_profile_ed, PV_profile_ed,Price_profile_ed] \
            = ED_setup.forecasting_result_query(Time+default_time.Time_step_ed, default_time.Look_ahead_time_ed, RESOURCE_MANAGER)
        #1.1.3) Obtain the unit committment plan
        # out_economic_dispatch = Queue()
        # procs = []
        # resultdict = {}
        Economic_dispatch_process = Process(target = ED.economic_dispatch, args=(Time+default_time.Time_step_ed, DG_ed, UG, PV, WP, ESS_ed, BIC, LOAD_AC, LOAD_AC_NC, LOAD_DC, LOAD_DC_NC,
                                         RESOURCE_MANAGER, UNCERTAINTY,UC_result,Load_profile_ac_ed, Load_profile_ac_nc_ed, Load_profile_dc_ed,
                                         Load_profile_dc_nc_ed,PV_profile_ed, WP_profile_ed, Price_profile_ed))

        Economic_dispatch_process.start( )
        Economic_dispatch_process.join( )
        # resultdict.update(out_economic_dispatch.get())

        print('The end of economic dispatch!')
        #1.1.3) Implement the optimal power flow for this time slot
        #1.2) Implemnt the optimal power flow for this time slot
        ED_result = OPF_setup.optimla_power_flow_command_from_economic_dispatch(Time+default_time.Time_step_ed)
        #In case the resource models have changed
        RESOURCE_MANAGER = Resource_manager_model()
        [Load_profile_ac_opf,Load_profile_ac_nc_opf,Load_profile_dc_opf,Load_profile_dc_nc_opf,WP_profile_opf,PV_profile_opf,Price_profile_opf] \
            = OPF_setup.optimal_power_flow_information_query(Time+default_time.Time_step_ed,RESOURCE_MANAGER)
        result_opf = OPF.optimal_power_flow(Time+default_time.Time_step_ed,DG_ed,UG,PV,WP,ESS_ed,BIC,LOAD_AC,LOAD_AC_NC,LOAD_DC,LOAD_DC_NC,RESOURCE_MANAGER,UNCERTAINTY,
                                            ED_result,Load_profile_ac_opf,Load_profile_ac_nc_opf,Load_profile_dc_opf,Load_profile_dc_nc_opf,PV_profile_opf,WP_profile_opf,Price_profile_opf)


        print('The end of optimal power flow!')
        ESS_ed.ESS0 = result_opf.SOC * ESS_ed.Capacity # Update the energy storage state
#        DG_ed.Fuel0 =result_ed.DG_fuel[0] # Update the fuel state

        #connection_RTC.connection_RTC(Time+default_time.Time_step_ed,result_opf)
        connection_RTC_local.connection_RTC_local(Time+default_time.Time_step_ed)
        print('The command has been sent to the real-time controller!')
        # 1.3) Send the command to RTC
        #The function is under test_functions.connection_RTC
        # 1.4) Implement the forecasting for the next and further time slot
        if Time%default_time.Time_step_uc == 0: #2) Do the intra day forecasting and unit committment
            print('The unit committment for the following time slot starts!')
            print(time.ctime(Time+default_time.Time_step_uc))
            FORECASTING.forecasting_day_ahead(Time+default_time.Time_step_uc, default_time.Look_ahead_time_uc, default_time.Time_step_uc)
            print('The end of intra day forecasting!')
            RESOURCE_MANAGER = Resource_manager_model()
            ESS_uc.ESS0 = result_opf.SOC * ESS_uc.Capacity  # Update the energy storage state
#            DG_uc.Fuel0 = result_ed.DG_fuel[0]  # Update the fuel state

            [Load_profile_ac_uc,Load_profile_ac_nc_uc,Load_profile_dc_uc,Load_profile_dc_nc_uc,WP_profile_uc,PV_profile_uc,Price_profile_uc]= UC_setup.forecasting_result_query(Time+default_time.Time_step_uc,default_time.Look_ahead_time_uc,RESOURCE_MANAGER)
            result_uc = UC.unit_committment_day_ahead(Time+default_time.Time_step_uc,DG_uc,UG,PV,WP,ESS_uc,BIC,LOAD_AC,LOAD_AC_NC,LOAD_DC,LOAD_DC_NC,RESOURCE_MANAGER,UNCERTAINTY,Load_profile_ac_uc,Load_profile_ac_nc_uc,Load_profile_dc_uc,Load_profile_dc_nc_uc,PV_profile_uc,WP_profile_uc,Price_profile_uc)
            Unit_committment_process = Process(target=UC.unit_committment_day_ahead, args=(Time+default_time.Time_step_uc,
                                                                                           DG_uc,UG,PV,WP,ESS_uc,BIC,LOAD_AC,LOAD_AC_NC,LOAD_DC,LOAD_DC_NC,RESOURCE_MANAGER,UNCERTAINTY,
                                                                                           Load_profile_ac_uc,Load_profile_ac_nc_uc,Load_profile_dc_uc,
                                                                                           Load_profile_dc_nc_uc,PV_profile_uc,WP_profile_uc,Price_profile_uc))
            Unit_committment_process.start( )
            Unit_committment_process.join( )

            print('The end of unit committment!')

#Forecasting
