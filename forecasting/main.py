#This is the integration of all forecasting related function
#The following functions are included
#1) Intra-day forecasting (Hourly ahead)
#1.1) Load, AC critical, AC non-critical, DC critical, DC non-critical
#1.2) Photovoltaic
#1.3) Wind
#1.4) Price (In fact, in ex-ante power market, these prices are fixed)

#2) Real-time forecasting (5 minutes ahead)
#2.1) Load, AC critical, AC non-critical, DC critical, DC non-critical
#2.2) Photovoltaic
#2.3) Wind
#2.4) Price()

#Import related packages
#The dependent packages
from sqlalchemy import create_engine,and_
from sqlalchemy.orm import sessionmaker

from data_management.database_format import Day_ahead_forecasting,Real_time_forecasting
import test_data.commercial_user_profile as critical_load_generation
import test_data.residential_user_profile as uncritical_load_generation
import test_data.photovoltaic_output_profile as pv_output_generation
import test_data.price_profile as price_generation
import test_data.wind_power_profile as wp_output_generation
from default_setting import default_local_database
from default_setting import default_time
from default_setting import default_uncertainty_settings
from test_data import PV_real_value

import random as rand

######################1) Day-ahead forecastig#############################
def forecasting_day_ahead(Start_time=default_time.Base_time,
                          Look_ahead_time=default_time.Look_ahead_time_uc,
                          Time_step=default_time.Time_step_uc):

    Start_time = round((Start_time - default_time.Base_time)/Time_step) + 1
    Look_ahead_time = round(Look_ahead_time/Time_step)
    forecasting_load_critical_ac=critical_load_generation.typical_commercial_load_generation(Start_time, Start_time + Look_ahead_time)
    forecasting_load_uncritical_ac = uncritical_load_generation.typical_resident_load_generation(Start_time, Start_time + Look_ahead_time)
    forecasting_load_critical_dc = critical_load_generation.typical_commercial_load_generation(Start_time, Start_time + Look_ahead_time)
    forecasting_load_uncritical_dc = uncritical_load_generation.typical_resident_load_generation(Start_time, Start_time + Look_ahead_time)
    forecasting_pv_output = PV_real_value.pv_output_generation_hourly(Start_time, Start_time + Look_ahead_time)
    forecasting_wp_output = wp_output_generation.typical_wp_output_generation(Start_time, Start_time + Look_ahead_time)
    forecasting_price = price_generation.typical_price_generation(Start_time, Start_time + Look_ahead_time)

    # Database information, this can also be defined as a
    db_str = default_local_database.db_str

    engine = create_engine(db_str, echo=False)

    Session = sessionmaker(bind=engine)

    session = Session()

    Base_time = default_time.Base_time  # Fri Feb 10 00:00:00 2017, The start time of the operation

    Delta_t_unit_committment = default_time.Time_step_uc  # The time step is

    k = 0

    for i in range(Start_time,Start_time + Look_ahead_time):

        Day_ahead_forecasting_result = Day_ahead_forecasting(TIME_STAMP = (Base_time + (i - 1)* Delta_t_unit_committment),
                                                             CAC3PL_ACTPOW = forecasting_load_critical_ac[k],
                                                             UCAC3PL_ACTPOW = forecasting_load_uncritical_ac[k],
                                                             CDCL_POW = forecasting_load_critical_dc[k],
                                                             UCDCL_POW = forecasting_load_uncritical_dc[k],
                                                             PV_FORECASTING = forecasting_pv_output[k],
                                                             WP_FORECASTING = forecasting_wp_output[k],
                                                             ELEC_PRICE = forecasting_price[k])

        if session.query(Day_ahead_forecasting).filter_by(TIME_STAMP = (Base_time + (i-1)*Delta_t_unit_committment)).count() == 0:#if the data do not exist, the data will be inserted
            session.add(Day_ahead_forecasting_result)
            session.commit()
        else:#if the data has already existed, this data will be updated.
            row=session.query(Day_ahead_forecasting).filter_by(TIME_STAMP = (Base_time + (i-1)*Delta_t_unit_committment)).first()
            row.CAC3PL_ACTPOW = forecasting_load_critical_ac[k]
            row.UCAC3PL_ACTPOW = forecasting_load_uncritical_ac[k]
            row.CDCL_POW = forecasting_load_critical_dc[k]
            row.UCDCL_POW = forecasting_load_uncritical_dc[k]
            row.PV_FORECASTING = forecasting_pv_output[k]
            row.WP_FORECASTING = forecasting_wp_output[k]
            row.ELEC_PRICE = forecasting_price[k]
            session.commit()

        k = k+1

    session.close()

def forecasting_real_time(Start_time=default_time.Base_time,
                          Look_ahead_time=default_time.Look_ahead_time_ed,
                          Time_step=default_time.Time_step_uc):

    Time = Start_time
    Start_time = int((Start_time - default_time.Base_time) / Time_step) + 1
    Start_time_ed = int((Time - default_time.Base_time) / default_time.Time_step_ed) + 1
    # Database information, this can also be defined as a
    db_str = default_local_database.db_str

    engine = create_engine(db_str, echo=False)

    Session = sessionmaker(bind=engine)

    session = Session()

    Base_time = default_time.Base_time  # Fri Feb 10 00:00:00 2017, The start time of the operation

    Delta_t_economic_dispatch = default_time.Time_step_ed  # When the microgrid is grid-connected
    Delta_t_unit_committment = default_time.Time_step_uc  # The time step is


    row_ed = session.query(Day_ahead_forecasting).filter_by(TIME_STAMP = default_time.Base_time+(Start_time-1)* Time_step).first()

    forecasting_pv_output_ed = PV_real_value.pv_output_generation_real_time(Start_time_ed,Start_time_ed + default_time.Look_ahead_time_ed)

    for j in range(default_time.Look_ahead_time_ed_time_step):
        Real_time_forecasting_result = Real_time_forecasting(TIME_STAMP = (Time + j * Delta_t_economic_dispatch),
                                                             CAC3PL_ACTPOW = row_ed.CAC3PL_ACTPOW*(1+(2*rand.random()-1)*default_uncertainty_settings.Uncertainty_critical_load_ac_real_time),
                                                             UCAC3PL_ACTPOW = row_ed.UCAC3PL_ACTPOW*(1+(2*rand.random()-1)*default_uncertainty_settings.Uncertainty_uncritical_load_ac_real_time),
                                                             CDCL_POW = row_ed.CDCL_POW*(1+(2*rand.random()-1)*default_uncertainty_settings.Uncertainty_critical_load_dc_real_time),
                                                             UCDCL_POW = row_ed.UCDCL_POW*(1+(2*rand.random()-1)*default_uncertainty_settings.Uncertainty_uncritical_load_dc_real_time),
                                                             PV_FORECASTING = forecasting_pv_output_ed[j],
                                                             WP_FORECASTING = row_ed.WP_FORECASTING*(1+(2*rand.random()-1)*default_uncertainty_settings.Uncertainty_wp_real_time),
                                                             ELEC_PRICE = row_ed.ELEC_PRICE)

        if session.query(Real_time_forecasting).filter_by(TIME_STAMP = (Time + j * Delta_t_economic_dispatch)).count() == 0:#if the data do not exist, the data will be inserted
            session.add(Real_time_forecasting_result)
            session.commit()

        else:#if the data has already existed, this data will be updated.
            row=session.query(Real_time_forecasting).filter_by(TIME_STAMP = (Base_time + (Start_time - 1)* Delta_t_unit_committment + j * Delta_t_economic_dispatch)).first()
            row.CAC3PL_ACTPOW = row_ed.CAC3PL_ACTPOW*(1+(2*rand.random()-1)*default_uncertainty_settings.Uncertainty_critical_load_ac_real_time)
            row.UCAC3PL_ACTPOW = row_ed.UCAC3PL_ACTPOW*(1+(2*rand.random()-1)*default_uncertainty_settings.Uncertainty_uncritical_load_ac_real_time)
            row.CDCL_POW = row_ed.CDCL_POW*(1+(2*rand.random()-1)*default_uncertainty_settings.Uncertainty_critical_load_dc_real_time)
            row.UCDCL_POW = row_ed.UCDCL_POW*(1+(2*rand.random()-1)*default_uncertainty_settings.Uncertainty_uncritical_load_dc_real_time)
            row.PV_FORECASTING = forecasting_pv_output_ed[j]
            row.WP_FORECASTING = row_ed.WP_FORECASTING*(1+(2*rand.random()-1)*default_uncertainty_settings.Uncertainty_wp_real_time)
            row.ELEC_PRICE = row_ed.ELEC_PRICE
            session.commit()

    session.close()

if __name__ == "__main__":
    #In main function, this is to evaluate day-ahead and real-time forecasting.
    #To check whether these functions can be implmented or not
    Start_time = default_time.Base_time

    Look_ahead_time = default_time.Look_ahead_time_uc

    End_time = Start_time + Look_ahead_time

    Time_step = default_time.Time_step_uc

    forecasting_day_ahead(Start_time,Look_ahead_time,Time_step)

    db_str = default_local_database.db_str

    engine = create_engine(db_str, echo=False)

    Session = sessionmaker(bind=engine)

    session = Session()

    Load_profile_ac = session.query(Day_ahead_forecasting.CAC3PL_ACTPOW).filter(
        and_(Day_ahead_forecasting.TIME_STAMP >= Start_time,
             Day_ahead_forecasting.TIME_STAMP < End_time)).all()

    Load_profile_ac_nc = session.query(Day_ahead_forecasting.UCAC3PL_ACTPOW).filter(
        and_(Day_ahead_forecasting.TIME_STAMP >= Start_time,
             Day_ahead_forecasting.TIME_STAMP < End_time)).all()

    Load_profile_dc = session.query(Day_ahead_forecasting.CDCL_POW).filter(
        and_(Day_ahead_forecasting.TIME_STAMP >= Start_time,
             Day_ahead_forecasting.TIME_STAMP < End_time)).all()

    Load_profile_dc_nc = session.query(Day_ahead_forecasting.UCDCL_POW).filter(
        and_(Day_ahead_forecasting.TIME_STAMP >= Start_time,
             Day_ahead_forecasting.TIME_STAMP < End_time)).all()

    WP_profile = session.query(Day_ahead_forecasting.WP_FORECASTING).filter(
        and_(Day_ahead_forecasting.TIME_STAMP >= Start_time,
             Day_ahead_forecasting.TIME_STAMP < End_time)).all()

    PV_profile = session.query(Day_ahead_forecasting.PV_FORECASTING).filter(
        and_(Day_ahead_forecasting.TIME_STAMP >= Start_time,
             Day_ahead_forecasting.TIME_STAMP < End_time)).all()

    Price_profile = session.query(Day_ahead_forecasting.ELEC_PRICE).filter(
        and_(Day_ahead_forecasting.TIME_STAMP >= Start_time,
             Day_ahead_forecasting.TIME_STAMP < End_time)).all()

    #session.close()

    Load_profile_ac_real = []
    Load_profile_ac_nc_real = []
    Load_profile_dc_real = []
    Load_profile_dc_nc_real = []
    WP_profile_real = []
    PV_profile_real = []
    Price_profile_real = []

    for i in range(len(Load_profile_ac)):
        Load_profile_ac_real.append(Load_profile_ac[i][0])
        Load_profile_ac_nc_real.append(Load_profile_ac_nc[i][0])
        Load_profile_dc_real.append(Load_profile_dc[i][0])
        Load_profile_dc_nc_real.append(Load_profile_dc_nc[i][0])
        WP_profile_real.append(WP_profile[i][0])
        PV_profile_real.append(PV_profile[i][0])
        Price_profile_real.append(Price_profile[i][0])

    print("The test day ahead forecasting result are shown as follows:")
    print(Load_profile_ac_real)
    print(Load_profile_ac_nc_real)
    print(Load_profile_dc_real)
    print(Load_profile_dc_nc_real)
    print(WP_profile_real)
    print(PV_profile_real)
    print(Price_profile_real)
