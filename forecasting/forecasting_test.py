#This funciton is to test the pv forecasting of PV
#This function have been tested by ZHAO TY
#verification date:1487906016.617815

import numpy as np
import neurolab as nl
from sklearn.svm import SVR
from sklearn.externals import joblib
from sqlalchemy import create_engine, and_
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, DATETIME, FLOAT, String
from default_setting import  default_time
import time
from database_format import History_data_pv
# Use sqlalchemy to connect database
Base = declarative_base()

class ForecastDBData(Base):
    __tablename__ = 'forecast'
    date = Column(DATETIME, primary_key=True)
    temp = Column(FLOAT)
    eload = Column(FLOAT)
    eload_f = Column(FLOAT)
    rad = Column(FLOAT)
    solar = Column(FLOAT)
    solar_f = Column(FLOAT)
    speed = Column(FLOAT)
    wind = Column(FLOAT)
    wind_f = Column(FLOAT)


train_start = "2010-01-01 00:01:00"
train_end = "2010-01-04 00:00:00"
test_start = "2010-01-04 12:01:00"
test_end = "2010-01-04 13:00:00"

db_str = 'mysql+pymysql://' + 'root' + ':' + 'Ntu@1003' + '@' + 'localhost' + '/' + 'emsdb'
db_str_pv = 'mysql+pymysql://' + 'root' + ':' + 'Ntu@1003' + '@' + 'localhost' + '/' + 'emsdbtest'

engine = create_engine(db_str, echo=False)
Session = sessionmaker(bind=engine)
session = Session()

engine_pv = create_engine(db_str_pv, echo=False)
Session_pv = sessionmaker(bind=engine_pv)
session_pv = Session_pv()


# for i in range(14400):
#     time_start = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time_int + i * 60))
#     selected_data = session_pv.query(ForecastDBData).filter_by(date = time_start).first()
#
# #Add data to the local database
#     if session.query(History_data_pv).filter_by(TIME_STAMP = default_time.Base_time + i*60).count() == 0:
#         Insert_terms = History_data_pv(TIME_STAMP=default_time.Base_time + i*60,ENV_TEMP=selected_data.temp,SOLAR_RAD=selected_data.rad,SOLAR_POW=selected_data.solar)
#         session.add(Insert_terms)
#         session.commit()
#     else:
#         row = session.query(History_data_pv).filter_by(TIME_STAMP = default_time.Base_time + i*60).first()
#         row.ENV_TEMP=selected_data.temp
#         row.SOLAR_RAD=selected_data.rad
#         row.SOLAR_POW=selected_data.solar
#         # if selected_data.solar != 0:
#         #     print(selected_data.solar)
#
#         session.commit()
#
#     if i%1000==0: print(i)

#Do the forecasting functions
#The forecasting includes the forecasting pre
train_start = default_time.Base_time
train_end = default_time.Base_time + 14400*60
test_start = default_time.Base_time + 11000*60
test_end = default_time.Base_time + 12000*60
train_data = session.query(History_data_pv.SOLAR_RAD, History_data_pv.SOLAR_POW). \
		filter(and_(History_data_pv.TIME_STAMP >= train_start, History_data_pv.TIME_STAMP <= train_end)).all()  # return a list
train_len = len(train_data)  # no. of training data

test_data = session.query(History_data_pv.SOLAR_RAD, History_data_pv.SOLAR_POW). \
		filter(and_(History_data_pv.TIME_STAMP >= test_start, History_data_pv.TIME_STAMP <= test_end)).all()  # return a list
test_len = len(test_data)  # no. of testing data

data = np.vstack((np.asarray(train_data), np.asarray(test_data)))  # list to array
rad = data[:, 0].reshape(-1, 1)
solar = data[:, 1].reshape(-1, 1)

norm_rad = nl.tool.Norm(rad)
radn = norm_rad(rad)
norm_power = nl.tool.Norm(solar)
solarn = norm_power(solar)

# prepare data for SVR
horizon = 1  # 1-step forecasting
lag_solar = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
lag_solar = lag_solar + horizon - 1
lag_rad = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])

trainY = solar[24:train_len]

X1 = np.zeros((len(trainY), len(lag_solar)))
h = range(24, train_len)
for i in range(0, len(lag_solar)):
	X1[:, i] = solar[h - lag_solar[i]].reshape(len(trainY))

X2 = np.zeros((len(trainY), len(lag_rad)))
for j in range(0, len(lag_rad)):
	X2[:, j] = rad[h - lag_rad[j]].reshape(len(trainY))

	# testing set
test_from = len(solar) - test_len
testY = solar[test_from:]

TX1 = np.zeros((len(testY), len(lag_solar)))
h = range(test_from, len(solar))
for i in range(0, len(lag_solar)):
	TX1[:, i] = solar[h - lag_solar[i]].reshape(len(testY))

TX2 = np.zeros((len(testY), len(lag_rad)))
for j in range(0, len(lag_rad)):
	TX2[:, j] = rad[h - lag_rad[j]].reshape(len(testY))

if lag_rad == []:
	trainX = X1
	testX = TX1
elif lag_solar == []:
	trainX = X2
	testX = TX2
else:
	trainX = np.hstack((X1, X2))
	testX = np.hstack((TX1, TX2))

	# SVR
trainY = np.ravel(trainY)
svr_rbf = SVR(kernel='rbf', tol=1e-5, gamma=1.5, epsilon=0.06, C=0.01)
TY = svr_rbf.fit(trainX, trainY).predict(testX)

TY = norm_power.renorm(TY)  # reverse to real scale
ybar = TY.reshape(len(testY))

testY = norm_power.renorm(testY)  # actual output
y = testY.reshape(len(testY))

# performance evaluation
maxPower = 1.0

for i in range(0, len(y)):
	if y[i] > maxPower:
		y[i] = maxPower
	elif y[i] < 0:
		y[i] = 0

test_rmse = np.sqrt(np.mean((y - ybar) ** 2)) / maxPower * 100
print('Forecasting nRMSE is: %.2f' % test_rmse)
	#save forecast model
filename = './solar_predict.pkl'
joblib.dump(svr_rbf, filename, compress = 9)
#Forecast functions

model = joblib.load(filename)
norm_solar = model.predict(trainX)
ybar_2 = norm_power.renorm(norm_solar)
real_rmse = np.sqrt(np.mean((ybar_2 - trainY) ** 2)) / maxPower * 100
print(real_rmse)