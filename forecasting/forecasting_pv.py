#This package is to forecast different time scale output of photovoltaic generations
#Build date: 22/Feb/2017
#This work is based on Li song's forecasting.
import numpy as np
import neurolab as nl
from sklearn.svm import SVR
from sklearn.externals import joblib
from sqlalchemy import create_engine, and_
from sqlalchemy.orm import sessionmaker
from data_management.database_format import History_data_pv
from default_setting import  default_local_database
from default_setting import default_forecasting_model

# load data from database
def read_history_data_pv(train_start,train_end,test_start,test_end):
	#The data size should be smaller than the avalialbe data in the database.
	ems_engine = create_engine(default_local_database.db_str)
	DBsession = sessionmaker(bind=ems_engine)
	session = DBsession( )

	train_data = session.query(History_data_pv.SOLAR_RAD, History_data_pv.SOLAR_POW). \
		filter(and_(History_data_pv.TIME_STAMP >= train_start, History_data_pv.TIME_STAMP <= train_end)).all()  # return a list

	train_len = len(train_data)  # no. of training data

	test_data = session.query(History_data_pv.SOLAR_RAD, History_data_pv.SOLAR_POW). \
		filter(and_(History_data_pv.TIME_STAMP >= test_start, History_data_pv.TIME_STAMP <= test_end)).all()  # return a list

	test_len = len(test_data)  # no. of testing data

	data = np.vstack((np.asarray(train_data), np.asarray(test_data)))  # list to array
	rad = data[:, 0].reshape(-1, 1)
	solar = data[:, 1].reshape(-1, 1)

	session.close()

	return solar, rad, train_len, test_len

def trainning_pv_forecasting_model(solar,rad,train_len,test_len):
	norm_rad = nl.tool.Norm(rad)
	radn = norm_rad(rad)
	norm_power = nl.tool.Norm(solar)
	solarn = norm_power(solar)

	# prepare data for SVR
	horizon = 1  # 1-step forecasting
	lag_solar = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
	lag_solar = lag_solar + horizon - 1
	lag_rad = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])

	trainY = solarn[24:train_len]

	X1 = np.zeros((len(trainY), len(lag_solar)))
	h = range(24, train_len)
	for i in range(0, len(lag_solar)):
		X1[:, i] = solarn[h - lag_solar[i]].reshape(len(trainY))

	X2 = np.zeros((len(trainY), len(lag_rad)))
	for j in range(0, len(lag_rad)):
		X2[:, j] = radn[h - lag_rad[j]].reshape(len(trainY))

	# testing set
	test_from = len(solarn) - test_len
	testY = solarn[test_from:]

	TX1 = np.zeros((len(testY), len(lag_solar)))
	h = range(test_from, len(solarn))
	for i in range(0, len(lag_solar)):
		TX1[:, i] = solarn[h - lag_solar[i]].reshape(len(testY))

	TX2 = np.zeros((len(testY), len(lag_rad)))
	for j in range(0, len(lag_rad)):
		TX2[:, j] = radn[h - lag_rad[j]].reshape(len(testY))

	if lag_rad == []:
		trainX = X1
		testX = TX1
	elif lag_solar == []:
		trainX = X2
		testX = TX2
	else:
		trainX = np.hstack((X1, X2))
		testX = np.hstack((TX1, TX2))

	# SVR
	trainY = np.ravel(trainY)
	svr_rbf = SVR(kernel='rbf', tol=1e-5, gamma=1.5, epsilon=0.06, C=0.01)
	TY = svr_rbf.fit(trainX, trainY).predict(testX)

	TY = norm_power.renorm(TY)  # reverse to real scale
	ybar = TY.reshape(len(testY))

	testY = norm_power.renorm(testY)  # actual output
	y = testY.reshape(len(testY))

	# performance evaluation
	maxPower = 2.0

	for i in range(0, len(y)):
		if y[i] > maxPower:
			y[i] = maxPower
		elif y[i] < 0:
			y[i] = 0
	test_rmse = np.sqrt(np.mean((y - ybar) ** 2)) / maxPower * 100
	#save forecast model
	filename = '.'+default_forecasting_model.pv_forecasting_model
	joblib.dump(svr_rbf, filename, compress=9)
	return test_rmse #return the forecasting performance

def forecasting_model(filename = '.'+default_forecasting_model.pv_forecasting_model,
					  rad = 0):
	model = joblib.load(filename)
	norm_solar = model.predict(testX)
	forecasting_result = norm_power.renorm(norm_solar)

	return forecasting_result