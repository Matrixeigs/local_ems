#This function is to forecast the output of photovoltaic generations during the predefined periods.
#The forcasting functions is based on the Lisong's work.

#Build date: 07/Dec/2016


#####The following scenarios are given when no base profile are given
def load_forecasting_simulated(Start_time=20,Look_ahead_time=24):
    #This function is written for demo, when there is no load forecasting value
    load_profile = [ ]

    return load_profile

"""
Date: 2016-Mar-26

Load forecasting using SVM

"""

import numpy as np
import neurolab as nl
import ems_functions as ems
from sklearn.svm import SVR
import time
import pylab as pl
from sqlalchemy import create_engine, and_
from sqlalchemy.orm import sessionmaker
from ems_functions import ForecastDBData
from sklearn.externals import joblib

# load data from database
tic = time.time()
train_start = "2010-01-01 00:01:00"
train_end = "2010-01-04 00:00:00"
test_start = "2010-01-04 12:01:00"
test_end = "2010-01-04 13:00:00"

eload, temp, train_len, test_len = ems.read_data_load(train_start, train_end, test_start, test_end)

# normalization
norm_temp = nl.tool.Norm(temp)
tempn = norm_temp(temp)
norm_power = nl.tool.Norm(eload)
loadn = norm_power(eload)

# prepare data for SVR
horizon = 1             # 1-step forecasting
lag_load = np.array([1, 2, 3, 23, 24, 25, 48, 72, 144, 168])
lag_load = lag_load + horizon - 1
lag_temp = [ ]

trainX, trainY, testX, testY = ems.prepare_data_load(lag_load, lag_temp, loadn, tempn, train_len, test_len)

# SVR
trainY = np.ravel(trainY)
svr_rbf = SVR(kernel='rbf', tol=1e-5, gamma=3, epsilon=0.0001)
TY = svr_rbf.fit(trainX, trainY).predict(testX)

TY = norm_power.renorm(TY)                                                # reverse to real scale
ybar = TY.reshape(len(testY))

testY = norm_power.renorm(testY)                                          # actual output
y = testY.reshape(len(testY))
toc = time.time()

maxPower = 8.0
test_rmse = np.sqrt(np.mean((y - ybar)**2))/maxPower*100
print('Load forecasting completed')
print('Forecasting nRMSE is: %.2f' % test_rmse)
print('Running time is: %.5f ' % (toc - tic))

# save forecast model
# filename = './predict/load_predict.pkl'
# joblib.dump(svr_rbf, filename, compress=9)#The sharing between the UEMS and EMS

# save forecasted value to database
ems_engine = create_engine('mysql://root:password@localhost/emsdb')
DBsession = sessionmaker(bind=ems_engine)
ems_session = DBsession()
selected_dates = ems_session.query(ForecastDBData.date). \
	filter(and_(ForecastDBData.date >= test_start, ForecastDBData.date <= test_end)).all()

for i in range(0, len(ybar)):
	row = ems_session.query(ForecastDBData).filter_by(date = selected_dates[i][0]).first()
	row.eload_f = ybar[i]

ems_session.commit()
ems_session.close()