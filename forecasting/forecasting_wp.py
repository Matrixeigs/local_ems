#This package is to realize both long-term and short-term forecasting functions for different kind of sources
#Build date: 07/Dec/2016
#Verifed date:
#
import numpy as num

from solver import random_generation as rnd


def wp_output_forecasting_simulated(wp_capacity=5,T=24):
    # This function is written to simulate the output of wind turbine
    wp_output_profile = num.zeros((T, 1))
    for i in range(0,T):
        wp_output_profile[i,:]=[wp_capacity*rnd.random()]

    return wp_output_profile

"""
Date: 2016-Mar-26

Wind forecasting using SVM

"""

import numpy as np
import neurolab as nl
import ems_functions as ems
from sklearn.svm import SVR
import time
import pylab as pl
import sqlite3 as sq
from sklearn.externals import joblib
from sqlalchemy import create_engine, and_
from sqlalchemy.orm import sessionmaker
from ems_functions import ForecastDBData


# read data from database
tic = time.time()
train_start = "2010-01-01 00:01:00"
train_end = "2010-01-04 00:00:00"
test_start = "2010-01-04 12:01:00"
test_end = "2010-01-04 13:00:00"

wind, speed, train_len, test_len = ems.read_data_wind(train_start, train_end, test_start, test_end)

# normalization
norm_speed = nl.tool.Norm(speed)
radn = norm_speed(speed)
norm_power = nl.tool.Norm(wind)
windn = norm_power(wind)

# prepare data for SVR
horizon = 1             # 1-step forecasting
lag_wind = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
lag_wind = lag_wind + horizon - 1
lag_speed = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])

trainX, trainY, testX, testY = ems.prepare_data(lag_wind, lag_speed, windn, radn, train_len, test_len)

# SVR
trainY = np.ravel(trainY)
svr_rbf = SVR(kernel='rbf', tol=1e-5, gamma=0.16, epsilon=0.001, C=0.5)
TY = svr_rbf.fit(trainX, trainY).predict(testX)

TY = norm_power.renorm(TY)                                                # reverse to real scale
ybar = TY.reshape(len(testY))

testY = norm_power.renorm(testY)                                          # actual output
y = testY.reshape(len(testY))
toc = time.time()

# performance evaluation
maxPower = 2.0
ybar = ems.y_in_range(ybar, maxPower)

test_rmse = np.sqrt(np.mean((y - ybar)**2))/maxPower*100
print('Forecasting nRMSE is: %.2f' % test_rmse)
print('Running time is: %.5f ' % (toc - tic))

# save forecast model
# filename = './predict/wind_predict.pkl'
# joblib.dump(svr_rbf, filename, compress=9)

# save forecasted value to database
ems_engine = create_engine('mysql://root:password@localhost/emsdb')
DBsession = sessionmaker(bind=ems_engine)
ems_session = DBsession()
selected_dates = ems_session.query(ForecastDBData.date). \
	filter(and_(ForecastDBData.date >= test_start, ForecastDBData.date <= test_end)).all()

for i in range(0, len(ybar)):
	row = ems_session.query(ForecastDBData).filter_by(date = selected_dates[i][0]).first()
	row.wind_f = ybar[i]

ems_session.commit()
ems_session.close()